<?php
/*
  Plugin Name: WordPress Move Comments
  Plugin URI: http://www.velvetblues.com/web-development-blog/wp-plugin-wordpress-move-comments/
  Description: Moving comments between posts or parents can be a chore, especially if your blog uses threaded comments. WordPress Move Comments simplifies this process by enabling users to easily change the comment's post or page, the parent comment, and the comment author. Edits can be made via both Quick Edit and the simpler single-comment edit screen. To get started: 1) Activate, 2) Edit your comments.
  Author: VelvetBlues.com
  Author URI: http://www.velvetblues.com/
  Author Email: info@velvetblues.com
  Version: 1.0
  License: GPLv2 or later
 */
/*  Copyright 2012  Velvet Blues Web Design  (email : info@velvetblues.com)

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
if (!function_exists('add_action')) {
    ?><h3>Oops! This page cannot be accessed directly.</h3>
    <p>For support using the Velvet Blues WordPress Move Comments plugin, <a href="http://www.velvetblues.com/web-development-blog/wordpress-move-comments-plugin/" title="Velvet Blues WordPress Move Comments plugin">click here</a>.</p>
    <p>If you are looking for general WordPress assistance, <a href="http://www.velvetblues.com/" title="WordPress Web Development and Services">Velvet Blues can help with that too</a>.</p><?php
    exit;
}

add_action('admin_menu', 'velvetblues_wmc_add_meta');
add_filter('comment_save_pre', 'velvetblues_wmc_save');
add_filter('plugin_row_meta', 'velvetblues_wmc_links', 10, 2);
add_filter('current_screen', 'velvetblues_wmc_check_screen');

function velvetblues_wmc_add_meta() {
    //add_meta_box('comment-info', __('Comment Options'), 'velvetblues_wmc_meta_box', 'comment', 'normal');
}

function velvetblues_wmc_meta_box() {
    global $comment;
    ?>
    <table class="widefat form-table editcomment" cellspacing="0">
        <tbody>
            <tr class="alternate">
                <td class="textright">
                    <label for="vb_comment_post_id"><?php _e('Page/Post ID'); ?></label>
                </td>
                <td>
                    <?php
                    $learndash = new learndash_woocommerce();
                    $courses = $learndash->list_courses();
                    if (!empty($courses)) {
                        foreach ($courses as $id => $course) {
                            ?>
                            <input type="checkbox" name="vb_comment_post_id[]" id="vb_comment_post_id_<?php echo $id ?>" value="<?php echo $id ?>" <?php echo ($id == trim(esc_attr($comment->comment_post_ID))) ? 'checked="checked"' : ''; ?> size="40" /> 
                            <?php
                        }
                    } else {
                        ?>
                        <input type="text" name="vb_comment_post_id" id="vb_comment_post_id" value="<?php echo esc_attr($comment->comment_post_ID); ?>" size="40" /> 
                    <?php }
                    ?>

                </td>
            </tr>
            <tr>
                <td class="textright">
                    <label for="vb_comment_parent_id"><?php _e('Comment Parent ID'); ?></label>
                </td>
                <td>
                    <input type="text" name="vb_comment_parent_id" id="vb_comment_parent_id" value="<?php //echo esc_attr( $comment->comment_parent );                                ?>" size="40" />
                </td>
            </tr>
    <!--	<tr class="alternate">
                    <td class="textright">
                            <label for="vb_comment_user_id"><?php //_e('Comment Author ID');                                ?></label>
                    </td>
                    <td>
                            <input type="text" name="vb_comment_user_id" id="vb_comment_user_id" value="<?php //echo esc_attr( $comment->user_id );                                ?>" size="40" />
                    </td>
            </tr>-->
        </tbody>
    </table>
    <?php
}

function velvetblues_wmc_save($comment_content) {

    $courseids = $_POST['courseids'];
    if ($_POST['vb_comment_parent_id']) {
        $comment = get_comment_id_parent_root($_POST['vb_comment_parent_id']);
        $courseids = get_comment_meta($comment->comment_ID, 'courseids');
    }
    if (empty($courseids)) {
        $courseids = $_POST['comment_post_ID'];
    }
    if (!is_array($courseids)) {
        $comment_post_IDS = $courseids;
    } else {
        $comment_post_IDS = implode(',', $courseids);
    }
    //  change to other post/page
//    foreach ($comment_post_IDS as $comment_post_ID) {
//
//
//        $comment_parent = absint($_POST['vb_comment_parent_id']);
//        $user_id = esc_attr($_POST['vb_comment_user_id']);
//        $comment_ID = absint($_POST['comment_ID']);
//
//        if ($comment_parent == $comment_ID)
//            return $comment_content;
//
//        $post = get_post($comment_post_ID);
//        if (!$post)
//            return $comment_content;
//
//        $parent = get_comment($comment_parent);
//        if (!$parent)
//            $comment_parent = 0;
//
//        $commentarr = get_comment($comment_ID, ARRAY_A);
//        $prev_comment_post_ID = $commentarr['comment_post_ID'];
//
//        $data = compact('comment_post_ID', 'comment_parent', 'user_id');
//        $rval = $wpdb->update($wpdb->comments, $data, compact('comment_ID'));
//
//        wp_update_comment_count($comment_post_ID);
//
//        if ($prev_comment_post_ID != $comment_post_ID) {
//            wp_update_comment_count($prev_comment_post_ID);
//            wp_update_comment_count($comment_post_ID);
//        }
//    }
    return $comment_content;
}

add_action('add_meta_boxes_comment', 'comment_add_meta_box');

function comment_add_meta_box() {
    add_meta_box('my-comment-title', __('Extra field comment'), 'comment_meta_box_subject', 'comment', 'normal', 'high');
}

function comment_meta_box_subject($comment) {
    $is_reply = false;
    if ($comment->comment_parent) {
        $commentParent = get_comment_id_parent_root($comment->comment_parent);
        $is_reply = true;
    } else {
        $commentParent = $comment;
    }

    $subject = get_comment_meta($comment->comment_ID, 'subject', true);
    global $wpdb;
    $table = $wpdb->prefix . 'commentmeta';
    $sql = "SELECT `meta_id` as `id`, `meta_value` as `value` FROM $table WHERE `meta_key` ='courseids' AND `comment_id` = '$comment->comment_ID' group by meta_value";
    $meta_list = $wpdb->get_results($sql, ARRAY_A);
    $metas = array();
    if (!empty($meta_list)) {
        foreach ($meta_list as $meta) {
            $metas[$meta['value']] = $meta['id'];
        }
    }
    ?>
    <script>
        function changeAuthor($id, $name, $option) {
                var editRow = jQuery('.editcomment');
                jQuery('#poststuff input[name="user_ID"]').val($id);
                jQuery('input[name="newcomment_author"]', editRow).val($name);
                jQuery('input#email', editRow).val(jQuery($option).data('email'));
                jQuery('input#email').parent().prev().find('a').attr('href','mailto:'+jQuery($option).data('email'));
            }
        jQuery(document).ready(function () {
            
            jQuery.ajax({
                type: 'POST',
                url: '<?php echo admin_url('admin-ajax.php') ?>',
                data: {action: 'get_comment_info_admin', comment_id: <?php echo $comment->comment_ID ?>, is_reply_comment: 2},
                success: function (response) {
                    jQuery('#namediv .inside').find('.comment_info_admin').remove();
                    jQuery(response).insertAfter('#namediv  .inside .editcomment');
                    jQuery(response).find('select').val(<?php echo $comment->user_id?>);
                    jQuery('#poststuff input[name="user_ID"]').val(<?php echo $comment->user_id?>);
                }
            });
            
        })
    </script>
    <p>
        <label for="subject"><?php _e('Subject: '); ?></label>
        <input type="text" name="subject" value="<?php echo esc_attr($subject); ?>"  class="widefat" />
    </p>
    <p>
        <label for="courseids"><?php _e('In Courses : '); ?></label><br/>
        <?php
        $learndash = new learndash_woocommerce();
        $courses = $learndash->list_courses();
        if (!empty($courses)) {
            foreach ($courses as $id => $course) {
                ?>
                <input type="checkbox" name="courseids[<?php echo (in_array($id, array_keys($metas))) ? $metas[$id] : ''; ?>]" id="courseids_<?php echo $id ?>" value="<?php echo $id ?>" <?php echo (in_array($id, array_keys($metas))) ? 'checked="checked"' : ''; ?> <?php echo ($id == $comment->comment_post_ID && !$is_reply) ? 'onclick="return false;" style="opacity:0.5"' : '' ?> /> <span><?php echo $course; ?></span>

                <?php
            }
        }
        ?>

    </p>
    <?php
}

add_action('edit_comment', 'comment_edit_function', 10, 2);
add_action('comment_post', 'comment_edit_function', 10, 2);

function comment_edit_function($comment_id) {

    $comment = get_comment($comment_id);
    if (!$comment) {
        my_admin_notice(__('Comment is not exist'), 'error');
        return false;
    }
    $courseids = array();

    $courseids = array_filter($courseids);

    if ($_POST['action'] == 'replyto-comment') {

        global $wpdb;
        $commentParent = get_comment($_POST['comment_ID']);

        $table = $wpdb->prefix . 'commentmeta';
        $sql = "SELECT `meta_id` as `id`, `meta_value` as `value` FROM $table WHERE `meta_key` ='courseids' AND `comment_id` = '$commentParent->comment_ID' group by meta_value";

        $meta_list = $wpdb->get_results($sql, ARRAY_A);
        if (!empty($meta_list)) {
            foreach ($meta_list as $meta) {
                $courseids[$meta['id']] = $meta['value'];
            }
        }
    } else if (isset($_POST['courseids'])) {
        $courseids = $_POST['courseids'];
    }
    $courseids = array_filter($courseids);
    if (empty($courseids)) {
        $courseids = $_POST['comment_post_ID'];
    }

    update_add_comment_meta($comment_id, 'courseids', $courseids);

    if (isset($_POST['subject']))
        update_comment_meta($comment_id, 'subject', esc_attr($_POST['subject']));
    if (isset($_POST['user_ID'])) {
        global $wpdb;
        $sqlc = "update $wpdb->comments set user_id= '" . $_POST['user_ID'] . "',comment_author= '" . $_POST['newcomment_author'] . "' ,comment_author_email= '" . $_POST['newcomment_author_email'] . "' where comment_ID= " . $comment_id;

        $wpdb->query($sqlc);
    }
}

function update_add_comment_meta($comment_id = 0, $key = '', $datas) {
    if (!is_array($datas)) {
        $datas = array($datas);
    }
    remove_meta_comment_old($comment_id, $key, $datas);
    if (!empty($datas)) {
        foreach ($datas as $courseid) {
            add_comment_meta($comment_id, $key, $courseid);
        }
    }
}

function remove_meta_comment_old($comment_id, $key, $datas) {
//    $meta_id = array_keys($datas);
//    $meta_ids = implode(',', $meta_id);
    global $wpdb;
//    $where = '';
//    if(!empty($datas)){
//        $where = "`meta_id` IN ($meta_ids) AND ";
//    }
    $table = $wpdb->prefix . 'commentmeta';
    $sql = "DELETE FROM $table WHERE `meta_key` = '$key' AND `comment_id` = $comment_id ";
    $wpdb->query($sql);
}

function my_admin_notice($msg, $type) {
    if ($msg) {
        ?>
        <div class="<?php echo $type; ?>">
            <p><?php _e($msg, 'divi'); ?></p>
        </div>
        <?php
    }
}

add_action('admin_notices', 'my_admin_notice');

function velvetblues_wmc_links($links, $file) {
    if (strpos($file, 'vb-wp-move-comments.php') !== false) {
        $links = array_merge($links, array('<a href="http://www.velvetblues.com/web-development-blog/wp-plugin-wordpress-move-comments/" title="Need help?">' . __('Support') . '</a>'));
        $links = array_merge($links, array('<a href="http://www.velvetblues.com/go/wpmovecommentsdonate/" title="Support plugin development">' . __('Donate') . '</a>'));
    }
    return $links;
}

add_action('wp_ajax_get_comment_info_admin', 'get_comment_info_admin');
add_action('wp_ajax_nopriv_get_comment_info_admin', 'get_comment_info_admin');

function get_comment_info_admin() {
    $comment_id = $_POST['comment_id'];
    $is_reply = $_POST['is_reply_comment'];
    $comment = get_comment($comment_id);
    $teachers = get_users(array('role' => 'teacher'));
    $user = wp_get_current_user();

    $author = '<div class="comment_info_admin" style="padding:5px"><label for="author">' . __('Select Author') . '</label><select name="replycomment_author" onchange="changeAuthor(this.value,this.options[this.selectedIndex].text,this.options[this.selectedIndex])">';
    $author .= ' <option  data-email="' . $user->data->user_email . '" value="' . $user->ID . '"';
    $author .=($user->ID == $comment->user_id) ? 'selected="selected"' : '';
    $author .= '>' . $user->data->display_name . '</option>';

    if (!empty($teachers)):
        foreach ($teachers as $teacher):
            if ($teacher->ID != $user->ID) :
                $author .='<option data-email="' . $teacher->data->user_email . '" value="' . $teacher->ID . '"';
                $author .=($teacher->ID == $comment->user_id) ? 'selected="selected"' : '';
                $author .= '>' . $teacher->data->display_name . '</option>';
            endif;
        endforeach;
    endif;
    $subject = '';
    if (!$is_reply) {
        $subject = get_comment_meta($comment_id, 'subject', true);
    }
    $author .= '</select>';
    if ($is_reply != 2):
        $author .= '<label for="subject">Subject</label>
        <input type="text" name="subject" placeholder="Subject" value="' . $subject . '"  id="subject" />';

    endif;
    $author .= '</div>';
    echo $author;
    exit;
}

function velvetblues_wmc_quick_edit_menu($str, $input) {
    extract($input);
    $table_row = TRUE;
    if ($mode == 'single') {
        $wp_list_table = _get_list_table('WP_Post_Comments_List_Table');
    } else {
        $wp_list_table = _get_list_table('WP_Comments_List_Table');
    }

    ob_start();
    $quicktags_settings = array('buttons' => 'strong,em,link,block,del,ins,img,ul,ol,li,code,spell,close');
    wp_editor('', 'replycontent', array('media_buttons' => false, 'tinymce' => false, 'quicktags' => $quicktags_settings, 'tabindex' => 104));
    $editorStr = ob_get_contents();
    ob_end_clean();

    ob_start();
    wp_nonce_field("replyto-comment", "_ajax_nonce-replyto-comment", false);
    if (current_user_can("unfiltered_html"))
        wp_nonce_field("unfiltered-html-comment", "_wp_unfiltered_html_comment", false);
    $nonceStr = ob_get_contents();
    ob_end_clean();

    $content = '<form method="get" action="">';
    if ($table_row) :
        $content .= '<table style="display:none;"><tbody id="com-reply"><tr id="replyrow" style="display:none;"><td colspan="' . $wp_list_table->get_column_count() . '" class="colspanchange">';
    else :
        $content .= '<div id="com-reply" style="display:none;"><div id="replyrow" style="display:none;">';
    endif;


    $author .='<select>';
    $content .= '
            <div id="replyhead" style="display:none;"><h5>' . __('Reply to Comment') . '</h5></div>
            <div id="addhead" style="display:none;"><h5>' . __('Add new Comment') . '</h5></div>
            <div id="edithead" style="display:none;">';

    $content .= '  
                <div class="inside">
               
                
                <input type="hidden" name="newcomment_author" size="50" value="" id="author" />
                </div>
         
                <div class="inside">
                <input type="hidden" name="newcomment_author_email" size="50" value="" id="author-email" />
                </div>
                <div style="clear:both;"></div>';

    $content .= '
                <div class="inside">
                <input type="hidden" id="comment-post-id" name="vb_comment_post_id" size="50" value="" />
                </div>
 
                <div class="inside">
                <input type="hidden" id="comment-parent" name="vb_comment_parent_id" size="50" value="" />
                </div>
 
                <div class="inside">
                <input type="hidden" id="comment-author-id" name="vb_comment_user_id" size="103" value="" />
                </div>
                <div style="clear:both;"></div>
                
            </div>
            ';

    $content .= "<div id='replycontainer'>\n";
    $content .= $editorStr;
    $content .= "</div>\n";

    $content .= '          
            <p id="replysubmit" class="submit">
            <a href="#comments-form" class="save button-primary alignright">
            <span id="addbtn" style="display:none;">' . __('Add Comment') . '</span>
            <span id="savebtn" style="display:none;">' . __('Update Comment') . '</span>
            <span id="replybtn" style="display:none;">' . __('Submit Reply') . '</span></a>
			<a href="#comments-form" class="cancel button-secondary alignleft">' . __('Cancel') . '</a>
            <span class="waiting spinner"></span>
            <span class="error" style="display:none;"></span>
            <br class="clear" />
            </p>';

    $content .= '
            <input type="hidden" name="user_ID" id="user_ID" value="' . get_current_user_id() . '" />
            <input type="hidden" name="action" id="action" value="" />
            <input type="hidden" name="comment_ID" id="comment_ID" value="" />
            <input type="hidden" name="comment_post_ID" id="comment_post_ID" value="" />
            <input type="hidden" name="status" id="status" value="" />
            <input type="hidden" name="position" id="position" value="' . $position . '" />
            <input type="hidden" name="checkbox" id="checkbox" value="';

    if ($checkbox)
        $content .= '1';
    else
        $content .= '0';
    $content .= "\" />\n";
    $content .= '<input type="hidden" name="mode" id="mode" value="' . esc_attr($mode) . '" />';

    $content .= $nonceStr;
    $content .="\n";

    if ($table_row) :
        $content .= '</td></tr></tbody></table>';
    else :
        $content .= '</div></div>';
    endif;
    $content .= "\n</form>\n";
    return $content;
}

function velvetblues_wmc_quick_edit_data($comment_text, $comment) {
    ?>
    <div id="inline-vbaddl-<?php echo $comment->comment_ID; ?>" class="hidden">
        <div class="comment-post-id"><?php echo esc_attr($comment->comment_post_ID); ?></div>
        <div class="comment-parent"><?php echo esc_attr($comment->comment_parent); ?></div>
        <div class="comment-author-id"><?php echo esc_attr($comment->user_id); ?></div>
        <div class="subject"><?php echo esc_attr(get_comment_meta($comment->comment_ID, 'subject', true)); ?></div>

    </div> 
    <?php
    return $comment_text;
}

function velvetblues_wmc_quick_edit_js() {
    ?>
    <script type="text/javascript">
        function expandedOpen(subject, userid) {
            var editRow = jQuery('#replyrow');
            //            var rowData = jQuery('#inline-vbaddl-' + id);
            //            jQuery('input.inputquickshow').show();
            //            jQuery('#comment-post-id', editRow).val(jQuery('div.comment-post-id', rowData).text());
            //            //jQuery('div.quickeditcomment', editRow).html(jQuery('div.quickeditcommentInput', rowData).html());
            //            jQuery('#comment-parent', editRow).val(jQuery('div.comment-parent', rowData).text());
            console.log(userid);
            jQuery('input#subject', editRow).val(subject);
        }
        function changeAuthor($id, $name, $option) {
            var editRow = jQuery('#replyrow');
            jQuery('#comment-author-id', editRow).val($id);
            jQuery('input#user_ID').val($id);
            jQuery('input#author', editRow).val($name);
            jQuery('input#author-email', editRow).val(jQuery($option).data('email'));
        }
        jQuery('#the-comment-list').on('click', '.comment-inline', function (e) {
            var $reply = 0;
            if (jQuery(this).hasClass('vim-r')) {
                $reply = 1;
            }
            jQuery.ajax({
                type: 'POST',
                url: '<?php echo admin_url('admin-ajax.php') ?>',
                data: {action: 'get_comment_info_admin', comment_id: jQuery(this).data('comment-id'), is_reply_comment: $reply},
                success: function (response) {
                    jQuery('#replyrow .colspanchange').find('.comment_info_admin').remove();
                    jQuery(response).insertAfter('#replyhead');
                    jQuery(response).find('select').trigger('change');
                }
            });
        });
    </script>
    <?php
}

function velvetblues_wmc_quick_edit_action($actions, $comment) {
    global $post;
//    $subject = get_comment_meta($comment->comment_ID, 'subject', true);
//    if (isset($_SESSION['user_ID'][$comment->comment_ID])) {
//        $authorid = $_SESSION['user_ID'][$comment->comment_ID];
//    } else {
//        $authorid = $comment->user_id;
//    }
    //$actions['quickedit'] = '<a onclick="commentReply.close();if (typeof(expandedOpen) == \'function\') expandedOpen(\'' .$subject . '\','.$authorid.');commentReply.open( \'' . $comment->comment_ID . '\',\'' . $post->ID . '\',\'edit\' );return false;" class="vim-q" title="' . esc_attr__('Quick Edit') . '" href="#">' . __('Quick&nbsp;Edit') . '</a>';
    $format = '<a data-comment-id="%d" data-post-id="%d" data-action="%s" class="%s" title="%s" href="#">%s</a>';

    $actions['reply'] = sprintf($format, $comment->comment_ID, $post->ID, 'replyto', 'vim-r comment-inline', esc_attr__('Reply to this comment'), __('Reply'));
    return $actions;
}

function velvetblues_wmc_check_screen($screen) {

    if (in_array($screen->id, array('edit-comments', 'post', 'page'))) {
        add_filter('wp_comment_reply', 'velvetblues_wmc_quick_edit_menu', 10, 2);
        add_filter('comment_text', 'velvetblues_wmc_quick_edit_data', 10, 2);
        //add_filter('comment_row_actions', 'velvetblues_wmc_quick_edit_action', 10, 2);
        add_action('admin_footer', 'velvetblues_wmc_quick_edit_js');
    }
    return $screen;
}
?>