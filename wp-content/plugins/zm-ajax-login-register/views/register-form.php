<?php
/**
 * This is the template for our register form. It should contain as less logic as possible
 */
?>

<!-- Register Modal -->
<?php if (get_option('users_can_register')) : ?>
    <div class="ajax-login-register-register-container">
        <p style="font-size: 13px;">(Please kindly note that we have difficulties delivering email from our server to certain providers, especially web.de, email.de, and gmx.*. Mails can of course always end up in your junk/spam folder, but with some providers, our mails don't even make it there. We continue to work on it, but if you can you may want to use a different service or any system notifications won't reach you. For instance the activation email you need to log in after registering. Or password reset emails. Apple's icloud.com or me.com, Google's gmail.com, and most independently hosted mail servers, e.g, yourdomain.xyz, have no trouble receiving our mail. If you only have an email address with one of the services that don't like us all that much, kindly contact us at alaya@dharma.online after registering, so we can activate your account for you. Also, if you can't find your activation mail in your inbox or junk/spam folder, give a shout and we'll sort you out. Thank you!)</p>
        <?php if (is_user_logged_in()) : ?>
            <p><?php printf('%s <a href="%s" title="%s">%s</a>', __('You are already registered', 'ajax_login_register'), wp_logout_url(site_url()), __('Logout', 'ajax_login_register'), __('Logout', 'ajax_login_register')); ?></p>
        <?php else : ?>
            <?php
                if (isset($_POST)&& $_POST['page'] =='welcome'):
                $user = get_user_by('email', $_SESSION['emailActive']);
                $meta = get_user_meta($user->ID);
                
                ?>
                <form  action="javascript://" name="registerform" class="validate ajax-login-default-form-container register_form <?php print get_option('ajax_login_register_default_style'); ?>">
                    <?php if (get_option('ajax_login_register_facebook')) : ?>
                        <div class="fb-login-container">
                            <a href="#" class="fb-login"><img src="<?php print plugin_dir_url(dirname(__FILE__)); ?>assets/images/fb-login-button.png" /></a>
                        </div>
                    <?php endif; ?>
                    <div class="form-wrapper">
                        <?php
                        wp_nonce_field('facebook-nonce', 'facebook_security');
                        wp_nonce_field('register_submit', 'security');
                        ?>
                        <div class="ajax-login-register-status-container">
                            <div class="ajax-login-register-msg-target"></div>
                        </div>
                        <?php do_action('zm_ajax_login_register_below_email_field'); ?>
                        <div class="noon"><label><?php _e('First name', 'Divi'); ?> <span class="required">*</span></label><input type="text" value="<?php echo $user->first_name; ?>" required name="first_name" class="user_first_name" /></div>
                        <div class="noon right"><label><?php _e('Last name', 'Divi'); ?> <span class="required">*</span></label><input type="text" value="<?php echo $user->last_name; ?>" required name="last_name" class="user_last_name" /></div>
                        <div class="noon"> <label><?php _e('Date of birth', "Divi"); ?></label>
                            <input type="text" id="dob_datepicker" name="dob" placeholder="dd/mm/yyyy" class="regular-text datepicker hasDatepicker" value="<?php echo $meta['dob'][0]; ?>" />
                        </div>
                        <div class="noon right"><label for="email"><?php _e('Email', 'ajax_login_register'); ?> <span class="required">*</span></label>
                            <input type="text" required id="email" name="email" value="<?php echo $user->user_email; ?>" placeholder="Email" class="validateEmail required user_email ajax-login-register-validate-email"/>
                        </div>
                        <div class="noon"><label><?php _e('Password', 'ajax_login_register'); ?> <span class="required">*</span></label>
                            <input type="password"  required name="password" id="password" class="user_password" />
                        </div>
                        <div class="noon right"><label><?php _e('Confirm Password', 'ajax_login_register'); ?> <span class="required">*</span></label><input type="password" required name="confirm_password" class="user_confirm_password" /></div>
                        <div class="noon"><label><?php _e('Refuge Name', "Divi"); ?> <span class="required">*</span></label><input type="text" name="display_name" value="<?php echo $user->display_name; ?>" class="user_display_name" required/></div>
                        <div class="noon right"><label><?php _e('Gender', "Divi"); ?> </label>
                            <input type="radio"  name="gender" value="1" <?php if($meta['gender'][0]==1) echo "checked='checked'"; ?> class="user_gender" /><span><?php _e('Male', "Divi"); ?></span>
                            <input type="radio"  name="gender" value="2" <?php if($meta['gender'][0]==2) echo "checked='checked'"; ?> class="user_gender" /><span><?php _e("Female", "Divi"); ?></span>
                        </div>
                        <div class="noon" style="width:calc(100% - 10px);"> <label for="address"><?php _e('Address', 'Divi'); ?></label>
                            <textarea id="area_address" name="address">
                                <?php echo $meta['address'][0]; ?>
                            </textarea>
                        </div>
<!--                        <div class="noon">  <label for="language"><?php //_e('Language'); ?></label>
                            <select class="language_selectbox" name="language" >
                                <option value="en_US"><?php //_e('English', "Divi"); ?></option>
                                <option value="vi_VN"><?php //_e('Vietnamese', "Divi"); ?></option>
                            </select>
                        </div>-->

                        <div class="noon right" style="margin-left: 5px"> <label for="notification"><?php _e('Sign up for notifications'); ?></label>        
<!--                            <p id="mc4wp-checkbox">-->
<!--                                <label>-->
<!--                                    <input type="checkbox" name="_mc4wp_subscribe_registration_form" value="1" --><?php //echo (isset($meta['user_meta_mailchimp'][0]) && ($meta['user_meta_mailchimp'][0]) )?'checked=""':''?><!--> -->
<!--                                    --><?php //_e('Sign me up for the newsletter!', 'Divi'); ?>
<!--                                </label>-->
<!--                            </p>-->
                        </div>                    
                        <div class="noon show-captcha" style="width:calc(100% - 13px)"> 
                            <div id="captchaIframe">
                                <?php
                                require_once(ABSPATH . 'wp-admin/admin-functions.php');
                                if (class_exists('ReallySimpleCaptcha')) { //check if the Really Simple Captcha class is available
                                    $captcha = new ReallySimpleCaptcha();
                                    $captcha->char_length = 6;
                                    $captcha->img_size = array(95, 28);
                                    $captcha_word = $captcha->generate_random_word(); //generate a random string with letters
                                    $captcha_prefix = mt_rand(); //random number
                                    $captcha_image = $captcha->generate_image($captcha_prefix, $captcha_word); //generate the image file. it returns the file name
                                    $captcha_view = rtrim(get_bloginfo('wpurl'), '/') . '/wp-content/plugins/really-simple-captcha/tmp/' . $captcha_image; //construct the absolute URL of the captcha image
                                }
                                ?>
                            </div>
                            <label for="captcha_code"><?php _e('Security code'); ?> <span class="required">*</span></label> 
                            <input class="captcharegister" required type="text" name="captcha_code" value="<?php echo esc_attr(stripslashes($_POST['captcha_code'])); ?>" />
                            <div class="imagecaptcha"><img id="imagecaptcha" src="<?php echo $captcha_view; ?>"  alt="captcha" /> <a href="javascript:;" onclick="App.reloadCaptchaReally(this);" data-link="<?php echo admin_url("admin-ajax.php"); ?>"  class="reload" title="<?php _e('Reload captcha', 'divi') ?>"></a></div>
                        </div>
                        <div class="button-container">
                            <p class='note pull-left'>
                                <i>(<span class="required">* </span><?php _e('required information', 'Divi'); ?>)</i>
                            </p>
                            <input type="hidden" id="hidden_captcha" name="captcha_prefix" value="<?php echo $captcha_prefix; ?>" />
                            <input type="hidden" name="reg_again" value="1" />
                            <input class="register_button green" type="submit" value="<?php _e('Update', 'ajax_login_register'); ?>" accesskey="p" name="register" disabled />
                            <a href="#" style="float:right;line-height: 28px;margin-right: 15px" class="already-registered-handle"><?php echo apply_filters('ajax_login_register_already_registered_text', __('Login?', 'ajax_login_register')); ?></a>
                        </div>
                    </div>
                </form>
            <?php else: ?>
                <form  action="javascript://" name="registerform" class="validate ajax-login-default-form-container register_form <?php print get_option('ajax_login_register_default_style'); ?>">
                    <?php if (get_option('ajax_login_register_facebook')) : ?>
                        <div class="fb-login-container">
                            <a href="#" class="fb-login"><img src="<?php print plugin_dir_url(dirname(__FILE__)); ?>assets/images/fb-login-button.png" /></a>
                        </div>
                    <?php endif; ?>
                    <div class="form-wrapper">
                        <?php
                        wp_nonce_field('facebook-nonce', 'facebook_security');
                        wp_nonce_field('register_submit', 'security');
                        ?>
                        <div class="ajax-login-register-status-container">
                            <div class="ajax-login-register-msg-target"></div>
                        </div>
                        <?php do_action('zm_ajax_login_register_below_email_field'); ?>
                        <div class="noon"><label><?php _e('First name', 'Divi'); ?> <span class="required">*</span></label><input type="text" required name="first_name" class="user_first_name" /></div>
                        <div class="noon right"><label><?php _e('Last name', 'Divi'); ?> <span class="required">*</span></label><input type="text" required name="last_name" class="user_last_name" /></div>
                        <div class="noon"> <label><?php _e('Date of birth', "Divi"); ?></label>
                            <input type="text" id="dob_datepicker" name="dob" placeholder="dd/mm/yyyy" class="regular-text datepicker hasDatepicker" value="" />
                        </div>
                        <div class="noon right"><label for="email"><?php _e('Email', 'ajax_login_register'); ?> <span class="required">*</span></label>
                            <input type="text" required id="email" name="email" value="" placeholder="Email" class="validateEmail required user_email ajax-login-register-validate-email" />
                        </div>
                        <div class="noon"><label><?php _e('Password', 'ajax_login_register'); ?> <span class="required">*</span></label>
                            <input type="password"  required name="password" id="password" class="user_password" />
                        </div>
                        <div class="noon right"><label><?php _e('Confirm Password', 'ajax_login_register'); ?> <span class="required">*</span></label><input type="password" required name="confirm_password" class="user_confirm_password" /></div>
                        <div class="noon"><label><?php _e('Refuge Name', "Divi"); ?> <span class="required">*</span></label><input type="text" name="display_name" class="user_display_name" required/></div>
                        <div class="noon right"><label><?php _e('Gender', "Divi"); ?> </label>
                            <input type="radio"  name="gender" value="1" class="user_gender" /><span><?php _e('Male', "Divi"); ?></span>
                            <input type="radio"  name="gender" value="2" class="user_gender" /><span><?php _e("Female", "Divi"); ?></span>
                        </div>
                        <div class="noon" style="width:calc(100% - 10px);"> <label for="address"><?php _e('Address', 'Divi'); ?></label>
                            <textarea id="area_address" name="address"></textarea>
                        </div>
<!--                        <div class="noon">  <label for="language"><?php //_e('Language'); ?></label>
                            <select class="language_selectbox" name="language" >
                                <option value="en_US"><?php //_e('English', "Divi"); ?></option>
                                <option value="vi_VN"><?php //_e('Vietnamese', "Divi"); ?></option>
                            </select>
                        </div>-->

                        <div class="noon right" style="margin-left: 5px"> <label for="notification">By registering I agree with the <a href="/causes-conditions/">Causes & Conditions</a></label>
<!--                            <p id="mc4wp-checkbox">-->
<!--                                <label>-->
<!--                                    <input type="checkbox" name="_mc4wp_subscribe_registration_form" value="1" checked=""> -->
<!--                                    --><?php //_e('Sign me up for the newsletter!', 'Divi'); ?>
<!--                                </label>-->
<!--                            </p>-->
                        </div>                    
                        <div class="noon show-captcha" style="width:calc(100% - 13px)"> 
                            <div id="captchaIframe">
                                <?php
                                require_once(ABSPATH . 'wp-admin/admin-functions.php');
                                if (class_exists('ReallySimpleCaptcha')) { //check if the Really Simple Captcha class is available
                                    $captcha = new ReallySimpleCaptcha();
                                    $captcha->char_length = 6;
                                    $captcha->img_size = array(95, 28);
                                    $captcha_word = $captcha->generate_random_word(); //generate a random string with letters
                                    $captcha_prefix = mt_rand(); //random number
                                    $captcha_image = $captcha->generate_image($captcha_prefix, $captcha_word); //generate the image file. it returns the file name
                                    $captcha_view = rtrim(get_bloginfo('wpurl'), '/') . '/wp-content/plugins/really-simple-captcha/tmp/' . $captcha_image; //construct the absolute URL of the captcha image
                                }
                                ?>
                            </div>
                            <label for="captcha_code"><?php _e('Security code'); ?> <span class="required">*</span></label> 
                            <input class="captcharegister" required type="text" name="captcha_code" value="<?php echo esc_attr(stripslashes($_POST['captcha_code'])); ?>" />
                            <div class="imagecaptcha"><img id="imagecaptcha" src="<?php echo $captcha_view; ?>"  alt="captcha" /> <a href="javascript:;" onclick="App.reloadCaptchaReally(this);" data-link="<?php echo admin_url("admin-ajax.php"); ?>"  class="reload" title="<?php _e('Reload captcha', 'divi') ?>"></a></div>
                        </div>
                        <div class="button-container">
                            <p class='note pull-left'>
                                <i>(<span class="required">* </span><?php _e('required information', 'Divi'); ?>)</i>
                            </p>
                            <input type="hidden" id="hidden_captcha" name="captcha_prefix" value="<?php echo $captcha_prefix; ?>" />
                            <input class="register_button green" type="submit" value="<?php _e('Register', 'ajax_login_register'); ?>" accesskey="p" name="register" disabled />
                            <a href="#" style="float:right;line-height: 28px;margin-right: 15px" class="already-registered-handle"><?php echo apply_filters('ajax_login_register_already_registered_text', __('Login?', 'ajax_login_register')); ?></a>
                        </div>
                    </div>
                </form>
            <?php endif; ?>
        <?php endif; ?>
    </div>
<?php else : ?>
    <p><?php _e('Registration is currently closed.', 'ajax_login_register'); ?></p>
<?php endif; ?>
<!-- End 'modal' -->