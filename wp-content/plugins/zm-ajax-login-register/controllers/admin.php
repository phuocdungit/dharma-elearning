<?php

Class ajax_login_register_Admin Extends AjaxLogin {

    public $campaign_text_link;
    public $campaign_banner_link;

    /**
     * WordPress hooks to be ran during init
     */
    public function __construct() {
        $this->campaign_text_link = 'http://store.zanematthew.com/downloads/zm-ajax-login-register-pro/?utm_source=WordPress&utm_medium=text&utm_campaign=ALR%20Pro';
        $this->campaign_banner_link = 'http://store.zanematthew.com/downloads/zm-ajax-login-register-pro/?utm_source=WordPress&utm_medium=banner&utm_campaign=ALR%20Pro';

        add_action('admin_init', array(&$this, 'admin_init'));
        add_action('admin_menu', array(&$this, 'admin_menu'));
        add_filter('plugin_action_links', array(&$this, 'plugin_action_links'), 10, 2);
        add_action('admin_notices', array(&$this, 'admin_notice'));
        add_action('admin_enqueue_scripts', array(&$this, 'admin_enqueue_scripts'));
    }

    /**
     * Register settings during the admin init
     */
    public function admin_init() {
        $this->register_settings();
    }

    /**
     * Retrieve our settings from our parent class and register each setting.
     * @todo use add_settings_section()
     */
    public function register_settings() {

        $settings = $this->get_settings();

        foreach ($settings as $k => $v) {
            foreach ($settings[$k] as $k) {
                register_setting('ajax_login_register', $k['key'], array(&$this, 'sanitize'));
            }
        }
    }

    /**
     * This method is fired when the settings that are register with register_settings()
     * are saved from the settings page. We filter out ALL html/js using wp_kses().
     */
    public function sanitize($setting) {
        return wp_kses($setting, array(), array());
    }

    /**
     * Build our admin menu
     */
    public function admin_menu() {

        $parent = 'options-general.php';

        $sub_menu_pages = array(
            array(
                'parent_slug' => $parent,
                'page_title' => __('AJAX Login &amp; Register', 'ajax_login_register'),
                'menu_title' => __('AJAX Login &amp; Register', 'ajax_login_register'),
                'capability' => 'manage_options',
                'menu_slug' => 'ajax-login-register-settings',
                'function' => 'load_template'
            )
        );

        foreach ($sub_menu_pages as $sub_menu) {
            add_submenu_page(
                    $sub_menu['parent_slug'], $sub_menu['page_title'], $sub_menu['menu_title'], $sub_menu['capability'], $sub_menu['menu_slug'], array(&$this, $sub_menu['function'])
            );
        }
    }

    /**
     * Call back function which is fired when the admin menu page is loaded.
     */
    public function load_template() {
        load_template(plugin_dir_path(dirname(__FILE__)) . 'views/settings.php');
    }

    /**
     * Add our links to the plugin page, these show under the plugin in the table view.
     *
     * @param $links(array) The links coming in as an array
     * @param $current_plugin_file(string) This is the "plugin basename", i.e., my-plugin/plugin.php
     */
    public function plugin_action_links($links, $current_plugin_file) {
        if ($current_plugin_file == 'zm-ajax-login-register/plugin.php') {
            $links['ajax_login_register_settings'] = '<a href="' . admin_url('options-general.php?page=ajax-login-register-settings') . '">' . esc_attr__('General Settings', 'ajax_login_register') . '</a>';
            //$links['ajax_login_register_pro'] = sprintf('<a href="%2$s" title="%1$s" target="_blank">%1$s</a>', esc_attr__('Pro Version', 'ajax_login_register'), $this->campaign_text_link );
        }

        return $links;
    }

    /**
     * Show an admin notice when the plugin is activated
     * note the option 'ajax_login_register_plugin_notice_shown', is removed
     * during the 'register_deactivation_hook', see 'ajax_login_register_deactivate()'
     */
    public function admin_notice() {
        if (!get_option('ajax_login_register_plugin_notice_shown') && is_plugin_active('zm-ajax-login-register/plugin.php')) {
            printf('<div class="updated"><p>%1$s %2$s</p></div>', __('Thanks for installing zM AJAX Login & Register, be sure to check out the features in the', 'ajax_login_register'), '<a href="' . $this->campaign_text_link . '" target="_blank">Pro version</a>.'
            );
            update_option('ajax_login_register_plugin_notice_shown', 'true');
        }
    }

    /**
     * Enqueue our Admin styles only on the ajax login register setting page
     */
    public function admin_enqueue_scripts() {
        $screen = get_current_screen();
        if ($screen->id == 'settings_page_ajax-login-register-settings') {
            wp_enqueue_style('ajax-login-register-admin-style', dirname(plugin_dir_url(__FILE__)) . '/assets/admin.css');
        }
    }

}

new ajax_login_register_Admin;
add_action('show_user_profile', 'numediaweb_custom_user_profile_fields');
add_action('edit_user_profile', 'numediaweb_custom_user_profile_fields');


function numediaweb_custom_user_profile_fields($user) {
    $meta = get_user_meta($user->ID);
    ?>
    <table class="form-table">
        <tr>
            <th>
                <label for="gender"><?php _e('Gender'); ?></label>
            </th>
            <td>
                <input type="radio" name="profile_register[gender]" value="1" <?php echo (isset($meta['gender']) && $meta['gender'][0] == 1) ? 'checked="checked"' : '' ?> /> <?php _e('Male'); ?>
                <input type="radio" name="profile_register[gender]" value="2" <?php echo (isset($meta['gender']) && $meta['gender'][0] == 2) ? 'checked="checked"' : '' ?> /> <?php _e('Female'); ?>
            </td>
        </tr>
        <tr>
            <th>
                <label for="dob"><?php _e('Date of birth'); ?></label>
            </th>
            <td>
                <input type="text" id="dob_datepicker" name="profile_register[dob]" class="regular-text datepicker hasDatepicker" value="<?php echo (isset($meta['dob'])) ? $meta['dob'][0] : ''; ?>" />              
                
            </td>
        </tr>
        <tr>
            <th>
                <label for="address"><?php _e('Address'); ?></label>
            </th>
            <td>
                <textarea class="address_textarea" cols="25" rows="3" name="profile_register[address]"><?php echo (isset($meta['address'])) ? $meta['address'][0] : ''; ?></textarea>
            </td>
        </tr>
<!--        <tr>-->
<!--            <th>-->
<!--                <label for="notification">--><?php //_e('Sign up for notifications'); ?><!--</label>-->
<!--            </th>-->
<!--            <td>-->
<!--                <input class="notification_checkbox" name="profile_register[user_meta_mailchimp]" type="checkbox" value="1" --><?php //echo (isset($meta['user_meta_mailchimp']) && $meta['user_meta_mailchimp'][0] == 1) ? 'checked="checked"' : ''; ?><!-- />-->
<!--            </td>-->
<!--        </tr>-->
        <tr>
            <th>
                <label for="language"><?php _e('Preferred Language'); ?></label>
            </th>
            <td>
                <select class="language_selectbox" name="profile_register[language]" >
                    <option value="en_US" <?php echo (isset($meta['language']) && $meta['language'][0] == 'en_US') ? 'selected="selected"' : ''; ?>><?php _e('English'); ?></option>
                    <option value="vi_VN" <?php echo (isset($meta['language']) && $meta['language'][0] == 'vi_VN') ? 'selected="selected"' : ''; ?>><?php _e('Vietnamese'); ?></option>
                </select>
            </td>
        </tr>
    </table>
    <?php
}
add_action('personal_options_update', 'numediaweb_custom_user_profile_update');
add_action('edit_user_profile_update', 'numediaweb_custom_user_profile_update');
function numediaweb_custom_user_profile_update($user_id){
    $profile_register = (array) $_POST['profile_register'];
    if(!isset($profile_register['user_meta_mailchimp'])){
        $profile_register['user_meta_mailchimp'] = 0;
    }    
    foreach ($profile_register as $key=>$value):
        update_user_meta($user_id, $key, $value);
    endforeach;
   
}
