<?php

function learndash_show_enrolled_courses($user) {
    $courses = get_pages("post_type=sfwd-courses");
    
    // Bai Nguyen
    // $secretCoursesBought = list_secret_courses($user->ID);
    
    $enrolled = array();
    $notenrolled = array();
    $secretCoursesPending = array();
    ?>
    <table class="form-table">
        <tr>
            <th> <h3><?php _e('Enrolled Courses', 'learndash'); ?></h3></th>
    <td>
        <ol>
            <?php
            foreach ($courses as $course) {
                if (sfwd_lms_has_access($course->ID, $user->ID)) {
                    $since = ld_course_access_from($course->ID, $user->ID);
                    $since = empty($since) ? "" : "Since: " . date("m/d/Y H:i:s", $since);
                    if (empty($since)) {
                        $since = learndash_user_group_enrolled_to_course_from($user->ID, $course->ID);
                        $since = empty($since) ? "" : "Since: " . date("m/d/Y H:i:s", $since) . " (Group Access)";
                    }
                    $issecret = get_post_meta($course->ID, 'wpcf-is-secret', true);
                    $typeCourse = '';
                    if ($issecret) {
                        $typeCourse = ' (' . __('Secret Course', 'learndash') . ')';
                    }
                    echo "<li><a href='" . get_permalink($course->ID) . "'>" . $course->post_title . $typeCourse . "</a> " . $since . "</li>";
                    $enrolled[] = $course;
                } else {
                    // Hoang Bui
                    $meta_course = get_post_meta($course->ID, '_sfwd-courses', true);
                    $pending_list = $meta_course['sfwd-courses_course_pending_list'];
                    $pending_list = explode(',', $pending_list);
                    if (in_array($user->ID, $pending_list)) {
                        $secretCoursesPending[] = $course;
                    }
                    $notenrolled[] = $course;
                    /*
                     * Bai Nguyen                      
                      if (in_array($course->ID, $secretCoursesBought)) {
                      $secretCoursesPending[] = $course;
                      } */
                }
            }
            ?>
        </ol>
    </td>
    </tr>
    <tr>
        <th><h3><?php _e('Secret Courses Pending', 'learndash'); ?></h3></th>
    <td>
        <?php if (!empty($secretCoursesPending)) { ?>
            <ol>
                <?php
                foreach ($secretCoursesPending as $course) {
                    echo "<li><input type='checkbox' name='reject_courses[]' value='".$course->ID."' /> Reject: &nbsp;"
                            . "<a href='" . get_permalink($course->ID) . "'>" . $course->post_title . "</a> " . $since . "</li>";
                }
                ?>

            </ol>
        <?php }
        ?>
    </td>
    <?php
    if (current_user_can('manage_options')) {
        ?>
        <tr>			
            <th> <h3><?php _e("Enroll a Course", "learndash"); ?></h3></th>
        <td>
            <select name="enroll_course">
                <option value=''> -- Select a Course --</option>
                <?php
                foreach ($notenrolled as $course) {
                    $issecret = get_post_meta($course->ID, 'wpcf-is-secret', true);
                    $typeCourse = '';
                    if ($issecret) {
                        $typeCourse = ' (' . __('Secret Course', 'learndash') . ')';
                    }
                    echo "<option value='" . $course->ID . "'>" . $course->post_title . $typeCourse . "</option>";
                }
                ?>
            </select>
        </td>
        </tr>
        <tr>			
            <th> <h3><?php _e("Unenroll a Course", "learndash"); ?></h3></th>
        <td>
            <select name="unenroll_course">
                <option value=''> -- Select a Course --</option>
                <?php
                foreach ($enrolled as $course) {
                    echo "<option value='" . $course->ID . "'>" . $course->post_title . "</option>";
                }
                ?>
            </select>
        </td>
        </tr>			
    <?php } ?>
    </table>
    <?php
}

function learndash_save_enrolled_courses($user_id) {

    if (!current_user_can('manage_options'))
        return FALSE;

    $reject_courses = (array) $_POST['reject_courses'];
    foreach ($reject_courses as $reject_course):
        ld_update_course_pending($user_id, $reject_course, true);
    endforeach;
    $enroll_course = $_POST['enroll_course'];
    $unenroll_course = $_POST['unenroll_course'];
    
    if (!empty($enroll_course)) {
        $meta = ld_update_course_access($user_id, $enroll_course);
        ld_update_course_pending($user_id, $enroll_course, true);
    }
    if (!empty($unenroll_course)) {
        $meta = ld_update_course_access($user_id, $unenroll_course, $remove = true);
    }
}

add_action('show_user_profile', 'learndash_show_enrolled_courses');
add_action('edit_user_profile', 'learndash_show_enrolled_courses');

add_action('personal_options_update', 'learndash_save_enrolled_courses');
add_action('edit_user_profile_update', 'learndash_save_enrolled_courses');
