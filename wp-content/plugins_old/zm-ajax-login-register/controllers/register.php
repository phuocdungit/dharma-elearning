<?php

/**
 * This is file is responsible for custom logic needed by all templates. NO
 * admin code should be placed in this file.
 */
Class ajax_login_register_Register Extends AjaxLogin {

    /**
     * Run the following methods when this class is loaded
     */
    public function __construct() {
        add_action('init', array(&$this, 'init'));
        add_action('wp_footer', array(&$this, 'footer'));

        parent::__construct();
    }

    /**
     * During WordPress' init load various methods.
     */
    public function init() {
        add_action('wp_ajax_nopriv_register_submit', array(&$this, 'register_submit'));
        add_action('wp_ajax_register_submit', array(&$this, 'register_submit'));

        add_shortcode('ajax_register', array(&$this, 'register_shortcode'));
    }

    /**
     * Any additional code to be ran during wp_footer
     *
     * If the user is not logged in we display the hidden jQuery UI dialog containers
     */
    public function footer() {
        load_template(plugin_dir_path(dirname(__FILE__)) . 'views/register-dialog.php');
    }

    /**
     * Registers a new user, checks if the user email or name is
     * already in use.
     *
     * @uses check_ajax_referer() http://codex.wordpress.org/Function_Reference/check_ajax_referer
     * @uses get_user_by_email() http://codex.wordpress.org/Function_Reference/get_user_by_email
     * @uses get_user_by() http://codex.wordpress.org/Function_Reference/get_user_by
     * @uses wp_create_user() http://codex.wordpress.org/Function_Reference/wp_create_user
     *
     * @param $login
     * @param $password
     * @param $email
     * @param $is_ajax
     */
    public function register_submit($login = null, $password = null, $email = null, $is_ajax = true) {
        if ($is_ajax)
            check_ajax_referer('register_submit', 'security');
            $emailName = preg_replace('/([^\pL \pN \.\ \@\ \_\ ]+)/u', '', strip_tags($_POST['email']));
            // TODO consider using wp_generate_password( $length=12, $include_standard_special_chars=false );
            // and wp_mail the users password asking to change it.
            
            
            require_once(ABSPATH . 'wp-admin/admin-functions.php');
            if (class_exists('ReallySimpleCaptcha')) { //check if the Really Simple Captcha class is available
                $captcha = new ReallySimpleCaptcha();
                if (!$captcha->check($_POST['captcha_prefix'], $_POST['captcha_code'])) {
                    $msg = $this->status('captcha_invalid');
                    $captcha->remove('registercaptcha');
                    if ($is_ajax) {
                        wp_send_json($msg);
                    } else {
                        return $msg;
                    }
                }
            }
            $user = array(
                'email' => empty($emailName) ? $email : sanitize_text_field($emailName),
                'password' => empty($_POST['password']) ? $password : sanitize_text_field($_POST['password']),
                'fb_id' => empty($_POST['fb_id']) ? false : sanitize_text_field($_POST['fb_id'])
            );
            $user['login'] = $user['email'];
            $valid['email'] = $this->validate_email($user['email'], false);
            $valid['username'] = $this->validate_username($user['login'], false);
            $user_id = null;
            // if session is not exits
            if(!$_SESSION['emailActive']):
                $userOld = get_user_by('email', $_SESSION['emailActive']);
                if ($valid['username']['code'] == 'error') {
                    $msg = $this->status('invalid_username'); // invalid user
                } else if ($valid['email']['code'] == 'error') {
                    $msg = $this->status('invalid_username'); // invalid user
                } else {
                    $user_id = wp_create_user($user['login'], $user['password'], $user['email']);

                    do_action('zm_ajax_login_after_successfull_registration', $user_id);

                    if (!is_wp_error($user_id)) {

                        update_user_meta($user_id, 'show_admin_bar_front', 'false');
                        update_user_meta($user_id, 'fb_id', $user['fb_id']);

                        if (is_multisite()) {
                            $this->multisite_setup($user_id);
                        }
        //wp_update_user( array( 'ID' => $user_id, 'role' => 'subscriber' ) );
                        $firstName = $this->character($_POST['first_name']);
                        $displayName =$this->character($_POST['display_name']);
                        $lastName = $this->character($_POST['last_name']);
                        $address = $this->character($_POST['address']);
                        $password = trim($_POST['password']);
                        update_user_meta($user_id, 'user_meta_active_pass', $password);
                        update_user_meta($user_id, 'user_meta_refuge_name', $displayName);
                        update_user_meta($user_id, 'address', $address);
                        $update = wp_update_user(array(
                            'ID' => $user_id,
                            'first_name' => $firstName,
                            'display_name'=>$displayName,
                            'last_name'=>$lastName
                        ));
                        add_user_meta($user_id, 'gender', $_POST['gender'], true);
                        add_user_meta($user_id, 'dob', $_POST['dob'], true);
                        add_user_meta($user_id, 'language', $_POST['language'], true);
                        update_user_meta($user_id, 'user_meta_mailchimp',$_POST['_mc4wp_subscribe_registration_form']);
                        
                        //$wp_signon = wp_signon(array('user_login' => $user['login'], 'user_password' => $user['password'], 'remember' => true), false);
                        if($update){
                            $_SESSION['emailActive'] = $emailName;
                            $msg = $this->status('success_registration'); // success
                            resend_activation_code($user_id);
                        }else{
                            $msg = $this->status('error_registration');
                        }
                    } else {
                        $msg = $this->status('invalid_username'); // invalid user
                    }
                }
            else:
                if($_POST['email']==$_SESSION['emailActive']):
                    $usercheck = get_user_by('email', $_SESSION['emailActive']);
                    if($usercheck):
                       update_info_register();
                       $msg = $this->status('update_registration');
                    
                    else:
                         register_new();
                         $msg = $this->status('success_registration');
                    endif;
                else:
                    if ($valid['username']['code'] == 'error') :
                        $msg = $this->status('invalid_username'); // invalid user
                    elseif(($valid['email']['code'] == 'error')) :
                        $msg = $this->status('invalid_username'); // invalid user
                    else:
                        register_new();
                        $msg = $this->status('success_registration');
                     endif;
                endif;
            endif;
            if ($is_ajax) {
                wp_send_json($msg);
            } else {
                return $msg;
            }
    }
    public function character($str){
        preg_replace('/([^\pL \pN \.\ \,\ ]+)/u', ' ', strip_tags($str));
        return $str;
    }
    /**
     * Load the login shortcode
     */
    public function register_shortcode() {
        ob_start();
        load_template(plugin_dir_path(dirname(__FILE__)) . 'views/register-form.php');
        return ob_get_clean();
    }

    public function multisite_setup($user_id = null) {
        return add_user_to_blog(get_current_blog_id(), $user_id, 'subscriber');
    }

// Create Facebook User
//
    public function create_facebook_user($user = array()) {

        $user['user_pass'] = wp_generate_password();
        $user['user_registered'] = date('Y-m-d H:i:s');
        $user['role'] = "subscriber";

        $user_id = wp_insert_user($user);

        if (is_wp_error($user_id)) {
            return $user_id;
        } else {
// Store random password as user meta
            $meta_id = add_user_meta($user_id, '_random', $user['user_pass']);

// Setup this user if this is Multisite/Networking
            if (is_multisite()) {
                $this->multisite_setup($user_id);
            }
        }

        return get_user_by('id', $user_id);
    }

}

new ajax_login_register_Register;
