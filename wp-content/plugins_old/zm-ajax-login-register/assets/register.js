jQuery(document).ready(function($) {

    /**
     * Close our dialog box when the user clicks
     * cancel/exit/close.
     */
    $(document).on('click', '.ajax-login-register-container .cancel', function() {
        $(this).closest('.ajax-login-register-container').dialog('close');
    });

    window.zMAjaxLoginRegisterDialog = {
        open: function() {
            $('#ajax-login-register-dialog').dialog('open');
            var $page = _ajax_login_settings.pageholder;
            var data = {
                action: 'load_template',
                template: 'register-form',
                page: $page,
                referer: 'register_form',
                security: $('#ajax-login-register-dialog').attr('data-security')
            };

            $.ajax({
                data: data,
                type: "POST",
                url: _ajax_login_settings.ajaxurl,
                beforeSend: function() {
                    $("#ajax-login-register-target").parent().append('<div class="ajaxloading"></div>');
                },
                success: function(msg) {
                    $("#ajax-login-register-target").fadeIn().html(msg); // Give a smooth fade in effect
                    jQuery('.ajaxloading').remove();
                    App.initValidate();
                    if (msg == '-1') {
                                jQuery('.ajaxloading').remove();
                        $("#ajax-login-register-login-target").fadeIn().html('Sorry, we seem to encounter some turbulence. Please kindly wait a moment while we reload the page to fix this.');
                        window.location.reload();
                        return;
                    }                    
                    jQuery('#register').click(function() {
                        jQuery(this).val('Loading...');
                    });
                }
            });
        }
    };

    if (_ajax_login_settings.register_handle.length) {
        $(document).on('click', _ajax_login_settings.register_handle, function(event) {
            event.preventDefault();
            _ajax_login_settings.pageholder = jQuery(this).data('page');
            zMAjaxLoginRegisterDialog.open();
        });
    }


    /**
     * Confirms that two input fields match
     */
    $(document).on('keyup', '.user_confirm_password', function() {
        $form = $(this).parents('form');

        if ($(this).val() == '') {
            $('.register_button', $form).attr('disabled', true);
            $('.register_button', $form).animate({opacity: 0.5});
        } else {
            $('.register_button', $form).removeAttr('disabled');
            $('.register_button', $form).animate({opacity: 1});
        }
    });


    /**
     * Our form is loaded via AJAX, so we need to attach our event to the document.
     * When the form is submitted process the AJAX request.
     */
    $(document).on('submit', '.register_form', function(event) {
        event.preventDefault();
        var $url = jQuery(this).attr("data-link");
        passwords_match = zMAjaxLoginRegister.confirm_password('.user_confirm_password');

        if (passwords_match.code == 'error') {
            ajax_login_register_show_message($(this), msg);
        } else {
            $.ajax({
                data: "action=register_submit&" + $(this).serialize(),
                dataType: 'json',
                type: "POST",
                url: _ajax_login_settings.ajaxurl,
                beforeSend: function() {
                    $("#ajax-login-register-target").parent().append('<div class="ajaxloading"></div>');
                },
                success: function(msg) {
                    if (msg.code == 'captcha_invalid') {
                        $('.form-wrapper').find('.error').remove();
                        $('.form-wrapper').prepend('<p class="error" style="color: #E32619;font-size: 13px;font-style: italic">' + msg.description + '</p>');
                        $("#ajax-login-register-target").parent().find('.ajaxloading').remove();
                        return;
                    }
                    if (msg.code == 'success_registration') {
                        $('.form-wrapper').find('.error').remove();
						if((msg.type!='undefined') && msg.type=='update'){
							alert( msg.description);
						}else{
						$('.form-wrapper').prepend('<p style="color:#2ea3f2">' + msg.description + '</p>');
						}
                        
                        $("#ajax-login-register-target").parent().find('.ajaxloading').remove();
                    }
                    if (msg.code == 'update_registration') {
                        $('.form-wrapper').find('.error').remove();
                        $('.form-wrapper').prepend('<p style="color:#2ea3f2">' + msg.description + '</p>');
                        $("#ajax-login-register-target").parent().find('.ajaxloading').remove();
                    }
                    ajax_login_register_show_message($(this), msg);
                    
                    if (msg == '-1') {
                         jQuery('.ajaxloading').remove();
                        $('#ajax-login-register-login-target').fadeIn().html('Sorry, we seem to encounter some turbulence. Please kindly wait a moment while we reload the page to fix this.');
                        window.location.reload();
                        return;
                    }                    
                }
            });
        }
    });
    $(document).on('click', '.already-registered-handle', function() {
        $('#ajax-login-register-dialog').dialog('close');
        zMAjaxLoginDialog.open();
    });
});