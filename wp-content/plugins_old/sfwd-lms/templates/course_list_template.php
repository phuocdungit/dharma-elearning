<?php
	the_title( '<h2 class="ld-entry-title entry-title"><a href="' . get_permalink() . '" title="' . the_title_attribute( 'echo=0' ) . '" rel="bookmark">', '</a></h2>' ); 
?>
<div class="ld-entry-content entry-content">
	<?php 
            $user_ID = get_current_user_id(); 
            the_post_thumbnail(); 
            global $more; $more = 0;
            get_the_content_course();
            //the_excerpt();
            the_content(__('Read more.', 'learndash')); 
                echo'<p><a href="' . get_permalink() . '" class="btn btn-primary" title="' . the_title_attribute( 'echo=0' ) . '" rel="bookmark">Details</a> &nbsp;&nbsp;';
            if($user_ID){
                echo'<a href="' . get_permalink() . '" class="btn btn-info" title="' . the_title_attribute( 'echo=0' ) . '" rel="bookmark">Launch</a></p>';
            }else{
                echo'<a href="' . get_permalink() . '" class="btn btn-danger" title="' . the_title_attribute( 'echo=0' ) . '" rel="bookmark">Enrol</a></p>';
            }
	?>
</div>

