<?php
/*
  Template Name: Profile Template
 */
$user = wp_get_current_user();
get_header();
if (!is_user_logged_in()):
    ?>
    <div id="main-content">
        <div id="content-area" class="clearfix post-course">
            <div class="entry-content" style="text-align:center">
                <div class="mycontent-loginform">
                    <?php
                    $args = array(
                        'echo' => true,
                        'redirect' => site_url($_SERVER['REQUEST_URI']),
                        'form_id' => 'loginform',
                        'label_username' => __('Email'),
                        'label_password' => __('Password'),
                        'label_remember' => __('Remember Me'),
                        'label_log_in' => __('Log In'),
                        'id_username' => 'user_login',
                        'id_password' => 'user_pass',
                        'id_remember' => 'rememberme',
                        'id_submit' => 'wp-submit',
                        'remember' => true,
                        'value_username' => NULL,
                        'value_remember' => false
                    );
                    wp_login_form($args);
                    ?>
                    <?php echo '<a class="register-link" id="register-link" data-href="' . wp_registration_url() . '" href="javascript:;" title="' . __('Register') . '">' . __('Register') . '</a>'; ?> | <a href="<?php echo wp_lostpassword_url(); ?>" title="Lost your password?"><?php _e('Lost your password ?', 'Divi'); ?></a>
                </div>
            </div>
        </div>
        <?php get_footer(); ?>
    </div>
    <?php
else:
//    wp_redirect('/my-profile');
//return;
    $is_page_builder_used = et_pb_is_pagebuilder_used(get_the_ID());
    $meta = get_user_meta($user->ID);
//    echo '<pre>';
//    print_r($meta);
//    echo '</pre>';
//    exit;
    $userData = get_userdata($user->ID);
    global $wpdb;
    ?>

    <div id="main-content">
        <?php if (!$is_page_builder_used) : ?>
            <div class="container">
                <div id="content-area" class="clearfix">
                    <div id="left-area" class="link"  link="<?php echo admin_url("admin-ajax.php"); ?>" id-user="<?php echo $user->ID; ?>">
                    <?php endif; ?> 
                    <div class="et_pb_column et_pb_column_4_4">
                        <div class="et_pb_row_inner">
                            <div class="et_pb_column et_pb_column_4_4 et_pb_column_inner">
                                <div class="et_pb_tabs">
                                    <ul class="et_pb_tabs_controls clearfix">
                                        <li class="<?php if ($_SESSION['activeTab'] == 1 || !$_SESSION['activeTab']) echo "et_pb_tab_active"; ?> tab" data-active='1'><a href="#"><?php _e('General', 'Divi'); ?></a></li>
                                        <li class="<?php if ($_SESSION['activeTab'] == 2) echo "et_pb_tab_active"; ?> tab" data-active='2'><a href="#"><?php _e('Refuge', 'Divi'); ?></a></li>
                                        <li class="<?php if ($_SESSION['activeTab'] == 3) echo "et_pb_tab_active"; ?> tab" data-active='3'><a href="#"><?php _e('Initiations received', 'Divi'); ?></a></li>
                                        <li class="<?php if ($_SESSION['activeTab'] == 4) echo "et_pb_tab_active"; ?> tab" data-active='4'><a href="#"><?php _e('Practices', 'Divi'); ?></a></li>
                                        <li class="<?php if ($_SESSION['activeTab'] == 5) echo "et_pb_tab_active"; ?> tab" data-active='5'><a href="#"><?php _e('Ngöndro', 'Divi'); ?></a></li>
                                    </ul>
                                    <form action="<?php echo admin_url('admin-ajax.php'); ?>" method="POST" class="validate" enctype="multipart/form-data">
                                        <?php wp_nonce_field('process_profile', 'security-code-here'); ?>
                                        <input name="action" value="process_profile" type="hidden">
                                        <input name="tab" id="tabActive" value="<?php
                                        if ($_SESSION['activeTab'])
                                            echo $_SESSION['activeTab'];
                                        else
                                            echo 1;
                                        ?>" type="hidden" />
                                        <div class="et_pb_all_tabs">
                                            <div class="et_pb_tab clearfix <?php if ($_SESSION['activeTab'] == 1 || !$_SESSION['activeTab']) echo "et_pb_active_content"; ?>">
                                                <div class="general-left et_pb_column_1_4">
                                                    <?php
                                                    echo get_avatar($user->ID);
                                                    ?>
                                                    <center>
                                                        <input type="file" class="picture hide" accept="image/*" name="wp-user-avatar" value=""/>
                                                        <input type="button" class="button btn-info change-picture" value="<?php _e('Change Picture', 'Divi'); ?>" />
                                                    </center>
                                                </div>
                                                <div class="general-right et_pb_column_4_2">
                                                    <p>
                                                        <label><?php _e('First Name', 'Divi'); ?></label>
                                                        <input type="text" class="input-text" name="first-name" placeholder="<?php _e('First name...', 'Divi'); ?>" value="<?php echo $meta['first_name'][0]; ?>" />
                                                    </p>
                                                    <p>
                                                        <label><?php _e('Last Name', 'Divi'); ?></label>
                                                        <input type="text" class="input-text" name="last-name" placeholder="<?php _e('Last name...', 'Divi'); ?>" value="<?php echo $meta['last_name'][0]; ?>" />
                                                    </p>
                                                    <p>
                                                        <label><?php _e('Email', 'Divi'); ?></label>
                                                        <input type="text" class="input-text"  disabled="disable" placeholder="<?php _e('Email ...', 'Divi'); ?>" value="<?php echo $user->user_email; ?>"/>
                                                    </p>
                                                    <p>
                                                        <label><?php _e('Display Name', 'Divi'); ?><span class="note"><?php _e(' (this name will show in questions you post in the Q&A forums)', 'Divi'); ?></span></label>
                                                        <input type="text" class="input-text"  name="fullname" placeholder="<?php _e('Display Name ...', 'Divi'); ?>" value="<?php echo $user->display_name; ?>"/>
                                                    </p>
                                                    <p>
                                                        <label>
                                                            <input type="checkbox" name="denied-dmail"<?php echo ($meta['user_meta_denied_dmail'][0] == 1) ? 'checked="checked"' : '' ?>  value="1"/>
                                                            <?php _e('Allow to be found in dMail by display name and email address', 'Divi'); ?>                                                            
                                                        </label>

                                                    </p>
                                                    <p>
                                                        <label><?php _e('Date of birth', 'Divi'); ?></label>
                                                        <input type="text" name="dob" class="input-text datepicker" placeholder="dd/mm/yy" value="<?php echo $meta['dob'][0]; ?>"/>
                                                    </p>
                                                    <p>
                                                        <label><?php _e('Gender', 'Divi'); ?></label>
                                                        <input type="radio" name="gender" value="1" <?php echo ($meta['gender'][0] == 1) ? 'checked="checked"' : '' ?>/> <?php _e('Male', 'Divi') ?>
                                                        <input type="radio" name="gender" value="2" <?php echo ($meta['gender'][0] == 2) ? 'checked="checked"' : '' ?>/> <?php _e('Female', 'Divi') ?>

                                                    </p>
                                                    <!--<p>-->
                                                    <!--                                                        <label><?php //_e('Are you subscribed to the newsletter?', 'Divi');      ?> -->
                                                    <input class="notification_checkbox" name="subscribe" type="hidden" value="<?php echo $meta['user_meta_mailchimp'][0] ?>"  />
                                                                                                                <!--<span class="note"><?php //_e('(This reflects your subscription status, please click on the box if you want to change your status, then "Update" below).', 'Divi');      ?></span></label>-->
                                                    <!--</p>-->
                                                    <!--<p>-->
                                                        <!--<label><?php //_e('Address', 'Divi');      ?></label>-->
                                                    <input type="hidden" name="address" placeholder="<?php _e("Enter address", "Divi"); ?>" class="input-text" value="<?php echo $meta['address'][0]; ?>" />
                                                    <!--</p>-->
    <!--                                                    <p>
                                                        <label><?php //_e('Preferred Language', 'Divi');      ?></label>
                                                        <select class="language_selectbox" name="language" >
                                                            <option value="en_US" <?php //echo (isset($meta['language']) && $meta['language'][0] == 'en_US') ? 'selected="selected"' : '';      ?>><?php //_e('English');      ?></option>
                                                            <option value="vi_VN" <?php //echo (isset($meta['language']) && $meta['language'][0] == 'vi_VN') ? 'selected="selected"' : '';      ?>><?php //_e('Vietnamese');      ?></option>
                                                        </select>
                                                    </p>-->

                                                    <p class="pull-right">
                                                        <input class="btn-info save-profile btn-save" id="update_profile" value="<?php _e('Update', 'Divi'); ?>" type="submit">
                                                    </p>
                                                </div>
                                            </div> <!-- .et_pb_tab -->                            
                                            <div class="et_pb_tab clearfix  et-pb-active-slide <?php if ($_SESSION['activeTab'] == 2) echo "et_pb_active_content"; ?>">
                                                <div class="refuge">
                                                    <p class="profile-title">
                                                        <?php _e("Please enter the details of your (main) refuge:", "Divi"); ?>
                                                    </p>
                                                    <p>
                                                        <label><?php _e('Refuge Name', 'Divi'); ?></label>
                                                        <input type="text" class="input-text" name="refugeName" placeholder="<?php _e('Refuge Name ...', 'Divi'); ?>" value="<?php echo $meta['user_meta_refuge_name'][0]; ?>"/>
                                                    </p>
                                                    <p>
                                                        <label><?php _e('Year Of Refuge', 'Divi'); ?></label>
                                                        <select name="refugeYear" class="input-text">
                                                            <?php
                                                            $year = range(date('Y'), 1900);
                                                            if ($meta['user_meta_refuge_year'][0] == 0) {
                                                                $showYear = 'Not sure';
                                                            } else {
                                                                $showYear = $meta['user_meta_refuge_year'][0];
                                                            }
                                                            if ($meta['user_meta_refuge_year'][0] != ""):
                                                                echo "<option value=" . $meta['user_meta_refuge_year'][0] . ">" . $showYear . "</option>";
                                                            endif;
                                                            echo "<option value=''>";
                                                            _e("Select Year", "Divi");
                                                            echo "</option>";
                                                            echo "<option value='0'>";
                                                            _e("Not sure", "Divi");
                                                            echo "</option>";
                                                            foreach ($year as $key):
                                                                echo "<option value=" . $key . ">" . $key . "</option>";
                                                            endforeach;
                                                            ?>
                                                        </select>
                                                    </p>
                                                    <p>
                                                        <label><?php _e('Place Of Refuge', 'Divi'); ?></label>
                                                        <input type="text" name="refugePlace" value="<?php echo $meta['user_meta_refuge_place'][0]; ?>" class="input-text" placeholder="<?php _e('Place Of Refuge', 'Divi'); ?>" />
                                                    </p>
                                                    <p>
                                                        <label><?php _e('Refuge Teacher', 'Divi'); ?></label>
                                                        <select name="refugeTeacher" class="show-teacher input-text">
                                                            <?php
                                                            if ($meta['user_meta_refuge_teacher'][0] != "" && $meta['user_meta_refuge_teacher'][0] != 0) {
                                                                $args = array(
                                                                    'post_type' => 'teacher',
                                                                    'orderby' => array('post_title' => 'ASC'),
                                                                    'posts_per_page' => -1,
                                                                    'post__not_in' => explode(',', $meta['user_meta_refuge_teacher'][0])
                                                                );
                                                                $query = new WP_Query($args);
                                                                $row = wp_get_single_post($meta['user_meta_refuge_teacher'][0]);
                                                                echo "<option value='" . $row->ID . "' selected>" . $row->post_title . "</option>";
                                                            } elseif ($meta['user_meta_refuge_teacher'][0] == 0) {
                                                                $args = array(
                                                                    'post_type' => 'teacher',
                                                                    'orderby' => array('post_title' => 'ASC'),
                                                                    'posts_per_page' => -1
                                                                );
                                                                $query = new WP_Query($args);
                                                            }
                                                            echo "<option value='0'>";
                                                            _e("Select Teacher", "Divi");
                                                            echo "</option>";
                                                            foreach ($query->posts as $teacher):
                                                                echo "<option value='" . $teacher->ID . "'>" . $teacher->post_title . "</option>";
                                                            endforeach;
                                                            ?>
                                                        </select>
                                                    <div class="box-teacher">
                                                        <span class="add-teacher"><i class="fa fa-plus"></i> <?php _e('Add teacher…', 'Divi'); ?></span>
                                                        <p class="hide">
                                                            <input type="text" class="form-control teacher-name" placeholder="<?php _e('Teacher Name...', 'Divi'); ?>" />
                                                            <input type="button"  class="button btn-info insert-teacher" value="<?php _e('Add item', 'Divi'); ?>" />
                                                        </p>
                                                    </div>
                                                    </p>
                                                    <p class="pull-right">
                                                        <input type="submit" class="btn-save btn-info save-profile" name="person[0][first_name]"  value="<?php _e('Save', 'Divi'); ?>"  />
                                                        <input type="submit" class="btn-save btn-info hide" name="person[0][last_name]"  value="1"  />
                                                    </p>
                                                </div>

                                            </div> <!-- .et_pb_tab -->
                                            <div class="et_pb_tab clearfix <?php if ($_SESSION['activeTab'] == 3) echo "et_pb_active_content"; ?>">
                                                <div class="practice">
                                                    <p class="profile-title">
                                                        <?php _e('Please enter the details about initiations you have received. Please proceed in order from left to right for each initiation, then click on "Apply" before entering the next initiation.', "Divi"); ?>
                                                    </p>
                                                    <div>
                                                        <label><?php _e('1.Practice', 'Divi'); ?></label>
                                                        <select id="practice-value" name="practice-list" class="select-practice input-text"> <!--multiple name="practice[]"-->
                                                            <?php
                                                            $listPractice = array(
                                                                'post_type' => 'practic',
                                                                'orderby' => array('post_title' => 'ASC'),
                                                                'posts_per_page' => -1
                                                            );
                                                            $practices = new WP_Query($listPractice);
                                                            echo"<option value=''>Select Practice</option>";
                                                            foreach ($practices->posts as $prac):
                                                                echo "<option value='" . $prac->ID . "'>" . $prac->post_title . "</option>";
                                                            endforeach;
                                                            ?>
                                                        </select>
                                                        <span class="practice-item"><i class="fa fa-plus"></i> <?php _e('Add practice…', 'Divi'); ?></span>
                                                        <div class="show-practice hide">
                                                            <input type="text" class="form-control practice-name" placeholder="<?php _e('Practice Name...', 'Divi'); ?>" />
                                                            <input type="button" class="button btn-info insert-practice" value="<?php _e('Add item', 'Divi'); ?>" />
                                                        </div>
                                                    </div>
                                                    <div>
                                                        <label><?php _e('2.Teacher', 'Divi'); ?></label>
                                                        <select class="show-teacher input-text" name="teacher-list" id="teacher-list"><!--name="received"-->
                                                            <?php
                                                            $args = array(
                                                                'post_type' => 'teacher',
                                                                'orderby' => array('post_title' => 'ASC'),
                                                                'posts_per_page' => -1
                                                            );
                                                            $query = new WP_Query($args);
                                                            echo"<option value=''>Select Teacher</option>";
                                                            foreach ($query->posts as $practice):
                                                                echo "<option value='" . $practice->ID . "'>" . $practice->post_title . "</option>";
                                                            endforeach;
                                                            ?>
                                                        </select>
                                                        <div class="box-teacher">
                                                            <span class="add-teacher"><i class="fa fa-plus"></i> <?php _e('Add teacher...', 'Divi'); ?></span>
                                                            <p class="hide">
                                                                <input type="text" class="form-control teacher-name" placeholder="<?php _e('Teacher Name...', 'Divi'); ?>" />
                                                                <input type="button"  class="button btn-info insert-teacher" value="<?php _e('Add item', 'Divi'); ?>" />
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div>
                                                        <label><?php _e('3.Year', 'Divi'); ?></label>
                                                        <select class="input-text" name="year-list" id="year-list">
                                                            <?php
                                                            $year = range(date('Y'), 1900);
                                                            echo"<option value=''>Select Year</option>";
                                                            echo"<option value='0'>Not sure</option>";
                                                            foreach ($year as $key):
                                                                echo"<option value=" . $key . ">" . $key . "</option>";
                                                            endforeach;
                                                            ?>
                                                        </select>
                                                    </div>

                                                    <div class="et_pb_column et_pb_column_4_4 et_pb_column_inner">
                                                        <div class="pull-right" style="margin-bottom: 2%;">
                                                            <span class="btn-save btn-info save-received"><?php _e('Apply', 'Divi'); ?></span>
                                                        </div>
                                                        <table class="dataTable table" id="show-received" data-link="<?php echo admin_url('admin-ajax.php'); ?>">
                                                            <thead>
                                                                <tr>
                                                                    <td data-sort-initial="true"><?php _e('Practice', 'Divi'); ?></td>                                                            
                                                                    <td><?php _e('Teacher', 'Divi'); ?></td>
                                                                    <td><?php _e('Year', 'Divi'); ?></td>                                                            
                                                                    <td data-sort-ignore="true" width="140px"><?php _e('Action', 'Divi'); ?></td>                                                            
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php
                                                                $user = wp_get_current_user();
                                                                $table = $wpdb->prefix . "initiations_received";
                                                                echo initiation_received($table, $user->ID);
                                                                ?>
                                                            </tbody>
                                                        </table>
                                                        <p class="pull-right clearfix">
                                                            <input type="submit" class="btn-save btn-info"  value="<?php _e('Save', 'Divi'); ?>"  />
                                                        </p>
                                                    </div>
                                                </div>
                                            </div> <!-- .et_pb_tab -->
                                            <div class="et_pb_tab clearfix <?php if ($_SESSION['activeTab'] == 4) echo "et_pb_active_content"; ?>">
                                                <p class="profile-title">
                                                    <?php _e("Please enter which practices you doing regularly (even if only once a month) and which practices you consider you main practices right now.", "Divi"); ?>
                                                </p>
                                                <table class="listPractice table">
                                                    <?php
                                                    if ($meta['user_meta_practice'][0] != "") {
                                                        $args = array(
                                                            'post_type' => 'practic',
                                                            'posts_per_page' => -1,
                                                            'post__not_in' => explode(',', $meta['user_meta_practice'][0]),
                                                            'orderby' => array('post_title' => 'ASC')
                                                        );
                                                        $query = new WP_Query($args);
                                                    } else {
                                                        $args = array(
                                                            'post_type' => 'practic',
                                                            'posts_per_page' => -1,
                                                            'orderby' => array('post_title' => 'ASC')
                                                        );
                                                        $query = new WP_Query($args);
                                                    }
                                                    $args2 = array(
                                                        'post_type' => 'practic',
                                                        'posts_per_page' => -1,
                                                        'orderby' => array('post_title' => 'ASC'),
                                                        'post__in' => explode(',', $meta['user_meta_practice'][0])
                                                    );
                                                    $result = new WP_Query($args2);
                                                    //wpcf-main-practices
                                                    echo "<thead>
                                                        <tr>
                                                            <td width='5%'></td>
                                                            <td width='60%'>"; //<input name='checkAll' id='select_all_main_practice' type='checkbox' />
                                                    _e('Practices', 'Divi');
                                                    echo "</td>
                                                            <td align='center'>"; //<input name='checkAll' id='select_all_practice' type='checkbox' />
                                                    _e("Currently practicing", "Divi");
                                                    echo "</td>
                                                            <td align='center'>";
                                                    _e("Main practices", "Divi");
                                                    echo "</td>    
                                                    </tr>";
                                                    echo "</thead>";
                                                    $arrayMain = array();
                                                    $arrayPractice = array();
                                                    foreach ($result->posts as $key):
                                                        $postMeta = get_post_meta($key->ID, 'wpcf-main-practice', true);
                                                        if ($postMeta != ""):
                                                            $arrayMain[] = array('ID' => $key->ID, 'title' => $key->post_title);
                                                        else:
                                                            $arrayPractice[] = array('ID' => $key->ID, 'title' => $key->post_title);
                                                        endif;
                                                    endforeach;
                                                    foreach ($arrayMain as $main):
                                                        ?>
                                                        <tr>
                                                            <td><i class='set-flag flag-red'></i></td>
                                                            <td>
                                                                <span class='mainPractice'>
                                                                    <?php echo $main['title']; ?>
                                                                </span>
                                                            </td>
                                                            <td align='center'>
                                                                <input type='checkbox' class='checkPractice' name='practice[]' value='<?php echo $main['ID']; ?>' checked/>
                                                            </td>
                                                            <td align='center'>
                                                                <input  class='checkFlag' type='checkbox' name='mainPractice[]' value='<?php echo $main['ID']; ?>' checked="" />
                                                            </td>
                                                        </tr>
                                                        <?php
                                                    endforeach;
                                                    foreach ($arrayPractice as $practice):
                                                        ?>
                                                        <tr>
                                                            <td><i class='set-flag flag' style="display:block;" ></i></td>
                                                            <td>
                                                                <span class='mainPractice'>
                                                                    <?php echo $practice['title']; ?>
                                                                </span>
                                                            </td>
                                                            <td align='center'>
                                                                <input type='checkbox' class='checkPractice' name='practice[]' value='<?php echo $practice['ID']; ?>' checked/>
                                                            </td>
                                                            <td align='center'>
                                                                <input  class='checkFlag' type='checkbox' name='mainPractice[]' value='<?php echo $practice['ID']; ?>' />
                                                            </td>
                                                        </tr>
                                                        <?php
                                                    endforeach;
                                                    foreach ($query->posts as $practice):
                                                        echo "<tr>
                                                                <td><i class='set-flag flag-white'></i></td>
                                                                <td>
                                                                    <span class='mainPractice'>
                                                                        " . $practice->post_title . "
                                                                    </span>
                                                                </td>
                                                                <td align='center'>
                                                                    <input type='checkbox' class='checkPractice' name='practice[]' value='" . $practice->ID . "' />
                                                                </td>
                                                                <td align='center'>
                                                                    <input class='checkFlag' type='checkbox' name='mainPractice[]' value='" . $practice->ID . "' />
                                                                </td>
                                                            </tr>";
                                                    endforeach;
                                                    ?>
                                                </table>
                                                <div class="clearfix addPractice">
                                                    <div class="pull-left">
                                                        <span class="practice-list"><i class="fa fa-plus"></i> <?php _e('Add practice...', 'Divi'); ?></span>
                                                        <p class="show-listPractice hide">
                                                            <input type="text" class="form-control practice-listName" placeholder="<?php _e('Practice Name...', 'Divi'); ?>" />
                                                            <input type="button" class="form-control btn-info insert-listPractice" value="<?php _e('Add item', 'Divi'); ?>" />
                                                        </p>
                                                    </div>
                                                    <p class="pull-right">
                                                        <input type="submit" class="btn btn-info btn-save save-profile"  value="<?php _e('Save', 'Divi'); ?>"  />
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="et_pb_tab clearfix <?php if ($_SESSION['activeTab'] == 5) echo "et_pb_active_content"; ?>" >
                                                <div class="select-radio">
                                                    <p class="profile-title">
                                                        <?php _e("Please enter the details about your Ngondro. If you are currently undertaking Ngondro, you can also share where you are in the process.", "Divi"); ?>
                                                    </p>
                                                    <b>
                                                        <input type="radio" name="follow"  value="1" <?php if ($meta['user_meta_checking'][0] == 1 || $meta['user_meta_checking'][0] == "") echo'checked="checked"'; ?> />
                                                        <label><?php _e('N/A', 'Divi'); ?></label> 
                                                    </b>
                                                    <b>
                                                        <input type="radio" name="follow"  value="2" <?php if ($meta['user_meta_checking'][0] == 2) echo'checked="checked"'; ?>/>
                                                        <label><?php _e('Not Yet', 'Divi'); ?></label>
                                                    </b>
                                                    <b>
                                                        <input type="radio" name="follow" class="show-sub-radio"  value="3" <?php if ($meta['user_meta_checking'][0] == 3) echo'checked="checked"'; ?> />
                                                        <label><?php _e('Ongoing', 'Divi'); ?></label>
                                                    </b>
                                                    <b>
                                                        <input type="radio" name="follow" class="add-year"  value="4" <?php if ($meta['user_meta_checking'][0] == 4) echo'checked="checked"'; ?> />
                                                        <label><?php _e('Completed', 'Divi'); ?></label>
                                                    </b>
                                                    <b>
                                                        <input type="radio" name="follow"  value="5" <?php if ($meta['user_meta_checking'][0] == 5) echo'checked="checked"'; ?> />
                                                        <label><?php _e('Canceled', 'Divi'); ?></label>
                                                    </b>
                                                </div>
                                                <div class="<?php if ($meta['user_meta_checking'][0] != 3) echo "hide"; ?> select-ongoing">
                                                    <ul>
                                                        <?php
                                                        $array = explode(',', $meta['user_meta_ongoing'][0]);
                                                        ?>
                                                        <li>
                                                            <span><input type="checkbox" name="changeOngoing[]" value="1" <?php if ($meta['user_meta_ongoing'][0] == 1) echo "checked='checked'"; ?>/>
                                                                <?php _e('Four thoughts', 'Divi'); ?></span>
                                                            <ul class="<?php if ($meta['user_meta_ongoing'][0] == 1) echo "show"; ?>">  
                                                                <li><input type="radio" class="not-yet" name="checkFourthought" value="1" <?php if ($meta['user_meta_radio_ongoing'][0] == 1) echo 'checked="checked"'; ?>  />
                                                                    <label><?php _e('Not Started', 'Divi'); ?></label>
                                                                </li>
                                                                <li><input type="radio" class="ongoing" name="checkFourthought" value="0" <?php if ($meta['user_meta_radio_ongoing'][0] == 0) echo 'checked="checked"'; ?>  />
                                                                    <label><?php _e('Ongoing', 'Divi'); ?></label>
                                                                </li>
                                                                <li><input type="radio" name="checkFourthought" class="completed" value="2" <?php if ($meta['user_meta_radio_ongoing'][0] == 2) echo 'checked="checked"'; ?>  />
                                                                    <label><?php _e('Completed', 'Divi'); ?></label>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        <li>
                                                            <span><input type="checkbox" name="changeOngoing[]" value="2"  <?php if (in_array(2, $array)) echo "checked='checked'"; ?>/><?php _e('Refuge & Prostrations', 'Divi'); ?> </span>
                                                            <ul class="<?php if (in_array(2, $array)) echo "show"; ?>">
                                                                <li><input type="radio" class="not-yet" name="checkProstration" value="1" <?php if ($meta['user_meta_refuge_prostration'][0] == 1) echo 'checked="checked"'; ?>  />
                                                                    <label><?php _e('Not Started', 'Divi'); ?></label>
                                                                </li>
                                                                <li><input type="radio" class="ongoing" name="checkProstration" value="0" <?php if ($meta['user_meta_refuge_prostration'][0] == 0) echo 'checked="checked"'; ?>/>
                                                                    <label><?php _e('Ongoing', 'Divi'); ?></label>
                                                                    <ul class='inputHide <?php if ($meta['user_meta_refuge_prostration'][0] == 0) echo 'show'; ?>'>
                                                                        <?php if ($meta['user_meta_prostration_ongoing'][0] == ''): ?>
                                                                            <li><input type="text" name="user_meta_prostration_ongoing" value="" class="validateNumber" placeholder="<?php _e('Current count', 'Divi'); ?>" /></li>
                                                                            <?php
                                                                        else:
                                                                            echo '<li><input type="text" name="user_meta_prostration_ongoing" value="' . $meta['user_meta_prostration_ongoing'][0] . '" class="validateNumber"  /></li>';
                                                                        endif;
                                                                        ?>
                                                                    </ul>
                                                                </li>
                                                                <li><input type="radio" class="completed" name="checkProstration" value="2" <?php if ($meta['user_meta_refuge_prostration'][0] == 2) echo 'checked="checked"'; ?>  />
                                                                    <label><?php _e('Completed', 'Divi'); ?></label>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        <li>
                                                            <span><input type="checkbox" name="changeOngoing[]" value="3" <?php if (in_array(3, $array)) echo "checked='checked'"; ?>/><?php _e('Vajrasattva', 'Divi'); ?></span>
                                                            <ul class="<?php if (in_array(3, $array)) echo "show"; ?>">
                                                                <li><input type="radio" class="not-yet" name="checkVajrasattva" value="1" <?php if ($meta['user_meta_vajrasattva'][0] == 1) echo 'checked="checked"'; ?>  />
                                                                    <label><?php _e('Not Started', 'Divi'); ?></label>
                                                                </li>
                                                                <li><input type="radio" class="ongoing" name="checkVajrasattva" value="0" <?php if ($meta['user_meta_vajrasattva'][0] == 0) echo 'checked="checked"'; ?>  />
                                                                    <label><?php _e('Ongoing', 'Divi'); ?></label>
                                                                    <ul class='inputHide <?php if ($meta['user_meta_vajrasattva'][0] == 0) echo 'show'; ?>'>
                                                                        <?php if ($meta['user_meta_vajrasattva_ongoing'][0] == ''): ?>
                                                                            <li><input type="text" name="user_meta_vajrasattva_ongoing" value="" class="validateNumber" placeholder="<?php _e('Current count', 'Divi'); ?>" /></li>
                                                                            <?php
                                                                        else:
                                                                            echo '<li><input type="text" name="user_meta_vajrasattva_ongoing" value="' . $meta['user_meta_vajrasattva_ongoing'][0] . '" class="validateNumber"  /></li>';
                                                                        endif;
                                                                        ?>
                                                                    </ul>
                                                                </li>
                                                                <li><input type="radio" class="completed" name="checkVajrasattva" value="2" <?php if ($meta['user_meta_vajrasattva'][0] == 2) echo 'checked="checked"'; ?>  />
                                                                    <label><?php _e('Completed', 'Divi'); ?></label>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        <li>
                                                            <span><input type="checkbox" name="changeOngoing[]" value="4" <?php if (in_array(4, $array)) echo "checked='checked'"; ?>/><?php _e('Mandala Offering', 'Divi'); ?></span>
                                                            <ul <?php if (in_array(4, $array)) echo "style='display:block;'"; ?>>
                                                                <li><input type="radio" class="not-yet" name="checkMandala" value="1" <?php if ($meta['user_meta_mandala_offering'][0] == 1) echo 'checked="checked"'; ?>  />
                                                                    <label><?php _e('Not Started', 'Divi'); ?></label>
                                                                </li>
                                                                <li><input type="radio" class="ongoing" name="checkMandala" value="0" <?php if ($meta['user_meta_mandala_offering'][0] == 0) echo 'checked="checked"'; ?>  />
                                                                    <label><?php _e('Ongoing', 'Divi'); ?></label>
                                                                    <ul class='inputHide <?php if ($meta['user_meta_mandala_offering'][0] == 0) echo 'show'; ?>'>
                                                                        <?php if ($meta['user_meta_mandala_ongoing'][0] == ''): ?>
                                                                            <li><input type="text" name="user_meta_mandala_ongoing" value="" class="validateNumber" placeholder="<?php _e('Current count', 'Divi'); ?>" /></li>
                                                                            <?php
                                                                        else:
                                                                            echo '<li><input type="text" name="user_meta_mandala_ongoing" value="' . $meta['user_meta_mandala_ongoing'][0] . '" class="validateNumber" /></li>';
                                                                        endif;
                                                                        ?>
                                                                    </ul>
                                                                </li>
                                                                <li><input type="radio" class="completed" name="checkMandala" value="2" <?php if ($meta['user_meta_mandala_offering'][0] == 2) echo 'checked="checked"'; ?>  />
                                                                    <label><?php _e('Completed', 'Divi'); ?></label>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        <li>
                                                            <span><input type="checkbox" name="changeOngoing[]"  value="5" <?php if (in_array(5, $array)) echo "checked='checked'"; ?> /><?php _e('Guru Yoga', 'Divi'); ?></span>
                                                            <ul <?php if (in_array(5, $array)) echo "style='display:block;'"; ?>>
                                                                <li><input type="radio" class="not-yet" name="checkGuru" value="1" <?php if ($meta['user_meta_guru_yoga'][0] == 1) echo 'checked="checked"'; ?>  />
                                                                    <label><?php _e('Not Started', 'Divi'); ?></label>
                                                                </li>
                                                                <li><input type="radio" class="ongoing" name="checkGuru" value="0" <?php if ($meta['user_meta_guru_yoga'][0] == 0) echo 'checked="checked"'; ?>  />
                                                                    <label><?php _e('Ongoing', 'Divi'); ?></label>
                                                                    <ul class='inputHide <?php if ($meta['user_meta_guru_yoga'][0] == 0) echo 'show'; ?>'>
                                                                        <?php if ($meta['user_meta_guru_ongoing'][0] == ""): ?>
                                                                            <li><input type="text" name="user_meta_guru_ongoing" value="" class="validateNumber" placeholder="<?php _e('Current count', 'Divi'); ?>" /></li>
                                                                            <?php
                                                                        else:
                                                                            echo '<li><input type="text" name="user_meta_guru_ongoing" value="' . $meta['user_meta_guru_ongoing'][0] . '" class="validateNumber" /></li>';
                                                                        endif;
                                                                        ?>
                                                                    </ul>
                                                                </li>
                                                                <li><input type="radio" class="completed" name="checkGuru" value="2" <?php if ($meta['user_meta_guru_yoga'][0] == 2) echo 'checked="checked"'; ?>  />
                                                                    <label><?php _e('Completed', 'Divi'); ?></label>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div>
                                                    <select name="year_complete"  class="year-complete <?php if ($meta['user_meta_checking'][0] != 4) echo "hide"; ?>">
                                                        <?php
                                                        $rand = range(2015, 1900);
                                                        if ($meta['user_meta_ngondo_complete'][0] != ""):
                                                            echo "<option value='" . $meta['user_meta_ngondo_complete'][0] . "'>" . $meta['user_meta_ngondo_complete'][0] . "</option>";
                                                        else:
                                                            echo "<option value=''>";
                                                            _e('Select Year', "Divi");
                                                            echo "</option>";
                                                        endif;
                                                        foreach ($rand as $num):
                                                            echo "<option value='" . $num . "'>" . $num . "</option>";
                                                        endforeach;
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="pull-right">
                                                    <input type="submit" class="btn btn-info btn-save save-profile save-ongoing"  value="<?php _e('Save', 'Divi'); ?>"  />
                                                </div>
                                            </div> <!-- .et_pb_tab -->
                                        </div>
                                    </form>
                                </div> <!-- .et_pb_tabs -->
                            </div> <!-- .et_pb_column -->
                        </div> <!-- .et_pb_row_inner -->
                    </div>      
                </div>
            </div> <!-- #content-area -->
        </div> <!-- .container -->
    </div> <!-- #main-content -->
    <?php get_footer(); ?>
<?php endif; ?>