<?php
/**
 * My Account page
 * Template Name: My Account
 * @author 		BaiNguyen
 * @package 	Divi/Templates
 */
if (!is_user_logged_in()):
    get_header();
    $is_page_builder_used = et_pb_is_pagebuilder_used(get_the_ID());
    
    ?>

    <div id="main-content" >

        <?php if (!$is_page_builder_used) : ?>

            <div class="container">
                <div id="content-area" class="clearfix">
                    <div id="left-area">

                    <?php endif; ?>

                    <?php while (have_posts()) : the_post(); ?>

                        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

                            <?php if (!$is_page_builder_used) : ?>

                                <h1 class="main_title"><?php the_title(); ?></h1>
                                <?php
                                $thumb = '';

                                $width = (int) apply_filters('et_pb_index_blog_image_width', 1080);

                                $height = (int) apply_filters('et_pb_index_blog_image_height', 675);
                                $classtext = 'et_featured_image';
                                $titletext = get_the_title();
                                $thumbnail = get_thumbnail($width, $height, $classtext, $titletext, $titletext, false, 'Blogimage');
                                $thumb = $thumbnail["thumb"];

                                if ('on' === et_get_option('divi_page_thumbnails', 'false') && '' !== $thumb)
                                    print_thumbnail($thumb, $thumbnail["use_timthumb"], $titletext, $width, $height);
                                ?>

                            <?php endif; ?>

                            <div class="entry-content">
                                <?php
                                the_content();
                                echo '<div class="et_pb_row">';
                                echo do_shortcode('[woocommerce_my_account]');
                                echo '</div>';
                                if (!$is_page_builder_used)
                                    wp_link_pages(array('before' => '<div class="page-links">' . __('Pages:', 'Divi'), 'after' => '</div>'));
                                ?>
                            </div> <!-- .entry-content -->

                            <?php
                            if (!$is_page_builder_used && comments_open() && 'on' === et_get_option('divi_show_pagescomments', 'false'))
                                comments_template('', true);
                            ?>

                        </article> <!-- .et_pb_post -->

                    <?php endwhile; ?>

                    <?php if (!$is_page_builder_used) : ?>

                    </div> <!-- #left-area -->

                    <?php get_sidebar(); ?>
                </div> <!-- #content-area -->
            </div> <!-- .container -->

        <?php endif; ?>

    </div> <!-- #main-content -->

    <?php get_footer(); ?>
    <?php
else:
    $user = wp_get_current_user();
    $meta = get_user_meta($user->ID);
	global $current_user;
    get_header();
    $is_page_builder_used = et_pb_is_pagebuilder_used(get_the_ID());
    if (isset($_GET['t']) && $_GET['t']) {
        echo '<script>jQuery(document).ready(function() {';
        echo ' jQuery("#tab_' . $_GET['tag'] . '_info"). trigger("click");';
        echo '});</script>';
    }
    ?>

    <div id="main-content" >

        <div class="container">
            <div id="content-area" class="clearfix">

                <div class="et_pb_column_4_4 myaccount_tab" >
                    <ul class="tabcontent " data-uid="<?php echo $user->ID ?>">
                        <li><a href="javascript:;" id="tab_my_account_info" data-forclass="tabcontentapply" data-idactive="my_account_info" class="active"  title="<?php _e('my Account Info', 'woocommerce'); ?>"><?php _e('my Account Info', 'woocommerce'); ?></a></li>
                        <li><a href="javascript:;" id="tab_my_offering_info" data-forclass="tabcontentapply" data-idactive="my_offering_info" class="" title="<?php _e('my Offering Info ', 'woocommerce'); ?>"><?php _e('my Offering Info ', 'woocommerce'); ?></a></li>

                    </ul>
                    <div id="my_account_info" class="my_account_info tabcontentapply active" style="display:block">
                        <p><?php
                            if ($current_user->membership_level->id) :
                                printf(__('Wonderful and beloved Benefactor. You can find the details in the lower part of %s. Click ', 'divi'),'<a href="javascript:;" onclick="jQuery(\'#tab_my_offering_info\').trigger(\'click\');" title="'.__('your Offerings Info','divi').'">'.__('your Offerings Info','divi').'</a>');
                                ?>
                                <a href="<?php echo pmpro_url("cancel", "?level=" . $current_user->membership_level->id) ?>"><?php _e("here to cancel your recurring offering", "pmpro"); ?></a>
                            <?php endif; ?>
                        </p>
                        <br/>
                        <form action="<?php echo admin_url('admin-ajax.php'); ?>" method="POST" class="validate" enctype="multipart/form-data">
                            <?php wp_nonce_field('process_profile', 'security-code-here'); ?>
                            <input name="action" value="process_profile" type="hidden">

                            <p>
                                <label><?php _e('First Name', 'Divi'); ?></label>
                                <input type="text" class="input-text" name="first-name" placeholder="<?php _e('First name...', 'Divi'); ?>" value="<?php echo $meta['first_name'][0]; ?>" />
                            </p>
                            <p>
                                <label><?php _e('Last Name', 'Divi'); ?></label>
                                <input type="text" class="input-text" name="last-name" placeholder="<?php _e('Last name...', 'Divi'); ?>" value="<?php echo $meta['last_name'][0]; ?>" />
                            </p>

                            <p>
                                <label><?php _e('Display Name', 'Divi'); ?><span class="note"><?php _e(' (this name will show in questions you post in the Q&A forums)', 'Divi'); ?></span></label>
                                <input type="text" class="input-text"  name="fullname" placeholder="<?php _e('Display Name ...', 'Divi'); ?>" value="<?php echo $user->display_name; ?>"/>
                            </p>
                            <p>
                                <label><?php _e('Email', 'Divi'); ?></label>
                                <input type="text" class="input-text"  disabled="disable" placeholder="<?php _e('Email ...', 'Divi'); ?>" value="<?php echo $user->user_email; ?>"/>
                            </p>
                            <p>
                                <label><?php _e('Date of birth', 'Divi'); ?></label>
                                <input type="text" name="dob" class="input-text datepicker" placeholder="dd/mm/yy" value="<?php echo $meta['dob'][0]; ?>"/>
                            </p>
                            <p>
                                <label><?php _e('Gender', 'Divi'); ?></label>
                                <input type="radio" name="gender" value="1" <?php echo ($meta['gender'][0] == 1) ? 'checked="checked"' : '' ?>/> <?php _e('Male', 'Divi') ?>
                                <input type="radio" name="gender" value="2" <?php echo ($meta['gender'][0] == 2) ? 'checked="checked"' : '' ?>/> <?php _e('Female', 'Divi') ?>

                            </p>

                                                                                <!--                                                    <p>
                                                                                     <label><?php //_e('Preferred Language', 'Divi');                      ?></label>
                                                                                     <select class="language_selectbox" name="language" >
                                                                                         <option value="en_US" <?php //echo (isset($meta['language']) && $meta['language'][0] == 'en_US') ? 'selected="selected"' : '';                      ?>><?php //_e('English');                      ?></option>
                                                                                         <option value="vi_VN" <?php //echo (isset($meta['language']) && $meta['language'][0] == 'vi_VN') ? 'selected="selected"' : '';                      ?>><?php //_e('Vietnamese');                      ?></option>
                                                                                     </select>
                                                                                 </p>-->
                            <p>
                                <label><?php _e('New Password', 'Divi'); ?></label>
                                <input type="password" id="password" name="password" class="input-text" placeholder="<?php _e('New password', 'Divi'); ?>"/>
                            </p>
                            <p>
                                <label><?php _e('Confirm Password', 'Divi'); ?></label>
                                <input type="password" id="confirmPassword" class="input-text" placeholder="<?php _e('Confirm password', 'Divi'); ?>"/>
                            </p>
                            <p>
                                <label><?php _e('Are you subscribed to the newsletter?', 'Divi'); ?> <input class="notification_checkbox" name="subscribe" type="checkbox" value="1" <?php echo (isset($meta['user_meta_mailchimp']) && $meta['user_meta_mailchimp'][0] == 1) ? 'checked="checked"' : ''; ?> />
                                    <br />
                                    <span class="note"><?php _e('(This reflects your subscription status, please click on the box if you want to change your status, then "Update" below).', 'Divi'); ?></span></label>
                            </p>
                            <p>
                                <label><?php _e('Address', 'Divi'); ?></label>
                                <input type="text" name="address" placeholder="<?php _e("Enter address", "Divi"); ?>" class="input-text" value="<?php echo $meta['address'][0]; ?>" />
                            </p>
                            <p style="text-align: right">
                                <input class="btn-info save-profile btn-save" id="update_profile" value="<?php _e('Update', 'Divi'); ?>" type="submit">
                            </p>


                        </form>
                    </div>
                    <div id="my_offering_info" class="my_offering_info tabcontentapply" style="display:none">
                        <?php
                        $customer_orders = get_list_orders_by_user();
                        $invoices = getInvoicesRecurring($current_user);
                        if ($customer_orders || !empty($invoices)) :
                            ?>

                            <h2><?php echo apply_filters('woocommerce_my_account_my_orders_title', __('Your Offering History', 'woocommerce')); ?></h2>
                            <p><?php _e('In the table below please find the offerings we have in our records from you through this platform. If there is an error or any issue, please kindly contact us <a href="' . get_home_url() . '/contact-us/" >here</a> and include the offering # in your message if that is available. If you have already enrolled in all the modules you like and would like to make an additional offering you can do so at the bottom of the page <a href="' . get_home_url() . '/dana/" >here</a>.', 'Divi') ?></p>
                            <p></p>
                            <table class="shop_table my_account_orders">

                                <thead>
                                    <tr>
                                        <th class="order-number"><span class="nobr"><?php _e('Offering', 'woocommerce'); ?></span></th>
                                        <th class="order-date"><span class="nobr"><?php _e('Date', 'woocommerce'); ?></span></th>
                                        <th class="order-status"><span class="nobr"><?php _e('Status', 'woocommerce'); ?></span></th>
                                        <th class="order-total"><span class="nobr"><?php _e('Total', 'woocommerce'); ?></span></th>
                                        <th class="order-actions" style="width: 350px;max-width: 350px;text-align: left"><?php _e('Item', 'woocommerce'); ?></th>
                                    </tr>
                                </thead>

                                <tbody><?php
                                    if (!empty($customer_orders)):
                                        foreach ($customer_orders as $customer_order) :

                                            $order = wc_get_order($customer_order['ID']);

                                            $item_count = $order->get_item_count();
                                            ?><tr class="order">
                                                <td class="order-number">
                <!--                                                <a href="<?php //echo $order->get_view_order_url();             ?>">
                                                    <?php //echo $order->get_order_number();    ?>
                                                    </a>-->
                                                    <?php echo $order->get_order_number(); ?>
                                                </td>
                                                <td class="order-date">
                                                    <time datetime="<?php echo date('Y-m-d', strtotime($order->order_date)); ?>" title="<?php echo esc_attr(strtotime($order->order_date)); ?>"><?php echo date_i18n(get_option('date_format'), strtotime($order->order_date)); ?></time>
                                                </td>
                                                <td class="order-status" style="text-align:left; white-space:nowrap;">
                                                    <?php echo wc_get_order_status_name($order->get_status()); ?>
                                                </td>
                                                <td class="order-total">
                                                    <?php echo sprintf(_n('%s for %s item', '%s for %s items', $item_count, 'woocommerce'), $order->get_formatted_order_total(), $item_count); ?>
                                                </td>
                                                <td class="order-actions"  style="text-align: left">
                                                    <?php
                                                    $products = $order->get_items();
                                                    if (!empty($products)):
                                                        foreach ($products as $product) :
                                                            echo '<p>' . $product['name'] . '</p>'; //'<a href="'.  get_permalink($product['product_id']).'">'.$product['name'].'</p>';
                                                        endforeach;
                                                    endif;
                                                    ?>
                                                </td>
                                            </tr><?php
                                        endforeach;
                                    endif;
                                    if (!empty($invoices)):
                                        ?>
                                        <tr>
                                            <td colspan="5">
                                                <?php _e('<h3>Your recurring offering: </h3>', 'divi') ?>
                                            </td>
                                        </tr>
                                        <?php foreach ($invoices as $invoice): ?>
                                            <tr class="order">
                                                <td class="order-number">
                                                    <?php echo $invoice->code ?>
                                                </td>
                                                <td class="order-date">
                                                    <time datetime="<?php echo date('Y-m-d', $invoice->timestamp); ?>" title="<?php echo esc_attr($invoice->timestamp); ?>"><?php echo date_i18n(get_option('date_format'), $invoice->timestamp); ?></time>
                                                </td>
                                                <td class="order-status" style="text-align:left; white-space:nowrap;">
                                                    <?php echo ($invoice->status == 'success') ? 'active' : $invoice->status; ?>
                                                </td>
                                                <td class="order-total">
                                                    <?php echo pmpro_formatPrice($invoice->total); ?>
                                                </td>
                                                <td class="order-actions"  style="text-align: left">
                                                    <?php echo $invoice->membership_level_name ?>
                                                </td>
                                            </tr>
                                            <?php
                                        endforeach;
                                    endif;
                                    ?></tbody>

                            </table>
                        <?php else: ?>
                            <p><?php _e('Our records do not show any offerings from you through this platform yet. If this looks like an error, please kindly contact us <a href="' . get_home_url() . '/contact-us/" >here</a>. If you would like to make an offering but have already enrolled in all the modules you like, you can offer any amount <a href="' . get_home_url() . '/product/dana/" >here</a>.', 'Divi') ?></p>
                            <p></p>
                        <?php endif; ?>
                    </div>
                </div>  
            </div> <!-- #content-area -->
        </div> <!-- .container -->
        <?php get_footer(); ?>
    <?php endif; ?>