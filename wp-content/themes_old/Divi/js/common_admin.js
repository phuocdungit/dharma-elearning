var App ={
    getAvatar:function(){
        jQuery(".change-avatar").click(function(){
            jQuery(".choose-image").click();
            jQuery("#your-profile").attr("enctype","multipart/form-data");
        });
        jQuery(".choose-image").change(function(){ 
            if(jQuery(this).val()!=''){
                read_file(this);
            }
        });
        function read_file(input,thumbimage) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    jQuery(".avatar").removeAttr("src").attr('src',e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
                jQuery(".avatar").show();
            }
            else {
                jQuery(".avatar").removeAttr("src").attr('src',input.value);
                jQuery(".avatar").show();
            }
        }
    },
    checkAllinitiation:function(){
        jQuery('.checkAll').click(function(){
            var checkboxes = jQuery(this).closest('.listInitiation').find(':checkbox');
            if(jQuery(this).is(':checked')) {
                checkboxes.attr('checked', 'checked');
            } else {
                checkboxes.removeAttr('checked');
            }
        });
    },
    editInititionreceived:function(){
        jQuery(".action").change(function(){
            var $select = jQuery(this).val(),
                $id = jQuery(this).attr("data-id"),
                $user = jQuery(this).attr("data-user"),
                $_this = jQuery(this),
                $url = jQuery(".dataTable").attr("data-link");
            if($select=='edit'){
                $_this.fadeOut("slow");
                jQuery(this).parent().find(".uncheck").fadeIn();
                jQuery(this).parents("tr").find(".selectShow").fadeIn();
                jQuery(this).parents("tr").find(".editHide").fadeOut();
            }
            if($select=='del'){
                var $result = confirm("You want to delete ?");
                if($result==true){
                    $_this.fadeOut("slow");
                    jQuery.ajax({
                        url:$url,
                        type:"POST",
                        data:{action:'delete_initiation',id:$id},
                        beforeSend:function(){
                          $_this.parent().html("<span class='loading hideLoad'></span>");    
                        },
                        success:function(data){
                            if(data==1){
                                ///alert("successfully removed");                                
                                jQuery(".hideLoad").parents("tr").slideUp("slow");
                            }
//                            else{
//                                alert("an error occurred please try again");
//                            }
                        }
                    });
                }else{
                    $_this.val(0);
                }
            }
            if($select=='duplicate'){
                $_this.fadeOut("slow");
                jQuery.ajax({
                    url:$url,
                    type:"POST",
                    data:{action:'duplicate_initiation',id:$id,user:$user},
                    beforeSend:function(){
                        $_this.parent().html("<span class='loading hideLoad'></span>");    
                    },success:function(data){
                        if(data!=""){
                            jQuery(this).parent().find(".selectEdit").fadeOut(); 
                            jQuery("#show-received tbody").html(data).fadeIn('slow');
                            $_this.parent().find(".action").val(0);
                            App.editInititionreceived();
                        }
                    }
                });
            }
        });
        jQuery(".uncheck").click(function(){
           jQuery(this).parents("tr").find(".selectShow").fadeOut();
           jQuery(this).parents("tr").find(".editHide").fadeIn();
           jQuery(this).parent().find(".action").val(0).fadeIn();
           jQuery(this).fadeOut();
           jQuery(this).parent().find(".selectEdit").fadeOut(); 
           jQuery(this).parent().find(".edit").fadeIn(); 
        });
        jQuery(".selectShow").change(function(){
           jQuery(this).parents("tr").find(".check").fadeIn("slow");
        });
        jQuery(".check").click(function(){
           var $id = jQuery(this).attr("data-id"),
               $user = jQuery(this).attr("data-user"),
               $practice = jQuery(this).parents("tr").find(".practiceEdit"),
               $teacher = jQuery(this).parents("tr").find(".teacherEdit").val(),
               $year = jQuery(this).parents("tr").find(".yearEdit").val(),
               $url = jQuery(".dataTable").attr("data-link"),
               $_this = jQuery(this),
               $pval = new Array();
                $practice.each(function(){
                   $pval.push(jQuery(this).val());
                });
                jQuery.ajax({
                    type:"POST",
                    url:$url,
                    data:{action:'update_initition_received_backend',id:$id,practice:$pval.join(','),year:$year,teacher:$teacher,user:$user},
                    beforeSend:function(){
                      $_this.parent().append("<span class='loading'></span>");    
                      $_this.parent().find(".selectEdit").fadeOut();
                    },
                    success:function(data){
                        if(data!="" && data!=1){
                            jQuery(this).parent().find(".selectEdit").fadeOut(); 
                            jQuery('.dataTables_empty').fadeOut("slow");
                            jQuery("#show-received tbody").html(data);
                            $_this.parent().find(".action").val(0);
                            App.editInititionreceived();
                        }else if(data==1){  
                            alert("Initiations received already exists. Please choose from the list");
                            $_this.parent().find(".action").fadeIn();
                            $_this.parent().find(".loading").fadeOut();
                            $_this.parents("tr").find(".editHide").fadeIn();
                            $_this.parents("tr").find(".selectShow").fadeOut();
                            //jQuery('.loading').parents("tr").remove();
                        }
//                      else{
//                        alert("an error occurred please try again");
//                      }
                    }
                });
        });
    },
    Practice:function(){
        jQuery(".checkPractice").click(function() {
            var $_this = jQuery(this);
            if (jQuery(this).is(":checked")) {
            } else {
                $_this.parents('tr').find(".checkFlag").removeAttr("checked");
            }
        });
        jQuery(".checkFlag").click(function() {
            var $_this = jQuery(this);
            if ($_this.is(":checked")) {
                $_this.parents('tr').find(".checkPractice").attr("checked", "checked");
            }
        });
    }
}
jQuery(document).ready(function(){
    App.getAvatar();
    App.checkAllinitiation();
    App.editInititionreceived();
    App.Practice();
});