<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * Override this template by copying it to yourtheme/woocommerce/content-single-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */
if (!defined('ABSPATH'))
    exit; // Exit if accessed directly
?>

<?php
/**
 * woocommerce_before_single_product hook
 *
 * @hooked wc_print_notices - 10
 */
do_action('woocommerce_before_single_product');
if($_GET['cart']){
    wp_redirect('/cart');
}
if (post_password_required()) {
    echo get_the_password_form();
    return;
}
?>
<?php
global $post;
?>
<?php get_header(); ?>


<?php
if ($post):
    ?>
    <?php if (et_get_option('divi_integration_single_top') <> '' && et_get_option('divi_integrate_singletop_enable') == 'on') echo(et_get_option('divi_integration_single_top')); ?>
    <article id="post-<?php the_ID() ?>" <?php post_class('et_pb_post'); ?>>
        <!--<h1><?php //echo $_related_course->post_title;      ?></h1>-->
        <div class="entry-content">
            <?php
                the_content();
            echo"<div class='clearfix'></div></div>";
            

            wp_link_pages(array('before' => '<div class="page-links">' . __('Pages:', 'Divi'), 'after' => '</div>'));
            ?>
        </div> <!-- .entry-content -->
        <?php
        if (et_get_option('divi_468_enable') == 'on') {
            echo '<div class="et-single-post-ad">';
            if (et_get_option('divi_468_adsense') <> '')
                echo( et_get_option('divi_468_adsense') );
            else {
                ?>
                <a href="<?php echo esc_url(et_get_option('divi_468_url')); ?>"><img src="<?php echo esc_attr(et_get_option('divi_468_image')); ?>" alt="468 ad" class="foursixeight" /></a>
                    <?php
                }
                echo '</div> <!-- .et-single-post-ad -->';
            }
            ?>

        <?php
        if (( comments_open() || get_comments_number() ) && 'on' == et_get_option('divi_show_postcomments', 'on'))
            comments_template('', true);
        ?>
    </article> <!-- .et_pb_post -->
    <?php if (et_get_option('divi_integration_single_bottom') <> '' && et_get_option('divi_integrate_singlebottom_enable') == 'on') echo(et_get_option('divi_integration_single_bottom')); ?>
<?php endif; ?>
<?php do_action('woocommerce_after_single_product'); ?>
