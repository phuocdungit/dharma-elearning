<?php

/**
 * Loop Add to Cart
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.1.0
 */
if (!defined('ABSPATH'))
    exit; // Exit if accessed directly

global $product;
$courseId = get_post_meta($product->id, '_related_course', true);
 $meta = get_post_meta($courseId);
$issecret = false;
if ($courseId == 'dana') {
    $textButton = __('Make an offering', 'Divi');
} else {    
    $textButton = __('Enroll', 'Divi');
    $issecret = get_post_meta($courseId, 'wpcf-is-secret', true);
}
if (is_user_logged_in()):
    $current_user = wp_get_current_user();
    $email = $current_user->email;
    $current_user->ID;

// Hoang Bui
    $meta_course = get_post_meta($courseId, '_sfwd-courses', true);
    $pending_list = $meta_course['sfwd-courses_course_pending_list'];
    
    $pending_list = explode(',', $pending_list);
    $allow_buy = true;
    if (in_array($current_user->ID, $pending_list)) :
        $allow_buy = false;
    endif;
    
//check user bought
    if (!$allow_buy || woocommerce_customer_bought_product($email, $current_user->ID, $product->id, $issecret)) :
        echo apply_filters('woocommerce_loop_add_to_cart_link', '<a href="javascript:;" onclick="alert(\'You have already applied for enrolment for this course and it is pending approval, so you cannot enrol again. If you feel this is taking too long, please kindly contact us and we will do our best to assist you.\')" rel="nofollow" class="et_pb_promo_button info add_to_cart_button">' . $textButton . '</a>', $product);
    else:
        echo apply_filters('woocommerce_loop_add_to_cart_link', sprintf('<a href="%s" rel="nofollow" data-product_id="%s" data-product_sku="%s" data-quantity="%s" class="et_pb_promo_button info %s product_type_%s">%s</a>', esc_url($product->add_to_cart_url()), esc_attr($product->id), esc_attr($product->get_sku()), esc_attr(isset($quantity) ? $quantity : 1 ), $product->is_purchasable() && $product->is_in_stock() ? 'add_to_cart_button' : '', esc_attr($product->product_type), esc_html($textButton)
                ), $product);
    endif;
else:
    if($meta['wpcf-sharing-course'][0]==1){
        echo '<a class="register-link et_pb_promo_button info add_to_cart_button product_type_simple" id="register-link" data-href="' . wp_registration_url() . '" href="javascript:;" title="' . __('Register') . '">' . __('Register') . '</a>';
    }else{
        echo apply_filters('woocommerce_loop_add_to_cart_link', sprintf('<a href="%s" rel="nofollow" data-product_id="%s" data-product_sku="%s" data-quantity="%s" class="et_pb_promo_button info %s product_type_%s">%s</a>', esc_url($product->add_to_cart_url()), esc_attr($product->id), esc_attr($product->get_sku()), esc_attr(isset($quantity) ? $quantity : 1 ), $product->is_purchasable() && $product->is_in_stock() ? 'add_to_cart_button' : '', esc_attr($product->product_type), esc_html($textButton)
            ), $product);
    }
    
endif; 

