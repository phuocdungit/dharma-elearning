<?php
/**
 * Customer completed order email
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates/Emails
 * @version     1.6.4
 */
if (!defined('ABSPATH'))
    exit; // Exit if accessed directly 
?>

<?php 
$free = floatval($order->get_total())==0;
if($free){
    $email_heading = "Thank you very much for enrolling!";
}
do_action('woocommerce_email_header', $email_heading); 
?>
<?php 
if($free){
    wc_get_template('emails/email_completed_order_template_free.php');
}else{
   wc_get_template('emails/email_completed_order_template.php'); 
}
 ?>
<p><?php printf(__("Your offering details are shown below for your reference:", 'woocommerce'), get_option('blogname')); ?></p>

<?php do_action('woocommerce_email_before_order_table', $order, $sent_to_admin, $plain_text); ?>

<h2><?php echo __('Offering:', 'woocommerce') . ' ' . $order->get_order_number(); ?></h2>

<table cellspacing="0" cellpadding="6" style="width: 100%; border: 1px solid #eee;" border="1" bordercolor="#eee">
    <thead>
        <tr>
            <th scope="col" style="text-align:left; border: 1px solid #eee;"><?php _e('Item', 'woocommerce'); ?></th>
            <!--<th scope="col" style="text-align:left; border: 1px solid #eee;"><?php //_e('Quantity', 'woocommerce'); ?></th>-->
            <th scope="col" style="text-align:left; border: 1px solid #eee;"><?php _e('Offering', 'woocommerce'); ?></th>
        </tr>
    </thead>
    <tbody>
        <?php echo $order->email_order_items_table(true, false, true); ?>
    </tbody>
    <tfoot>
        <?php
        if ($totals = $order->get_order_item_totals()) {
            $i = 0;
            foreach ($totals as $total) {
                $i++;
                ?><tr>
                    <th scope="row" style="text-align:left; border: 1px solid #eee; <?php if ($i == 1) echo 'border-top-width: 4px;'; ?>"><?php echo $total['label']; ?></th>
                    <td style="text-align:left; border: 1px solid #eee; <?php if ($i == 1) echo 'border-top-width: 4px;'; ?>"><?php echo $total['value']; ?></td>
                </tr><?php
            }
        }
        ?>
    </tfoot>
</table>

<?php do_action('woocommerce_email_after_order_table', $order, $sent_to_admin, $plain_text); ?>

<?php do_action('woocommerce_email_order_meta', $order, $sent_to_admin, $plain_text); ?>

<h2><?php _e('Your details', 'woocommerce'); ?></h2>


<?php if ($order->user_email): ?>
    <p><strong><?php _e('Email:', 'woocommerce'); ?></strong> <?php echo $order->user_email; ?></p>
<?php else: ?>
    <?php if ($order->billing_email) : ?>
        <p><strong><?php _e('Email:', 'woocommerce'); ?></strong> <?php echo $order->billing_email; ?></p>
    <?php endif; ?>
<?php endif; ?>
<?php if ($order->billing_phone) : ?>
    <p><strong><?php _e('Tel:', 'woocommerce'); ?></strong> <?php echo $order->billing_phone; ?></p>
<?php endif; ?>

<?php wc_get_template('emails/email-addresses.php', array('order' => $order)); ?>

<?php do_action('woocommerce_email_footer'); ?>