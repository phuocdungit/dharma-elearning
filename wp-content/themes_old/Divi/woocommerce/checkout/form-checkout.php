<?php
/**
 * Checkout Form
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */
if (!defined('ABSPATH'))
    exit; // Exit if accessed directly

wc_print_notices();

do_action('woocommerce_before_checkout_form', $checkout);

// If checkout registration is disabled and not logged in, the user cannot checkout
if (!$checkout->enable_signup && !$checkout->enable_guest_checkout && !is_user_logged_in()) {
    echo apply_filters('woocommerce_checkout_must_be_logged_in_message', __('You must be logged in to checkout.', 'woocommerce'));
    return;
}

// filter hook for include new pages inside the payment method
$get_checkout_url = apply_filters('woocommerce_get_checkout_url', WC()->cart->get_checkout_url());
$str = "Please confirm your offerings! <br /><br />"
        . "Please carefully check the courses you have selected and your offerings. Use the browser “back” button if you want to make any changes.<br /><br />";
$str.="From this page you will be redirected to the Paypal payment gateway where you can either pay with a paypal account or with your credit card. Even if you use a credit card, Paypal will ask for an email address but to the best of our knowledge they will not require you to create and account. After entering your details over there you will return to this site for the final payment confirmation. So, see you in a few moments.<br /><br />";
?>

<p class="thankmsg"><?php echo apply_filters('woocommerce_thankyou_order_received_text', __($str, 'woocommerce'), null); ?></p>

<form name="checkout" method="post" class="checkout" action="<?php echo esc_url($get_checkout_url); ?>" enctype="multipart/form-data">

    <?php if (sizeof($checkout->checkout_fields) > 0) : ?>

        <?php do_action('woocommerce_checkout_before_customer_details'); ?>

        <div class="col2-set" id="customer_details">

            <div class="col-1">

                <?php //do_action( 'woocommerce_checkout_billing' );  ?>

            </div>

            <div class="col-2">

                <?php //do_action( 'woocommerce_checkout_shipping' );  ?>

            </div>

        </div>

        <?php do_action('woocommerce_checkout_after_customer_details'); ?>

        <h3 id="order_review_heading"><?php _e('Your offerings', 'woocommerce'); ?></h3>

    <?php endif; ?>

    <?php do_action('woocommerce_checkout_order_review'); ?>

</form>

<?php do_action('woocommerce_after_checkout_form', $checkout); ?>