<?php
/*
  Template Name: Launch
 */
?>
<html onkeypress="return disableCtrlKeyCombination(event);" onkeydown="return disableCtrlKeyCombination(event);">
    <?php
    if (!is_user_logged_in()) {
        get_header();
        ?>
        <div id="main-content">
            <div id="content-area" class="clearfix post-course">
                <div class="entry-content" style="text-align:center">
                    <?php
                    $datas = get_query_var('lauching');
                    $data = explode(',', $datas);
                    if (is_course($data[0])) {
                        $sharingcourse = get_post_meta($data[0], 'wpcf-sharing-course', true);
                        if ($sharingcourse) {
                            ?>
                            <p>&nbsp;</p>
                            <p>Dear Dharma-friend :-)</p>
                            <p>The module you are trying to launch is open for all members. If you already are a member, please login below to launch the module.</p>
                            <p>If you are a new friend, please kindly register (using the link underneath the login button below) to access this module. Registration is free.</p>
                            <p>May your practice flourish and be beneficial. May all be auspicious."</p>
                            <?php
                        }
                    } else {
                        wp_redirect(site_url());
                        exit;
                    }
                    ?>
                    <div class="mycontent-loginform">
                        <?php
//                    
                        $args = array(
                            'echo' => true,
                            'redirect' => get_permalink() . '?lauching=' . get_query_var('lauching'),
                            'form_id' => 'loginform',
                            'label_username' => __('Email'),
                            'label_password' => __('Password'),
                            'label_remember' => __('Remember Me'),
                            'label_log_in' => __('Log In'),
                            'id_username' => 'user_login',
                            'id_password' => 'user_pass',
                            'id_remember' => 'rememberme',
                            'id_submit' => 'wp-submit',
                            'remember' => true,
                            'value_username' => NULL,
                            'value_remember' => false
                        );
                        wp_login_form($args);
                        ?>
                        <?php wp_register('', ''); ?> | <a href="<?php echo wp_lostpassword_url(); ?>" title="Lost your password?"><?php _e('Lost your password ?', 'Divi'); ?></a>
                    </div>
                </div>
                <?php get_footer(); ?>
            </div>
        </div>
        <?php
    } else {
        //do_shortcode($meta['wpcf-course-document'][0]);
        $user = wp_get_current_user();
        $datas = get_query_var('lauching');
        $data = explode(',', $datas);
        $enrolled = get_enrolled_courses($user, true);
        if (is_course($data[0])) {
//        if ($user->ID != $data[1]) {
//            $_SESSION['direct_launch'] = 1;
//            wp_redirect(site_url());
//            exit;
//        }
            if (in_array($data[0], $enrolled)) {
                $meta = get_post_meta($data[0], 'wpcf-course-document', true);
                ?>
                <head>
                    <meta charset="<?php bloginfo('charset'); ?>" />
                    <title><?php elegant_titles(); ?></title>
                    <?php elegant_description(); ?>
                    <?php elegant_keywords(); ?>
                    <script>
                        document.onkeypress = function(event) {
                            event = (event || window.event);
                            if (event.keyCode == 123) {
                                //alert('No F-12');
                                return false;
                            }
                        }
                        document.onmousedown = function(event) {
                            event = (event || window.event);
                            if (event.keyCode == 123) {
                                //alert('No F-keys');
                                return false;
                            }
                        }
                        document.onkeydown = function(event) {
                            event = (event || window.event);
                            if (event.keyCode == 123) {
                                //alert('No F-keys');
                                return false;
                            }
                        }
                        function disableCtrlKeyCombination(e)
                        {
                            //list all CTRL + key combinations you want to disable
                            var forbiddenKeys = new Array('a', 'u', 'n', 'c', 'x', 'v', 'j', 'w');
                            var key;
                            var isCtrl;
                            if (window.event)
                            {
                                key = window.event.keyCode;     //IE
                                if (window.event.ctrlKey)
                                    isCtrl = true;
                                else
                                    isCtrl = false;
                            }
                            else
                            {
                                key = e.which;     //firefox
                                if (e.ctrlKey)
                                    isCtrl = true;
                                else
                                    isCtrl = false;
                            }
                            //if ctrl is pressed check if other key is in forbidenKeys array
                            if (isCtrl)
                            {
                                for (i = 0; i < forbiddenKeys.length; i++)
                                {
                                    //case-insensitive comparation
                                    if (forbiddenKeys[i].toLowerCase() == String.fromCharCode(key).toLowerCase())
                                    {
                                        //alert('Key combination CTRL + ' + String.fromCharCode(key) + ' has been disabled.');
                                        return false;
                                    }
                                }
                            }
                            return true;
                        }
                    </script>
                </head>
                <body onkeypress="return disableCtrlKeyCombination(event);" onkeydown="return disableCtrlKeyCombination(event);">
                    <iframe src='<?php echo do_shortcode($meta) ?>' width='100%' height='100%' frameborder="0"><?php _e('<p>The content is not avaiable. <a href="/" title="Go back Homepage">Go back Homepage</a>', 'divi') ?></iframe> 
                </body>
                <?php
            } else {
                $_SESSION['direct_launch'] = 1;
                wp_redirect(site_url());
                exit;
                get_header();
                ?>
                <div id="main-content" style="padding-top:150px">
                    <div id="content-area" class="clearfix post-course">
                        <div class="entry-content" style="text-align:center">
                            <p><?php
                                _e('Dear friend, the content you are trying to access is part of Dharma-eLearning.net.', 'Divi');
                                echo '</p><p>';
                                _e('Please kindly enroll to access the content at ', 'Divi');
                                echo '<a href="/content" title="Content">here</a>';
                                ?></p>
                        </div>
                    </div>
                </div>
                <?php
            }
        } else {
            get_header();
            ?>
            <div id="main-content" style="padding-top:150px">
                <div id="content-area" class="clearfix post-course">
                    <div class="entry-content" style="text-align:center">
                        <p> <?php
                            _e('The content you are trying to access is not exist in Dharma-eLearning.net.', 'Divi');
                            echo '</p><p><a href="/content" title="' . get_bloginfo('name') . '">' . get_bloginfo('name') . '</a>';
                            ?></p>
                    </div>
                </div>
            </div>
            <?php
        }
    }
    ?>
</html>