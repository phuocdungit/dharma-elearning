<?php
/*
  Template Name: Affiliation Template
 */
$user= wp_get_current_user();
if(!$user->ID):
    wp_redirect(home_url()."/login");
else:    
get_header();
$is_page_builder_used = et_pb_is_pagebuilder_used( get_the_ID() );
?>
<div id="main-content">
<?php 
    global $wpdb;
    $teacher= $wpdb->prefix."teachers";
    $meta = get_user_meta($user->ID);
?>
<?php if ( ! $is_page_builder_used ) : ?>
	<div class="container">
            <div id="content-area" class="clearfix">
<?php endif; ?> 
                    <div class="et_pb_column et_pb_column_4_4 et_pb_column_inner link" link="<?php echo admin_url("admin-ajax.php"); ?>">
                        <div class="et_pb_toggle et_pb_toggle_open">
                            <h5 class="et_pb_toggle_title"><?php _e('Geography'); ?></h5>
                            <div class="et_pb_toggle_content clearfix">
                                <div class='list-affiliation'>
<!--                                    <span class='add-country'><i class='fa fa-plus'></i> Add item <b>Country</b></span>
                                        <p class="hide show-country">
                                            <input type="text" class="form-control country-name" placeholder="Country..." />
                                            <input type="submit"  class="button btn-info insert-country insert-item" value="Add item" />
                                        </p>-->
                                    <?php
                                        function show_affiliation($parent=0){
                                            global $wpdb;
                                            $table= $wpdb->prefix."countries";
                                            $query=$wpdb->get_results("SELECT * FROM ".$table." WHERE country_parent={$parent} ORDER BY country_name ASC");
                                            echo"<ul>";
                                                foreach ($query as $key):
                                                    echo"<li><input type='checkbox' name='ick[]' value='".$key->ID."' /><span >".$key->country_name."</span><br />";
                                                        show_affiliation($key->ID);    
                                                        echo'<div class="show-add">
                                                                <span class="add-item"><i class="fa fa-plus"></i> Add item in <b>'.$key->country_name.'</b></span>
                                                                <p class="hide">
                                                                    <input type="text" class="form-control country-name" placeholder="Country..." />                                                                        
                                                                    <input type="submit" id-add="'.$key->ID.'"  class="button btn-info insert-item" value="Add item" />
                                                                </p>
                                                            </div>';
                                                    echo"</li>";
                                                endforeach;
                                            echo"</ul>";
                                        }
                                        show_affiliation();
                                    ?>
                                </div>
                                <div class="clearfix"></div>
                                <div class="select-country">
                                    <label><?php _e('Select Geography'); ?></label>
                                    <select class="form-control show-select" multiple>

                                    </select>
                                    <div class="pull-right">
                                        <input type="submit" class="btn-info btn-save btn-country" value="Save" />
                                    </div>
                                </div>
                            </div>
                        </div> <!-- .et_pb_toggle_content -->
                        <div class="et_pb_toggle et_pb_toggle_close">
                            <h5 class="et_pb_toggle_title"><?php _e('Lineage & School'); ?></h5>
                            <div class="et_pb_toggle_content clearfix" style="display: none;">
                                <div class='list-school'>
                                    <?php
                                        function show_school($parent=0){
                                            global $wpdb;
                                            $school= $wpdb->prefix."school";
                                            $query=$wpdb->get_results("SELECT * FROM ".$school." WHERE school_parent={$parent} ORDER BY school_name ASC");
                                            echo"<ul>";
                                                foreach ($query as $key):
                                                    echo"<li><input type='checkbox' name='val[]' value='".$key->ID."' /><span>".$key->school_name."</span><br />";
                                                        show_school($key->ID);
                                                        echo'<div class="show-add">
                                                            <span class="add-item-school"><i class="fa fa-plus"></i> Add item in <b>'.$key->school_name.'</b></span>
                                                            <p class="hide">
                                                                <input type="text" class="form-control school-name" placeholder="School..." />
                                                                <input type="submit"  id-add="'.$key->ID.'" class="button btn-info insert-school" value="Add item" />
                                                            </p>
                                                        </div>';
                                                    echo"</li>";
                                                endforeach;
                                            echo"</ul>";
                                        }
                                        show_school();
                                    ?>
                                </div>
                                <div class="clearfix"></div>
                                <span class='add-school'><i class='fa fa-plus'></i> <?php _e(' Add item <b>Lineage & School</b>'); ?></span>
                                <p class="hide box-school">
                                    <input type="text" class="form-control school-name" placeholder="School..." />
                                    <input type="submit"  class="button btn-info insert-lineage" value="Add item" />
                                </p>
                                <div class="select-school">
                                    <label><?php _e('Select Lineage & School'); ?></label>
                                    <select class="form-control show-school" multiple>
                                         <?php 
//                                            $tableSchool= $wpdb->prefix . "school";
//                                            $tableUser=$wpdb->prefix."users";
//                                            $key = unserialize($meta['select_school'][0]);
//                                            foreach ( $key as $val):
//                                                $result = $wpdb->get_row("SELECT * FROM " . $tableSchool . " AS s,".$tableUser." AS u WHERE s.ID=".$val." AND u.ID =".$user->ID);
//                                                echo "<option value='" . $result->ID . "' selected>" . $result->school_name . "</option>";
//                                            endforeach;
                                         ?>
                                    </select>
                                    <div class="pull-right">
                                        <input type="submit" class="btn-info btn-save btn-school" value="Save" />
                                    </div>
                                </div>
                            </div>
                        </div><!-- .et_pb_toggle_content -->
                        <div class="et_pb_toggle et_pb_toggle_close">
                            <h5 class="et_pb_toggle_title"><?php _e('Teachers'); ?></h5>
                            <div class="et_pb_toggle_content clearfix" style="display: none;">
                                <select class="show-teacher multiteacher" multiple>
                                    <?php
                                    $result = $wpdb->get_results("SELECT * FROM ".$teacher." ORDER BY teacher_name ASC"); 
                                    foreach ($result as $key):
                                      echo"<option value='".$key->ID."'>".$key->teacher_name."</option>";  
                                    endforeach;
                                    ?>
                                </select>
                                <div class="box-teacher">
                                    <span class="add-teacher"><i class="fa fa-plus"></i> <?php _e('Add teacher'); ?></span>
                                    <p class="hide">
                                        <input type="text" class="form-control teacher-name" placeholder="Teacher..." />
                                        <input type="submit"  class="button btn-info insert-teacher" value="Add item" />
                                    </p>
                                </div>
                                <div class="pull-right">
                                    <input type="submit" value="Save" class="btn-info btn-save save-teacher">
                                </div>
                            </div>
                        </div><!-- .et_pb_toggle_content -->
                    </div>
            </div> <!-- #content-area -->
	</div> <!-- .container -->
</div> <!-- #main-content -->
<?php get_footer(); ?>
<?php endif;?>

