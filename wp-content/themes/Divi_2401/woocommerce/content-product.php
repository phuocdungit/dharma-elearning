<?php
/**
 * The template for displaying product content within loops.
 *
 * Override this template by copying it to yourtheme/woocommerce/content-product.php
 *
 * @author        WooThemes
 * @package    WooCommerce/Templates
 * @version     1.6.4
 */
if (!defined('ABSPATH'))
    exit; // Exit if accessed directly

global $product, $woocommerce_loop;
// Store loop count we're currently on
if (empty($woocommerce_loop['loop']))
    $woocommerce_loop['loop'] = 0;

// Store column count for displaying the grid
if (empty($woocommerce_loop['columns']))
    $woocommerce_loop['columns'] = apply_filters('loop_shop_columns', 1);

//bainguyen set collumn
$woocommerce_loop['columns'] = 1;
// Ensure visibility
if (!$product || !$product->is_visible())
    return;

// Increase loop count
//$woocommerce_loop['loop'] ++;
$woocommerce_loop['loop']++;

// Extra post classes
$classes = array('et_pb_column_4_4');
if (0 == ($woocommerce_loop['loop'] - 1) % $woocommerce_loop['columns'] || 1 == $woocommerce_loop['columns'])
//    $classes[] = 'first';
    if (0 == $woocommerce_loop['loop'] % $woocommerce_loop['columns'])
//    $classes[] = 'last';
        $current_user = wp_get_current_user();
$_related_courseid = get_post_meta($product->id, '_related_course', true);
if ($_related_courseid != 'dana') {// if related_course is dana => can buy again
//if (true) {// if related_course is dana => can buy again
$meta = get_post_meta($_related_courseid);
if (!sfwd_lms_has_access($_related_courseid, $current_user->ID)) {
//    if (true) {
?>
<div class="entry-content et_pb_column_4_4 row_<?php echo $woocommerce_loop['loop'] % 2; ?>">
    <div class="" style="padding: 0px 0px 20px 0px">
        <div <?php post_class($classes); ?> >
            <?php do_action('woocommerce_before_shop_loop_item'); ?>

            <?php
            $producMeta = get_post_meta(get_the_ID());
            //                    echo "<pre>";
            //                    print_r($producMeta);
            //                    echo "</pre>";
            //                    exit;
            $_related_courseid = $producMeta['_related_course'][0];
            if ($_related_courseid) {
                $_related_course = get_post($_related_courseid);
                echo '<div class="title et_pb_column_1_4 fleft"><h3><a href="' . get_permalink($_related_course->ID) . '" title="' . $_related_course->post_title . '">' . $_related_course->post_title . '</a></h3></div>';
                echo '<div class="img et_pb_column_1_4 fleft">';
                if (has_post_thumbnail()) {
                    echo get_the_post_thumbnail(get_the_ID(), 'full', array('class' => 'img-responsive center-block')) . '</div>';
                } else {
                    echo '<img class="img-responsive center-block" src="' . get_template_directory_uri() . '/images/no-image.jpg" /></div>';
                }
                echo '<div class="box-button et_pb_column_1_2 fleft nomr"><div class="launchRemoveWraper" style="padding-top:10px;display: none">';
//                echo '<a class="et_pb_promo_button btn-green" href="' . get_permalink() . '" title="' . __('Detail', 'woocommerce') . '">' . __('Detail', 'woocommerce') . '</a>';
                echo do_shortcode('[add_to_cart id="' . $product->id . '"]');
                echo '</div>';
                echo '</div>';
                echo '</div>';
            }
            ?>
            <?php //the_permalink(); ?>

            <?php do_action('woocommerce_after_shop_loop_item'); ?>
        </div>
        <?php }
        } ?>
    </div>
    <!--</div>-->