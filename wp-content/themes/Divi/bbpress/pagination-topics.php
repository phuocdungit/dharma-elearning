<?php
/**
 * Pagination for pages of topics (when viewing a forum)
 *
 * @package bbPress
 * @subpackage Theme
 */
?>

<?php do_action('bbp_template_before_pagination_loop'); ?>

<div class="bbp-pagination">
    <div class="bbp-pagination-count">

        <?php bbp_forum_pagination_count(); ?>        
        <?php
        if (is_user_logged_in() && (!bbp_is_user_home())):
            $slug = sanitize_title(get_the_title(), $fallback_title);            
            if ($slug != "my-forums") {
                ?>
                <a class="right" href="/my-forums" title="<?php printf(esc_attr__("%s's Favorites", 'bbpress'), bbp_get_displayed_user_field('display_name')); ?>"><?php _e('Favorites', 'bbpress'); ?></a>
    <?php } endif; ?>
    </div>

    <div class="bbp-pagination-links">

<?php bbp_forum_pagination_links(); ?>

    </div>
</div>

<?php do_action('bbp_template_after_pagination_loop'); ?>
