<?php 
/*
    Template Name: Error Page
*/
    get_header(); 
    $post = get_page_by_path('noresults');
?>
<div id="main-content" >
    <?php if (!$is_page_builder_used) : ?>
        <div class="container">
            <div id="content-area" class="clearfix">
                <div>
                <?php endif; ?>
                <?php if($post->ID): ?>
                    <article id="post-<?php echo $post->ID; ?>" >
                        <?php if (!$is_page_builder_used) : ?>
                            <h1 class="main_title"><?php $post->post_title; ?></h1>
                           
                        <?php endif; ?>
                        <div class="entry-content">
                           <?php
                            echo do_shortcode($post->post_content);
                            if (!$is_page_builder_used)
                                wp_link_pages(array('before' => '<div class="page-links">' . __('Pages:', 'Divi'), 'after' => '</div>'));
                            ?>

                        </div> <!-- .entry-content -->
                    </article> <!-- .et_pb_post -->
                <?php else: ?>
                    <?php get_template_part( 'includes/no-results', '404' ); ?>
                <?php endif; ?>
                <?php if (!$is_page_builder_used) : ?>
                </div> <!-- #left-area -->
                <?php //get_sidebar(); ?>
            </div> <!-- #content-area -->
        </div> <!-- .container -->
    <?php endif; ?>
</div> 

<?php get_footer(); ?>