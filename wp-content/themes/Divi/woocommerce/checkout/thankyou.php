<?php
/**
 * Thankyou page
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.2.0
 */
if (!defined('ABSPATH'))
    exit; // Exit if accessed directly
$_SESSION['cartcustom'] = array();
if ($order) :
    ?>

    <?php if ($order->has_status('failed')) : ?>

        <p><?php _e('Unfortunately your order cannot be processed as the originating bank/merchant has declined your transaction.', 'woocommerce'); ?></p>

        <p><?php
            if (is_user_logged_in())
                _e('Please attempt your purchase again or go to your account page.', 'woocommerce');
            else
                _e('Please attempt your purchase again.', 'woocommerce');
            ?></p>

        <p>
            <a href="<?php echo esc_url($order->get_checkout_payment_url()); ?>" class="button pay"><?php _e('Pay', 'woocommerce') ?></a>
            <?php if (is_user_logged_in()) : ?>
                <a href="<?php echo esc_url(get_permalink(wc_get_page_id('myaccount'))); ?>" class="button pay"><?php _e('My Account', 'woocommerce'); ?></a>
            <?php endif; ?>
        </p>

        <?php
    else :

        $str = "<p>Thank you very much! Your offering has been received. Any unrestricted modules you have enrolled in will be immediately available in ";
        $str.="<a href='" . home_url() . "/my-content'>my Content.</a></p>";
        $str.="<p>Any restricted items will need to be approved by us and will only be available after the approval has gone through. We will need to determine whether you meet the criteria necessary for accessing these modules. This can take a little while. Please kindly be patient with us. </p>";

        $str.="<p>We may also need to contact you by email to ensure that we can all guard our samaya well. If you feel this takes too long or if you are unsure about your qualifications for any restricted module, please kindly ";
        $str.="<a href='" . home_url() . "/contact-us'>contact us</a>";
        $str.=" and we will assist you as best we can. We will of course refund your offerings for any restricted courses that we cannot approve if you wish.</p>";

        $str.="<p>Since we are using Jinpa’s company The Vimalakirti Project Ltd for the payment process, the confirmation email from Paypal will show that name instead of Dharma-eLearning. However, you should also find our offering-related email address, ";
        $str.="<a href='mailto:dana@dharma-elearning.net'>dana@dharma-elearning.net</a>";
        $str.=" in the confirmation. Your bank or credit card statement will show PAYPAL*DHARMA-ELG for this transaction. Once again, if you have any questions please don’t hesitate to ";
        $str.="<a href='" . home_url() . "/contact-us'>contact us</a>";
        $str.=" or reply to the confirmation email from the Dana email address.</p>";
        $str.="<p>Once more thank you very much for your offerings and your practice!</p>";

        if (floatval($order->get_total()) == 0) {
            $str = "<p>Thank you very much! Your enrolment has been received. Any unrestricted modules you have enrolled in will be immediately available in ";
            $str.="<a href='" . home_url() . "/my-content'>my Content.</a></p>";
            $str.="<p>Any restricted items will need to be approved by us and will only be available after the approval has gone through. We will need to determine whether you meet the criteria necessary for accessing these modules. This can take a little while. Please kindly be patient with us. </p>";

            $str.="<p>We may also need to contact you by email to ensure that we can all guard our samaya well. If you feel this takes too long or if you are unsure about your qualifications for any restricted module, please kindly ";
            $str.="<a href='" . home_url() . "/contact-us'>contact us</a>";
            $str.=" and we will assist you as best we can. We will of course refund your offerings for any restricted courses that we cannot approve if you wish.</p>";
            $str.="<p>Once more thank you very much for your enrolment and your practice!</p>";
        }
        ?>
        <p class="thankmsg"><?php echo apply_filters('woocommerce_thankyou_order_received_text', __($str, 'woocommerce'), $order); ?></p>

        <ul class="order_details">
            <li class="order">
                <?php _e('Offering:', 'woocommerce'); ?>
                <strong><?php echo $order->get_order_number(); ?></strong>
            </li>
            <li class="date">
                <?php _e('Date:', 'woocommerce'); ?>
                <strong><?php echo date_i18n(get_option('date_format'), strtotime($order->order_date)); ?></strong>
            </li>
            <li class="total">
                <?php _e('Payment:', 'woocommerce'); ?>
                <strong><?php echo $order->get_formatted_order_total(); ?></strong>
            </li>
            <?php if ($order->payment_method_title) : ?>
                <li class="method">
                    <?php _e('Payment method:', 'woocommerce'); ?>
                    <strong><?php echo $order->payment_method_title; ?></strong>
                </li>
            <?php endif; ?>
        </ul>
        <div class="clear"></div>

    <?php endif; ?>

    <?php do_action('woocommerce_thankyou_' . $order->payment_method, $order->id); ?>
    <?php do_action('woocommerce_thankyou', $order->id); ?>

<?php else : ?>

    <p><?php echo apply_filters('woocommerce_thankyou_order_received_text', __($str, 'woocommerce'), null); ?></p>

<?php endif; ?>