<?php if (!defined('ABSPATH'))
    exit; // Exit if accessed directly 
$user = wp_get_current_user();
?>
<p>Dear <?php echo $user->first_name?> </p>
<p>Thank you very much! We have received your enrolment. 
<p>Any unrestricted modules you have enrolled in will be immediately available</p>
in <strong><a href="<?php echo home_url()?>/my-content/" title="my Dharma - my Content">my Dharma - my Content</a>.</strong></p>
<p>Any restricted items will need to be approved by us and will only be 
available after the approval has gone through. We will need to determine 
whether you meet the criteria necessary for accessing these modules. This 
can take a little while. Please kindly be patient with us.</p>
<p>We may also need to contact you by email to ensure that we can all guard 
our samaya well. If you feel this takes too long or if you are unsure about 
your qualifications for any restricted module, please kindly contact us via 
the website or by replying to this message and we will assist you as best 
we can. We will of course refund your offerings for any restricted courses 
that we cannot approve if you wish.</p>
<p>If you would like to make an offering later without enrolling into another 
module, please kindly go to <a href="http://dharma-elearning.net/dana/" title="Dana">http://dharma-elearning.net/dana/</a></p>
<p>Once again, if you have any questions please don’t hesitate to contact us 
through the website or simply reply to this email.</p>
<p>Once more thank you very much for your enrolment and your practice!</p>