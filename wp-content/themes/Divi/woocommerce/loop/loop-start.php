<?php
/**
 * Product Loop Start
 *
 * @author        WooThemes
 * @package    WooCommerce/Templates
 * @version     2.0.0
 */
?>
<div class="et_pb_row">
    <div class="et_pb_column et_pb_column_4_4">
        <div class="et_pb_text et_pb_bg_layout_light et_pb_text_align_left">
            <div id="main-content">
                <div id="content">
                    <div class="productslist products col2">
