<?php
/**
 * Cart Page
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.1.0
 */
if (!defined('ABSPATH'))
    exit; // Exit if accessed directly

wc_print_notices();

do_action('woocommerce_before_cart');
?>
<div class="woocommerce-info hide alert-cart">
    <span><?php _e('The total offering can either be 0 or any amount greater than 1$.', 'Divi'); ?></span>
</div>
<form action="<?php echo esc_url(WC()->cart->get_cart_url()); ?>" method="post" >

    <?php do_action('woocommerce_before_cart_table'); ?>

    <table class="shop_table cart" cellspacing="0">
        <thead>
            <tr>
                <th class="product-remove">&nbsp;</th>
                <th class="product-thumbnail">&nbsp;</th>
                <th class="product-name"><?php _e('Item', 'woocommerce'); ?></th>
                <th class="product-price"><?php _e('Select your offering', 'woocommerce'); ?></th>
                <!--<th class="product-quantity"><?php //_e('Quantity', 'woocommerce');   ?></th>-->
                <th class="product-subtotal"><?php _e('Offering', 'woocommerce'); ?></th>
            </tr>
        </thead>
        <tbody>
            <?php do_action('woocommerce_before_cart_contents'); ?>

            <?php
            foreach (WC()->cart->get_cart() as $cart_item_key => $cart_item) {

                $_product = apply_filters('woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key);
                $product_id = apply_filters('woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key);

                if ($_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters('woocommerce_cart_item_visible', true, $cart_item, $cart_item_key)) {
                    ?>
                    <tr class="<?php echo esc_attr(apply_filters('woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key)); ?>">

                        <td class="product-remove">
                            <?php
                            echo apply_filters('woocommerce_cart_item_remove_link', sprintf('<a href="%s" class="remove" title="%s">&times;</a>', esc_url(WC()->cart->get_remove_url($cart_item_key)), __('Remove this item', 'woocommerce')), $cart_item_key);
                            ?>
                        </td>

                        <td class="product-thumbnail">
                            <?php
                            $thumbnail = apply_filters('woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key);

//                            if (!$_product->is_visible())
//                                echo $thumbnail;
//                            else
//                                printf('<a href="%s">%s</a>', $_product->get_permalink(), $thumbnail);
                            ?>
                        </td>

                        <td class="product-name">
                            <?php
                            if (!$_product->is_visible())
                                echo apply_filters('woocommerce_cart_item_name', $_product->get_title(), $cart_item, $cart_item_key);
                            else
                                echo apply_filters('woocommerce_cart_item_name', $_product->get_title(), $cart_item, $cart_item_key);

                            // Meta data
                            echo WC()->cart->get_item_data($cart_item);

                            // Backorder notification
                            if ($_product->backorders_require_notification() && $_product->is_on_backorder($cart_item['quantity']))
                                echo '<p class="backorder_notification">' . __('Available on backorder', 'woocommerce') . '</p>';
                            ?>
                        </td>

                        <td class="product-price">
                            <!--<input type="text" class="inputprice number float" value="<?php //echo $cart_item['data']->price;    ?>">-->
                            <?php
                            $pricetmp = $cart_item['data']->price;
                            if (has_role_sponsor()) {
                                $pricetmp = 0;
                            }
                            if (isset($_SESSION['cartcustom'][$cart_item->product_id]['price'])) {
                                $idprod = $cart_item['product_id'];
                                $pricetmp = $_SESSION['cartcustom'][$idprod]['price'];
                            }
                            $prices = get_post_meta($cart_item['product_id'], "wpcf-price-item");
                            $mimimum_price = get_post_meta($cart_item['product_id'], "wpcf-mimimum-price");
                            $product_price = $_SESSION['cartcustom'][$cart_item['product_id']]['price'];
                            if ($mimimum_price[0] > 0) {
                             echo '<input type="text" data-id="' . $cart_item_key . '" placeholder="0.00"  value="' . number_format($mimimum_price[0], 2) . '" class="price-custom hide"  />';   
                            } else {
                            echo '<input type="text" data-id="' . $cart_item_key . '" placeholder="0.00"  value="' . number_format($prices[0], 2) . '" class="price-custom hide"  />';
                            }                            
                            echo '<input type="text" value="' . $mimimum_price[0] . '" id="' . $cart_item_key . '" class="mimimum-price hide"  />';
                            echo '<select name="cart[' . $cart_item_key . '][line_subtotal]" class="change-price ' . $cart_item_key . '">';
                            //echo '<option value="'.$subtotal[0].'" selected>'.$subtotal[0].'</option>';
                            if ($product_price):
                                echo '<option value="' . $product_price . '" selected>' . number_format($product_price, 2) . '</option>';
                            else:
                                echo '<option data-price="notselect" value="notselect">Select...</option>';
                            endif;
                            $selectedPriceZero = '';
                            if (isset($_SESSION['line_subtotal']) && ($_SESSION['line_subtotal'] === '0')) {
                                $selectedPriceZero = 'selected="selected"';
                            }
                            // echo '<option value="0" '.$selectedPriceZero.'>0</option>';
                            if ($mimimum_price[0] > 0) {
                                echo '<option value="' . $mimimum_price[0] . '" ' . $selectedPriceZero . '>' . number_format($mimimum_price[0], 2) . '</option>';
                            }
//                            else{
//                                echo '<option value="0" ' . $selectedPriceZero . '>0</option>';
//                            }
                            foreach ($prices as $k => $price):
                                if ($product_price == $price):

                                else:
                                    echo '<option value="' . $price . '">' . number_format($price, 2) . '</option>';
                                endif;
                            endforeach;
                            if ($product_price):
                                echo '<option class="order-price" data-toggle="1" value="' . $product_price . '">';
                                _e('other...', 'woocommerce');
                                echo '</option>';
                            else:
                                echo '<option class="order-price" data-toggle="1" value="' . $pricetmp . '">';
                                _e('other...', 'woocommerce');
                                echo '</option>';
                            endif;
                            echo '</select>';
                            ?>

                            <?php apply_filters('woocommerce_cart_item_price', WC()->cart->get_product_price($_product), $cart_item, $cart_item_key); ?>
                            <?php
                            if ($_product->is_sold_individually()) {
                                $product_quantity = sprintf('<input type="hidden" name="cart[%s][qty]" value="1" />', $cart_item_key);
                            } else {
                                $product_quantity = woocommerce_quantity_input(array(
                                    'input_name' => "cart[{$cart_item_key}][qty]",
                                    'input_value' => $cart_item['quantity'],
                                    'max_value' => $_product->backorders_allowed() ? '' : $_product->get_stock_quantity(),
                                    'min_value' => '0'
                                        ), $_product, false);
                            }

                            echo apply_filters('woocommerce_cart_item_quantity', $product_quantity, $cart_item_key);
                            ?>
                        </td>

                        <td class="product-subtotal">
                            <?php
                            if (!isset($_SESSION['line_subtotal']) || $_SESSION['line_subtotal'] == 'notselect') {
                                _e('Please select your offering on left', 'woocommerce');
                            } else {
                                echo apply_filters('woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal($_product, $cart_item['quantity']), $cart_item, $cart_item_key);
                            }
                            session_start();
                            ?>
                        </td>
                    </tr>
                    <?php
                }
            }

            do_action('woocommerce_cart_contents');
            ?>
            <tr>
                <td colspan="6" class="actions">
                    <?php
                    global $woocommerce;
                    ?>
                    <?php if (WC()->cart->coupons_enabled()) { ?>
                        <div class="coupon">

                            <label for="coupon_code"><?php _e('Coupon', 'woocommerce'); ?>:</label> <input type="text" name="coupon_code" class="input-text" id="coupon_code" value="" placeholder="<?php _e('Coupon code', 'woocommerce'); ?>" /> <input type="submit" class="button" name="apply_coupon" value="<?php _e('Apply Coupon', 'woocommerce'); ?>" />

                            <?php do_action('woocommerce_cart_coupon'); ?>

                        </div>
                    <?php } ?>

                    <input type="submit" class="button" name="update_cart" value="<?php _e('Update Offerings', 'woocommerce'); ?>" /> 

                    <?php do_action('woocommerce_proceed_to_checkout'); ?>

                    <?php wp_nonce_field('woocommerce-cart'); ?>
                </td>
            </tr>

            <?php do_action('woocommerce_after_cart_contents'); ?>
        </tbody>
    </table>

    <?php do_action('woocommerce_after_cart_table'); ?>



    <div class="cart-collaterals">

        <?php do_action('woocommerce_cart_collaterals'); ?>

        <?php woocommerce_cart_totals(); ?>

        <?php woocommerce_shipping_calculator(); ?>

    </div>
    <input type="submit" onclick="return App.updateCart(this, '<?php echo home_url(); ?>/checkout/')" data-total-cart="<?php echo $woocommerce->cart->total; ?>" class="checkout-button button alt wc-forward" name="proceed" value="<?php _e('Proceed', 'woocommerce'); ?>" />
</form>
<?php do_action('woocommerce_after_cart'); ?>
<div id="dialog_box"></div>