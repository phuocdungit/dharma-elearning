<?php if (!isset($_SESSION)) session_start(); ?>
<!DOCTYPE html>
<!--[if IE 6]>
<html id="ie6" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 7]>
<html id="ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html id="ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!-->

<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
    <meta charset="<?php bloginfo('charset'); ?>"/>
    <title><?php elegant_titles(); ?></title>
    <?php elegant_description(); ?>
    <?php elegant_keywords(); ?>
    <?php elegant_canonical(); ?>

    <?php do_action('et_head_meta'); ?>

    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>"/>

    <?php $template_directory_uri = get_template_directory_uri(); ?>
    <!--[if lt IE 9]>
    <script src="<?php echo esc_url($template_directory_uri . '/js/html5.js"'); ?>" type="text/javascript"></script>
    <![endif]-->
    <script type="text/javascript">
        document.documentElement.className = 'js';
    </script>
    <script type="text/javascript" src="<?php echo $template_directory_uri; ?>/js/swfobject.js">
    </script>
    <script type="text/javascript" src="<?php echo $template_directory_uri; ?>/js/ParsedQueryString.js">
    </script>
    <link href="http://vjs.zencdn.net/c/video-js.css" rel="stylesheet">
    <script src="http://vjs.zencdn.net/c/video.js"></script>
    <link rel='stylesheet' id='font-css'  href='https://dharma-elearning.staging.visualweber.net/wp-includes/css/dashicons.min.css' type='text/css' media='all' />
    <?php wp_head(); ?>
	<script>
	function loadlink(){
        jQuery.ajax({
            type: 'POST',
            url: '/wp-admin/admin-ajax.php',
            data: {action: 'getNotifi',id:<?php echo get_the_ID() ?>},
            success: function(response) {
                 var data = jQuery.parseJSON(response);
                 if (data.err == 0 && data.count>0) {
                    jQuery('.my-notification').html(data.notifi);
                    jQuery('.number').html(data.count);
                     jQuery('span.number').css({'display': 'block'})

                 }
            }
        });
}
    loadlink();
setInterval(function(){
    loadlink() // this will run after every 5 seconds
}, 50000);
	</script>
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-58212357-1', 'auto');
        ga('send', 'pageview');

    </script>
</head>
<body <?php body_class(); ?>>
<div id="page-container">
    <?php
    if (is_page_template('page-template-blank.php')) {
        return;
    }
    readNotifi();
    $et_secondary_nav_items = et_divi_get_top_nav_items();

    $et_phone_number = $et_secondary_nav_items->phone_number;

    $et_email = $et_secondary_nav_items->email;

    $et_contact_info_defined = $et_secondary_nav_items->contact_info_defined;

    $show_header_social_icons = $et_secondary_nav_items->show_header_social_icons;

    $et_secondary_nav = $et_secondary_nav_items->secondary_nav;

    $primary_nav_class = 'et_nav_text_color_' . et_get_option('primary_nav_text_color', 'dark');

    $secondary_nav_class = 'et_nav_text_color_' . et_get_option('secondary_nav_text_color', 'light');

    $et_top_info_defined = $et_secondary_nav_items->top_info_defined;
    ?>

    <?php if ($et_top_info_defined) : ?>
        <div id="top-header" class="<?php echo esc_attr($secondary_nav_class); ?>">
            <div class="container clearfix">

                <?php if ($et_contact_info_defined) : ?>

                    <div id="et-info">
                        <?php if ('' !== ($et_phone_number = et_get_option('phone_number'))) : ?>
                            <span id="et-info-phone"><?php echo esc_html($et_phone_number); ?></span>
                        <?php endif; ?>

                        <?php if ('' !== ($et_email = et_get_option('header_email'))) : ?>
                            <a href="<?php echo esc_attr('mailto:' . $et_email); ?>"><span
                                    id="et-info-email"><?php echo esc_html($et_email); ?></span></a>
                        <?php endif; ?>

                        <?php
                        if (true === $show_header_social_icons) {
                            get_template_part('includes/social_icons', 'header');
                        }
                        ?>
                    </div> <!-- #et-info -->

                <?php endif; // true === $et_contact_info_defined  ?>

                <div id="et-secondary-menu">
                    <?php
                    //bainguyen start

                    $array_menu = wp_get_nav_menu_items('welcome');
                    $js = '';
                    foreach ($array_menu as $key => $value) {
                        if ($value->description != '') {
                            $js = 'onclick="App.setCookie(\'uid' . get_current_user_id() . '\',' . "'" . $value->description . "'" . ', 30);"';
                        }
                        $sub_menu[] = "<li ><a " . $js . " href='" . $value->url . "'>" . $value->title . "</a></li>";
                        $js = '';
                    }
                    $metaUser = get_user_meta(wp_get_current_user()->ID);
                    $welcome = (wp_get_current_user()->display_name) ? wp_get_current_user()->display_name : $metaUser['nickname'][0];
                    if (is_user_logged_in()) {
                        $count = '<a href="/notification"><span class="dashicons dashicons-admin-site"></span><span class="number"></span></span></a>';
                        $link = '<li  class="menu-item menu-type-link menu-notification">'
                            . $count
                            . '<ul class="my-notification link-my-account">'
                            . '</ul></li>';
                        $link .= '<li  class="menu-item menu-type-link">'
                            . '<a href="' . home_url() . '/my-profile">' . ('Welcome ') . $welcome . '</a>'
                            . '<ul class="link-my-account">'
                            . implode("", $sub_menu)
                            . '</ul></li>';
                        $link .= '<li id="log-out-link" class="menu-item menu-type-link">' . '<a href="' . wp_logout_url() . '" title="' . __('Logout') . '">' . __('Logout') . '</a>' . '</li>';
                        // check_benefactor_status(wp_get_current_user());
                    } else {

                        $link = '<li class="login-link" id="login-link" class="menu-item menu-type-link">' . '<a data-href="' . wp_login_url() . '" href="javascript:;" title="' . __('Login') . '">' . __('Login') . '</a>' . '</li>';
                        $link .= '<li class="register-link" id="register-link" class="menu-item menu-type-link">' . '<a data-href="' . wp_registration_url() . '" href="javascript:;" title="' . __('Register') . '">' . __('Register') . '</a>' . '</li>';
                    }

                    echo '<ul id="menuUser">' . $link . '</ul>';
                    if (isset($_REQUEST['ajaxaction'])) {
                        $type = 'register';
                        if ($_REQUEST['ajaxaction'] != 'register') {
                            $type = 'login';
                        }
                        echo '<script>jQuery(document).ready(function() {';
                        echo 'if (jQuery(".' . $type . '-link") . length) {
                                    jQuery(".' . $type . '-link"). trigger("click");
                                 }';
                        echo '});</script>';
                    }

                    //end
                    if (!$et_contact_info_defined && true === $show_header_social_icons) {
                        get_template_part('includes/social_icons', 'header');
                    } else if ($et_contact_info_defined && true === $show_header_social_icons) {
                        ob_start();

                        get_template_part('includes/social_icons', 'header');

                        $duplicate_social_icons = ob_get_contents();

                        ob_end_clean();

                        printf(
                            '<div class="et_duplicate_social_icons">
								%1$s
							</div>', $duplicate_social_icons
                        );
                    }

                    if ('' !== $et_secondary_nav) {
                        echo $et_secondary_nav;
                    }
                    echo '<div id="cartheader">';
                    et_show_cart_total();
                    $cartmenu = wp_nav_menu(array('theme_location' => 'cart-menu'));
                    echo '</div>';
                    ?>
                </div> <!-- #et-secondary-menu -->

            </div> <!-- .container -->
        </div> <!-- #top-header -->
    <?php endif; // true ==== $et_top_info_defined  ?>

    <header id="main-header" class="<?php echo esc_attr($primary_nav_class); ?>">
        <div class="container clearfix">
            <?php
            $logo = ($user_logo = et_get_option('divi_logo')) && '' != $user_logo ? $user_logo : $template_directory_uri . '/images/logo.png';
            ?>
            <a href="<?php echo esc_url(home_url('/')); ?>">
                <img src="<?php echo esc_attr($logo); ?>" alt="<?php echo esc_attr(get_bloginfo('name')); ?>"
                     id="logo"/>
            </a>

            <div id="et-top-navigation">
                <nav id="top-menu-nav">
                    <?php
                    $menuClass = 'nav';
                    if ('on' == et_get_option('divi_disable_toptier'))
                        $menuClass .= ' et_disable_top_tier';
                    $primaryNav = '';

                    $primaryNav = wp_nav_menu(array('theme_location' => 'primary-menu', 'container' => '', 'fallback_cb' => '', 'menu_class' => $menuClass, 'menu_id' => 'top-menu', 'echo' => false));

                    if ('' == $primaryNav) :
                        ?>
                        <ul id="top-menu" class="<?php echo esc_attr($menuClass); ?>">
                            <?php if ('on' == et_get_option('divi_home_link')) { ?>
                                <li <?php if (is_home()) echo('class="current_page_item"'); ?>><a
                                        href="<?php echo esc_url(home_url('/')); ?>"><?php esc_html_e('Home', 'Divi'); ?></a>
                                </li>
                            <?php }; ?>

                            <?php show_page_menu($menuClass, false, false); ?>
                            <?php show_categories_menu($menuClass, false); ?>
                        </ul>
                        <?php
                    else :
                        echo($primaryNav);
                    endif;
                    ?>
                </nav>

                <?php
                if (!$et_top_info_defined) {
                    et_show_cart_total(array(
                        'no_text' => true,
                    ));
                }
                ?>

                <?php if (false !== et_get_option('show_search_icon', true)) : ?>
                    <div id="et_top_search">
                        <span id="et_search_icon"></span>
                        <form role="search" method="get" class="et-search-form et-hidden"
                              action="<?php echo esc_url(home_url('/')); ?>">
                            <?php
                            printf('<input type="search" class="et-search-field" placeholder="%1$s" value="%2$s" name="s" title="%3$s" />', esc_attr_x('Search &hellip;', 'placeholder', 'Divi'), get_search_query(), esc_attr_x('Search for:', 'label', 'Divi')
                            );
                            ?>
                        </form>
                    </div>
                <?php endif; // true === et_get_option( 'show_search_icon', false )  ?>

                <?php do_action('et_header_top'); ?>
            </div> <!-- #et-top-navigation -->
        </div> <!-- .container -->
    </header> <!-- #main-header -->

    <div id="et-main-area">

