<?php
get_header();?>
	<div id='player'></div>
	<script type='text/javascript'>
		jwplayer('player').setup({
			sources: [{
                    file: "https://s3-ap-southeast-1.amazonaws.com/dharmaelearning/Happy-Birthday-Rinpoche-la_360p_0-23-11-2015-09%3A31%3A58.mp4",
                    label: "720p"
                }, {
                    file: "https://s3-ap-southeast-1.amazonaws.com/dharmaelearning/Happy-Birthday-Rinpoche-la_360p_0-23-11-2015-09%3A31%3A58.mp4",
                    label: "360p",
                    "default": "true"
                }, {
                file: "https://s3-ap-southeast-1.amazonaws.com/dharmaelearning/Happy-Birthday-Rinpoche-la_360p_0-23-11-2015-09%3A31%3A58.mp4",
                    label: "180p"
                }],
			width: '100%',
			aspectratio: '16:9',
			skin: 'five',
			logo: {
				file: "https://sites.google.com/site/gitluutru/home/khuluutrufilegmit/logo.png",
				link: 'http://www.youtube.com/watch?v=qSTS3QkoMjc',
			}
		});
	</script>	
 <?php get_footer(); ?>