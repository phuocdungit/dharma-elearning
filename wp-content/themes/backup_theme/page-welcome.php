<?php
if (isset($_SESSION['emailActive']) && !is_user_logged_in()):   
get_header();
$is_page_builder_used = et_pb_is_pagebuilder_used(get_the_ID());
?>
<script>
    <?php if (isset($_SESSION['active']) && ($_SESSION['active'])) { ?>
        alert("Thank you for activating your account..\n You're all set now.");
    <?php
}
if (isset($_SESSION['direct_launch']) && ($_SESSION['direct_launch'])) {
    ?>
        alert("The content you are trying to access is restricted.\nYou may need to enrol or get approval in order to access it.\nIf you feel this is an error, please kindly contact us.");
    <?php
}
$_SESSION['active'] = false;
$_SESSION['activing'] = false;
$_SESSION['direct_launch'] = false;
?>
</script>
<div id="main-content" class="link" data-link="<?php echo admin_url('admin-ajax.php'); ?>">
    <?php if (!$is_page_builder_used) : ?>
        <div class="container">
            <div id="content-area" class="clearfix" >
                <div id="left-area">
                <?php endif;?>
                <?php while (have_posts()) : the_post(); ?>
                    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                        <?php if (!$is_page_builder_used) : ?>
                            <h1 class="main_title"><?php the_title(); ?></h1>
                            <?php
                            $thumb = '';
                            $width = (int) apply_filters('et_pb_index_blog_image_width', 1080);
                            $height = (int) apply_filters('et_pb_index_blog_image_height', 675);
                            $classtext = 'et_featured_image';
                            $titletext = get_the_title();
                            $thumbnail = get_thumbnail($width, $height, $classtext, $titletext, $titletext, false, 'Blogimage');
                            $thumb = $thumbnail["thumb"];
                            if ('on' === et_get_option('divi_page_thumbnails', 'false') && '' !== $thumb)
                                print_thumbnail($thumb, $thumbnail["use_timthumb"], $titletext, $width, $height);
                            ?>
                        <?php endif; ?>
                        <div class="entry-content">
                           <?php
                            the_content();
                            if (!$is_page_builder_used)
                                wp_link_pages(array('before' => '<div class="page-links">' . __('Pages:', 'Divi'), 'after' => '</div>'));
                            ?>
                            <div class="et_pb_row">
                                <div class="et_pb_column et_pb_column_4_4">
                                    <div class="et_pb_text et_pb_bg_layout_light et_pb_text_align_left">
                                        <?php _e("Registration info: ","Divi"); ?> <a class="register-link edit-register" data-page="welcome" id="register-link" data-href="<?php echo wp_registration_url(); ?>" href="javascript;"> <i class="fa fa-pencil-square-o"></i> <?php _e("Edit registration info","Divi"); ?></a>
                                        <ul class="register-info">
                                            <?php 
                                            $user = get_user_by('email',$_SESSION['emailActive']);
                                            $meta = get_user_meta($user->ID);
                                            //echo $meta['uae_user_activation_code'][0];
                                            ?>
                                            <li><?php _e("Email address (username): ","Divi"); echo $user->user_email; ?></li>
                                            <li><?php _e("Full Name: ","Divi"); echo $user->first_name . ' ' . $user->last_name; ?></li>
                                            <li><?php _e("Refuge Name: ","Divi"); echo $user->display_name; ?></li>
                                        </ul>
                                        <p>
                                            <?php _e("Activation code: ","Divi"); ?>
                                            <input type="text" class="form-control code"  placeholder="Enter Activation code...">
                                            <input type="submit" data-link='<?php echo admin_url('admin-ajax.php'); ?>' id="activeCode" class="btn btn-info" value="<?php _e("Activate","Divi"); ?>" />
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div> <!-- .entry-content -->
                       <?php
                        if (!$is_page_builder_used && comments_open() && 'on' === et_get_option('divi_show_pagescomments', 'false'))
                            comments_template('', true);
                        ?>
                    </article> <!-- .et_pb_post -->
                <?php endwhile; ?>
                <?php if (!$is_page_builder_used) : ?>
                </div> <!-- #left-area -->
                <?php get_sidebar(); ?>
            </div> <!-- #content-area -->
        </div> <!-- .container -->
    <?php endif; ?>
</div> <!-- #main-content -->
<?php get_footer(); ?>
<?php else:
    wp_redirect(home_url());
    exit;
endif; ?>