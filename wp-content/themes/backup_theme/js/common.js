var App = {
    fixPopupPosition: function () {
        if (jQuery('#menuUser').length) {
            jQuery('#menuUser').click(function () {
                jQuery(window).scroll(function () {
                    var $top = jQuery(window).scrollTop() + 100;
                    if (jQuery('.ui-dialog').length) {
                        jQuery('.ui-dialog').css({'top': $top});
                    }
                });
            });
        }
    },
    changePriceRecurring: function ($val, $elem) {
        var $input = jQuery('input#price', jQuery($elem).parent());
        if ($val == 'other') {
            $input.val('').attr({'type': 'text'});
        } else {
            $input.val(0).attr({'type': 'hidden'});
            $input.val(0).attr({'type': 'hidden'});
        }

    },
    limitText: function () {
        jQuery('.my-content .et_pb_text').each(function () {
            var $data = jQuery(this).text();
            if ($data.length < 600) {
            } else {
                $data = jQuery(this).text().substr(0, 600) + '[...]';
                jQuery(this).text('').text($data);
            }
        });
    },
    hideAddcard: function () {
        setTimeout(function () {
            jQuery('.add_to_cart_button').click(function () {
                jQuery(this).fadeOut('slow');
            });
        }, 3000);
    },
    removeMyCourses: function (element, textalert) {
        var $result = confirm(textalert),
            $ajaxurl = jQuery(element).data('link'),
            $id = jQuery(element).data('id'),
            $uid = jQuery(element).data('userid');
        if ($result == true) {
            jQuery.ajax({
                type: 'POST',
                url: $ajaxurl,
                data: {action: 'unenrole_course_user', courseid: $id, uid: $uid},
                beforeSend: function () {
                    jQuery(element).addClass('loading');
                },
                success: function (response) {
                    if (response) {
                        jQuery(element).parent().prepend('<p class="notice">Your content has been removed.<br/> Please kindly wait for the page to reload.</p>')
                    }
                    jQuery(element).removeClass('loading');
                    location.reload();
                }
            });
        }
    },
    Changefavorite: function (element) {
        if (element == 1) {
            jQuery('.favorites').val(2);
            jQuery('.box-search-left .search-submit').click();
        } else {
            jQuery('.favorites').val(1);
            jQuery('.box-search-left .search-submit').click();
        }
    },
    Addfavorite: function (element, textalert) {
        $ajaxurl = jQuery(element).data('link');
        $id = jQuery(element).data('id');
        $uid = jQuery(element).data('userid');
        jQuery.ajax({
            type: 'POST',
            url: $ajaxurl,
            data: {action: 'favorite_course_user', courseid: $id, uid: $uid},
            beforeSend: function () {
                // jQuery(element).addClass('loading');
            },
            success: function (response) {
                if (response == 1) {
                    jQuery(element).addClass('add');
                    jQuery(element).removeClass('remove');
                } else {
                    jQuery(element).removeClass('add');
                    jQuery(element).addClass('remove');
                }
            }
        });

    },
    showhidecomment: function (element) {
        var $parent = jQuery(element).parents('.et_pb_column_4_4');
        var $thiscomment = $parent.find('.comment_course');
        var $allcontainercomment = jQuery('.comment_course');

        if (!$thiscomment.hasClass('active')) {
            $allcontainercomment.removeClass('active').slideUp(300);
            $thiscomment.addClass('active').slideDown(300);
        } else {
            $thiscomment.removeClass('active').slideUp(300);
        }

    },
    changePositionLaunchRemove: function () {
        if (jQuery('.launchRemoveWraper').length) {
            jQuery('.launchRemoveWraper').each(function () {
                var $parent = jQuery(this).parents('.entry-content');
                var $pbtext = $parent.find('#entry_content .et_pb_row:first-of-type .et_pb_text:last-of-type');

                if ($pbtext.length) {
                    $pbtext.append(jQuery(this));
                }
                jQuery(this).css({'display': 'block'});
            });
        }
    },
    sponsorAndBenefactor: function ($userid, $courseid, $issecret, $element) {
        if ($userid && $courseid) {
            jQuery.ajax({
                type: 'POST',
                url: '/wp-admin/admin-ajax.php',
                data: {action: 'add_course_for_sponsor', userid: $userid, courseid: $courseid, issecret: $issecret},
                beforeSend: function () {
                    jQuery('span', $element).addClass('loading');
                },
                success: function (response) {
                    if (response && $issecret) {
                        alert(jQuery($element).data('alert'));
                        location.reload();
                    } else {
                        alert('Thank you for your enrollment. The course should now be accessible. If the buttons haven not changed, try reloading the page, if the course still is not accessible, please kindly contact us at alaya@dharma.online.');
                        jQuery('.sponsorLaunch').fadeIn(300);
                        jQuery($element).hide(0);
                        location.reload();
                    }
                    jQuery('span', $element).removeClass('loading');

                }
            });
        }
    },
    changeStatusChannel: function ($courseid, $status) {
        if ($status == 0) {
            jQuery.ajax({
                type: 'POST',
                url: '/wp-admin/admin-ajax.php',
                data: {action: 'change_status_channel', courseid: $courseid},
                success: function (response) {

                }
            });
        } else {
            alert("Channel is used please come back in a few minutes");
            return false;
        }
    },
    tabcontent: function () {
        if (jQuery('.tabcontent').length) {

            var $uid = jQuery('.tabcontent').data('uid');
            var cname = 'uid' + $uid;
            var $tabcookie = App.getCookie(cname);
            if (jQuery('.tabcontent').data('fixwidth')) {
                var $width = jQuery('.tabcontent').width() - 2;
                var $liwidth = $width / jQuery('.tabcontent > li').length - 2;
                jQuery('.tabcontent > li').width($liwidth);
            }

            var $archos = jQuery('.tabcontent > li > a');

            $archos.click(function () {
                if (!jQuery(this).hasClass('active')) {
                    var $forclass = jQuery(this).data('forclass');
                    var $idactive = jQuery(this).data('idactive');
                    if (jQuery($forclass).length) {
                        jQuery($forclass).removeClass('active').hide();
                    } else {
                        jQuery('.' + $forclass).removeClass('active').hide();
                    }
                    if (jQuery($idactive).length) {
                        jQuery($idactive).addClass('active').slideDown();
                    } else {
                        jQuery('#' + $idactive).addClass('active').slideDown(300);
                    }
                    console.log(jQuery('.tabcontent > li > a'));
                    jQuery('.tabcontent > li > a').removeClass('active');
                    jQuery(this).addClass('active');
                }
                if (typeof ($uid) != 'undefined' && parseInt($uid)) {
                    App.setCookie(cname, $idactive, 30);
                }
            });
            if (jQuery.trim($tabcookie) != '') {
                jQuery('#tab_' + $tabcookie).trigger('click');
            }
        }
    },
    actionCountry: function () {
        jQuery('.add-country').click(function () {
            jQuery('.show-country').toggle('slow');
            jQuery('.country-name').val('');
        });
        var $status = 1;
        jQuery('.insert-country').click(function () {
            var $_this = jQuery(this);
            $_this.val('Loading...');
            var $name = jQuery('.country-name').val(),
                $ajaxurl = jQuery('.link').attr('link');
            if ($name == "") {
                alert("Enter name, Please");
                $_this.val('Add item');
            }
            if ($status == 1 && $name != "") {
                jQuery.ajax({
                    type: 'POST',
                    url: $ajaxurl,
                    data: {action: 'insert_country', name: $name},
                    beforeSend: function () {
                        $status = 0;
                    },
                    success: function (response) {
                        if (response == 1) {
                            location.reload();
                        }
                    }
                });
            }
        });
    },
    actionTeacher: function () {
        jQuery('.add-teacher').click(function () {
            jQuery(this).parent().find('p').toggle('slow');
            jQuery('.teacher-name').val('');
        });
        jQuery('.insert-teacher').click(function () {
            var $_this = jQuery(this),
                $name = $_this.parent().find('.teacher-name').val(),
                $url = jQuery('.link').attr('link'),
                $status = 1;
            $_this.val('Loading...');
            if ($name == "") {
                alert("Enter name, Please");
                $_this.val('Add item');
            }
            if ($status == 1 && $name != "") {
                jQuery.ajax({
                    type: 'POST',
                    url: $url,
                    data: {action: 'check_list_teacher', name: $name},
                    beforeSend: function () {
                        $status = 0;
                    },
                    success: function (response) {
                        if (response == 1) {
                            alert("Name already exists. Please choose from the list");
                            $_this.parent().find('.teacher-name').val('');
                            $_this.val('Add item');
                        } else {
                            jQuery.ajax({
                                type: 'POST',
                                url: $url,
                                data: {action: 'insert_teacher', name: $name},
                                success: function (response) {
                                    if (response != "") {
                                        jQuery('.show-teacher').append(response);
                                    } else {
                                        alert("insert error !!!");
                                    }
                                }, complete: function () {
                                    $_this.val('Add item');
                                    jQuery('.box-teacher').find('p').fadeOut('slow');
                                }
                            });
                        }
                    }
                });
            }
        });
    },
    actionAffiliation: function () {
        jQuery('.list-affiliation ul li span').click(function () {
            jQuery(this).parent().find("ul").toggle('slow');
        });
        jQuery('.list-school ul li span').click(function () {
            jQuery(this).parent().find("ul").toggle('slow');
        });
        jQuery('.list-affiliation ul li .add-item').click(function () {
            jQuery('.country-name').val('');
            jQuery('.show-add').find("p").fadeOut('slow');
            jQuery(this).parent().find('p').toggle('slow');
        });
        jQuery('.insert-item').click(function () {
            var $id = jQuery(this).attr('id-add'),
                $status = 1;
            var $name = jQuery(this).parent().find('.country-name').val(),
                $_this = jQuery(this),
                $link = jQuery('.link').attr('link');
            $_this.val('Loading...');
            if ($name == "") {
                alert("Enter name, Please");
                $_this.val('Add item');
            }
            if ($status == 1 && $name != "") {
                jQuery.ajax({
                    type: 'POST',
                    url: $link,
                    data: {action: 'insert_city', name: $name, id: $id},
                    beforeSend: function () {
                        $status = 0;
                    },
                    success: function (response) {
                        if (response == 1) {
                            location.reload();
                        } else {
                            alert("insert error !!!");
                        }
                    }
                });
            }
        });
    },
    actionCheckedCountry: function () {
        var $status = 1;
        jQuery('.list-affiliation ul li input:checkbox').on('click', function () {
            var $chk = jQuery(this),
                $li = $chk.closest('li'),
                $ul, $parent;
            if ($li.has('ul')) {
                $li.find(':checkbox').not(this).prop('checked', this.checked)
            }
            do {
                $ul = $li.parent();
                $parent = $ul.siblings(':checkbox');
                if ($chk.is(':checked')) {
                    $parent.prop('checked', $ul.has(':checkbox:not(:checked)').length == 0)
                } else {
                    $parent.prop('checked', false)
                }
                $chk = $parent;
                $li = $chk.closest('li');
            } while ($ul.is(':not(.someclass)'));
            var $select = new Array(),
                $icheck = document.getElementsByName('ick[]');
            for (var $i = 0; $i < $icheck.length; $i++) {
                if ($icheck[$i].checked == true) {
                    $select.push($icheck[$i].value);
                }
            }
            var $id = $select.join(','),
                $link = jQuery('.link').attr('link');
            // if ($status == 1) {
            jQuery.ajax({
                type: 'POST',
                url: $link,
                data: {action: 'show_select_country', id: $id},
                beforeSend: function () {
                    $status = 0;
                },
                success: function (data) {
                    if (data != "") {
                        jQuery('.show-select').html(data);
                    } else {
                        jQuery('.show-select').html('');
                    }
                },
                complete: function () {
                    $status = 1;
                    var $top = jQuery('.select-country').offset().top;
                    jQuery('html,body').animate({scrollTop: $top}, 800);
                }
            });
            // }
        });
    },
    actionSchool: function () {
        jQuery('.add-school').click(function () {
            jQuery('.show-add').find('p').fadeOut('slow');
            jQuery('.box-school').toggle('slow');
            jQuery('.school-name').val('');
        });
        var $status = 1;
        jQuery('.insert-lineage').click(function () {
            var $name = jQuery(this).parent().find('.school-name').val(),
                $_this = jQuery(this),
                $link = jQuery('.link').attr('link');
            $_this.val('Loading...');
            //    alert($id +" - "+ $name +" - "+ $link);
            if ($name == "") {
                alert("Enter name, Please");
                $_this.val('Add item');
            }
            if ($status == 1 && $name != "") {
                jQuery.ajax({
                    type: 'POST',
                    url: $link,
                    data: {action: 'insert_lineage', name: $name},
                    beforeSend: function () {
                        $status = 0;
                    },
                    success: function (response) {
                        if (response == 1) {
                            location.reload();
                        } else {
                            alert("insert error !!!");
                            jQuery('.school-name').val('');
                        }
                    }
                });
            }
        });
        jQuery('.add-item-school').click(function () {
            jQuery('.show-add').find('p').fadeOut('slow');
            jQuery('.box-school').fadeOut('slow');
            jQuery(this).parent().find('p').toggle('slow');
            jQuery('.school-name').val('');
        });
        jQuery('.insert-school').click(function () {
            var $id = jQuery(this).attr('id-add'),
                $status = 1;
            var $name = jQuery(this).parent().find('.school-name').val(),
                $_this = jQuery(this),
                $link = jQuery('.link').attr('link');
            $_this.val('Loading...');
            if ($name == "") {
                alert("Enter name, Please");
                $_this.val('Add item');
            }
            if ($status == 1 && $name != "") {
                jQuery.ajax({
                    type: 'POST',
                    url: $link,
                    data: {action: 'insert_school', name: $name, id: $id},
                    beforeSend: function () {
                        $status = 0;
                    },
                    success: function (response) {
                        if (response == 1) {
                            location.reload();
                        } else {
                            alert("insert error !!!");
                            jQuery('.school-name').val('');
                        }
                    }
                });
            }
        });
    },
    actionCheckedSchool: function () {
        var $status = 1;
        jQuery('.list-school ul li input:checkbox').on('click', function () {
            var $chk = jQuery(this),
                $li = $chk.closest('li'),
                $ul, $parent;
            if ($li.has('ul')) {
                $li.find(':checkbox').not(this).prop('checked', this.checked)
            }
            do {
                $ul = $li.parent();
                $parent = $ul.siblings(':checkbox');
                if ($chk.is(':checked')) {
                    $parent.prop('checked', $ul.has(':checkbox:not(:checked)').length == 0)
                } else {
                    $parent.prop('checked', false)
                }
                $chk = $parent;
                $li = $chk.closest('li');
            } while ($ul.is(':not(.someclass)'));

            var $select = new Array(),
                $icheck = document.getElementsByName('val[]');
            for (var $i = 0; $i < $icheck.length; $i++) {
                if ($icheck[$i].checked == true) {
                    $select.push($icheck[$i].value);
                }
            }
            var $id = $select.join(','),
                $link = jQuery('.link').attr('link');
            // if ($status == 1) {
            jQuery.ajax({
                type: 'POST',
                url: $link,
                data: {action: 'show_select_school', id: $id},
                beforeSend: function () {
                    $status = 0;
                },
                success: function (data) {
                    if (data != "") {
                        jQuery('.show-school').html(data);
                    } else {
                        jQuery('.show-school').html('');
                    }
                },
                complete: function () {
                    $status = 1;
                    var $top = jQuery('.show-school').offset().top;
                    jQuery('html,body').animate({scrollTop: $top}, 800);
                }
            });
            //}
        });
    },
    actionMultiSelect: function () {
        jQuery('.btn-country').click(function () {
            var $id = jQuery('.show-select').val(),
                $status = 1,
                $link = jQuery(".link").attr('link');
            jQuery(this).val('Load...');
            if ($id == null) {
                alert("Select Geography, Please !!!");
                jQuery(this).val('Save');
            } else {
                if ($status == 1) {
                    jQuery.ajax({
                        type: 'POST',
                        url: $link,
                        data: {action: 'insert_select_country', id: $id},
                        beforeSend: function () {
                            $status = 0;
                        },
                        success: function (data) {
                            if (data == 1) {
                                location.reload();
                            } else {
                                alert("insert error !!!");
                                jQuery(this).val('Save');
                            }
                        },
                        complete: function () {
                            $status = 1;
                        }
                    });
                }
            }
        });
        jQuery('.btn-school').click(function () {
            var $id = jQuery('.show-school').val(),
                $status = 1,
                $link = jQuery(".link").attr('link');
            jQuery(this).val('Load...');
            if ($id == null) {
                alert("Select School, Please !!!");
                jQuery(this).val('Save');
            } else {
                if ($status == 1) {
                    jQuery.ajax({
                        type: 'POST',
                        url: $link,
                        data: {action: 'insert_select_school', id: $id},
                        beforeSend: function () {
                            $status = 0;
                        },
                        success: function (data) {
                            if (data == 1) {
                                location.reload();
                            } else {
                                alert("insert error !!!");
                                jQuery(this).val('Save');
                            }
                        },
                        complete: function () {
                            $status = 1;
                        }
                    });
                }
            }
        });
        jQuery('.save-teacher').click(function () {
            var $id = jQuery('.show-teacher').val(),
                $status = 1,
                $link = jQuery(".link").attr('link');
            ;
            jQuery(this).val('Load...');
            if ($id == null) {
                alert("Select Teacher, Please !!!");
            } else {
                if ($status == 1) {
                    jQuery.ajax({
                        type: 'POST',
                        url: $link,
                        data: {action: 'insert_select_teacher', id: $id},
                        beforeSend: function () {
                            $status = 0;
                        },
                        success: function (data) {
                            if (data == 1) {
                                location.reload();
                            } else {
                                alert("insert error !!!");
                                jQuery(this).val('Save');
                            }
                        },
                        complete: function () {
                            $status = 1;
                        }
                    });
                }
            }
        });
    },
    DatePicker: function () {
        jQuery('.datepicker').datepicker({
            dateFormat: 'dd/mm/yy',
            changeMonth: true,
            changeYear: true,
            yearRange: "1960:2016",
        });
    },
    playVideoRecore: function () {
        if (!jQuery('.videowhisper_watch').hasClass('active')) {
            jQuery('.videowhisper_watch').removeClass('active');
            jQuery('.videowhisper_watch').filter(':visible').slideUp('normal');
            jQuery('.videowhisper_watch').addClass('active').next().stop(true, true);
            var videofirst = jQuery('.video-first').val();
            var result = videofirst.split('.mp4');
//            console.log(result[0] + '.mp4')
//            return false;
            jQuery('.link-0').addClass("play");
            jwplayer('content').setup({
                sources: [{
                    file: "https://s3-ap-southeast-1.amazonaws.com/dharmaelearning/" + result[0] + '.mp4',
                    label: "720p",
                    "default": "true"
                },
//                    {
//                        file: "https://s3-ap-southeast-1.amazonaws.com/dharmaelearning/" + result[0] + '_360p.mp4',
//                        label: "360p"
//                    },
//                    {
//                        file: "https://s3-ap-southeast-1.amazonaws.com/dharmaelearning/" + result[0] + '_240p.mp4',
//                        label: "240p"
//                    }, 
//                    {
//                        file: "https://s3-ap-southeast-1.amazonaws.com/dharmaelearning/" + result[0] + '_160p.mp4',
//                        label: "160p"
//                    }
                ],
                width: "100%",
                height: "500px",
                aspectratio: '16:9',
                skin: 'five',
                volume: "100",
                menu: "true",
                allowscriptaccess: "always",
                wmode: "opaque",
                autostart: true
            });

        } else {
            jwplayer('content').stop();
            jQuery('.movieurl').removeClass("play");
            document.cookie = "jwplayer.qualityLabel=720p ; path=/";
            jQuery('.videowhisper_watch').removeClass('active');
            jQuery('.videowhisper_watch').next().stop(true, true).slideUp('normal');
        }
    },
    playVideoOffline: function () {
        jQuery("#playlist li").on("click", function () {
            jwplayer('content').stop();
//            setInterval(function() {
//                jQuery('#content.jwplayer').html("");
//            });
            jQuery('.movieurl').removeClass("play");
            jQuery(this).addClass("play");
            var video = jQuery(this).attr("movieurl");
            var result = video.split('.mp4');
            jwplayer('content').setup({
                sources: [{
                    file: "https://s3-ap-southeast-1.amazonaws.com/dharmaelearning/" + result[0] + '.mp4',
                    label: "720p",
                    "default": "true"
                },
//                    {
//                        file: "https://s3-ap-southeast-1.amazonaws.com/dharmaelearning/" + result[0] + '_360p.mp4',
//                        label: "360p",
//                    },
//                    {
//                        file: "https://s3-ap-southeast-1.amazonaws.com/dharmaelearning/" + result[0] + '_240p.mp4',
//                        label: "240p"
//                    },
//                    {
//                        file: "https://s3-ap-southeast-1.amazonaws.com/dharmaelearning/" + result[0] + '_160p.mp4',
//                        label: "160p"
//                    }
                ],
                width: "100%",
                height: "500px",
                aspectratio: '16:9',
                skin: 'five',
                volume: "100",
                menu: "true",
                allowscriptaccess: "always",
                wmode: "opaque",
                autostart: true
            });
        });
    },
    generalProfile: function () {
        //        jQuery("#update_profile").click(function () {
//            var $pass1 = jQuery("#password").val(),
//                    $pass2 = jQuery("#confirmPassword").val();
//            if ($pass1 != "") {
//                if ($pass2 == "") {
//                    alert("Enter confirm password, Please");
//                    return false;
//                }
//                if ($pass1 != $pass2) {
//                    alert("Confirm password do not match !!!");
//                    return false;
//                }
//            }
//            if ($pass2 != "" && $pass1 == "") {
//                alert("Enter new password, Please");
//                return false;
//            }
//            if ($pass1 != "" && $pass2 != "" && $pass1 == $pass2) {
//                alert("Change password success !!!");
//            }
//        });
        jQuery(".change-picture").click(function () {
            jQuery(".picture").click();
        });
        jQuery(".picture").change(function () {
            var ext = jQuery(this).val().split('.').pop().toLowerCase();
            if (jQuery.inArray(ext, ['gif', 'png', 'jpg', 'jpeg', 'bmp']) == -1) {
                alert('Sorry, this file type is not permitted for security reasons');
                jQuery(this).val('');
            } else {
                if (jQuery(this).val() != "") {
                    read_file(this);
                }
            }
        });

        function read_file(input, thumbimage) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    jQuery(".avatar").removeAttr("src").attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
                jQuery(".avatar").show();
            }
            else {
                jQuery(".avatar").removeAttr("src").attr('src', input.value);
                jQuery(".avatar").show();
            }
        }
    },
    ActionPractice: function () {
        jQuery('.practice-item').click(function () {
            jQuery('.show-practice').toggle('slow');
            jQuery('.practice-name').val('');
        });
        var $status = 1;
        jQuery('.insert-practice').click(function () {
            var $name = jQuery('.practice-name').val(),
                $link = jQuery('.link').attr('link');
            jQuery(this).val("Load...");
            if ($name == "") {
                alert("Enter practice name, Please");
                jQuery(this).val("Add item");
            } else {
                if ($status == 1) {
                    jQuery.ajax({
                        type: "POST",
                        url: $link,
                        data: {action: 'check_list_practice', name: $name},
                        beforeSend: function () {
                            $status = 0;
                        }, success: function (data) {
                            if (data == 1) {
                                alert("Name already exists. Please choose from the list");
                                jQuery('.practice-name').val('');
                                jQuery('.insert-practice').val("Add item");
                            } else {
                                jQuery.ajax({
                                    type: "POST",
                                    url: $link,
                                    data: {action: 'insert_practice_name', name: $name},
                                    beforeSend: function () {
                                        $status = 0;
                                    }, success: function (data) {
                                        if (data != "") {
                                            jQuery('.select-practice').append(data);
                                        } else {
                                            alert("insert error !!!");
                                        }
                                    }, complete: function () {
                                        $status = 1;
                                        jQuery('.show-practice').fadeOut('slow');
                                        jQuery(this).val("Add item");
                                        jQuery('.practice-name').val('');
                                    }
                                });
                            }
                        }
                    });
                }
            }
        });
        jQuery('.practice-list').click(function () {
            jQuery(".show-listPractice").slideDown('slow');
        });
        jQuery('.insert-listPractice').click(function () {
            var $name = jQuery('.practice-listName').val(),
                $link = jQuery('.link').attr('link');
            jQuery(this).val("Load...");
            if ($name == "") {
                alert("Enter practice name, Please");
                jQuery(this).val("Add item");
            } else {
                if ($status == 1) {
                    jQuery.ajax({
                        type: "POST",
                        url: $link,
                        data: {action: 'check_list_practice', name: $name},
                        beforeSend: function () {
                            $status = 0;
                        }, success: function (data) {
                            if (data == 1) {
                                alert("Name already exists. Please choose from the list");
                                jQuery('.practice-listName').val('');
                                jQuery('.insert-listPractice').val("Add item");
                                App.ActionPractice();
                            } else {
                                jQuery.ajax({
                                    type: "POST",
                                    url: $link,
                                    data: {action: 'insert_list_practice', name: $name},
                                    beforeSend: function () {
                                        $status = 0;
                                    }, success: function (data) {
                                        if (data != "") {
                                            jQuery('.listPractice').append(data).fadeIn("slow");
                                            App.actionOngoing();
                                        } else {
                                            alert("insert error !!!");
                                        }
                                    }, complete: function () {
                                        $status = 1;
                                        jQuery('.show-listPractice').fadeOut('slow');
                                        jQuery(this).val("Add item");
                                        jQuery('.practice-listName').val('');
                                    }
                                });
                            }
                        }
                    });
                }
            }
        });
    },
    ActionRadio: function () {
        jQuery(".select-radio input[type='radio']").click(function () {
            if (jQuery(this).hasClass('add-year')) {
                jQuery(".year-complete").fadeIn('slow');
            } else {
                jQuery(".year-complete").fadeOut('slow');
            }
        });
        jQuery('.select-radio input[type="radio"]').click(function () {
            var $id = jQuery(this).val();
            if ($id == 3) {
                jQuery('.select-ongoing').slideDown('slow');
            } else {
                jQuery('.select-ongoing').slideUp('slow');
            }
        });
        jQuery(".select-ongoing ul li input[type='checkbox']").click(function () {
            if (jQuery(this).is(":checked")) {
                var $check = jQuery(this).parents("li").find("ul:first");
                $check.fadeIn("slow");
                if (jQuery(this).parents("li").find("input[type='radio']:checked").hasClass("ongoing")) {
                    jQuery(this).parents("li").find("ul.inputHide").fadeIn("slow");
                }
            } else {
                jQuery(this).parents("li").find("ul").fadeOut("slow").removeClass("show");
            }
        });
        jQuery(".select-ongoing ul li ul li input[type='radio']").change(function () {
            jQuery(this).parents("ul:nth-child(2n)").find("ul.inputHide").fadeOut('slow').removeClass('show');
            var $checkRadio = jQuery(this).parents("ul").find(".completed:checked").length,
                $check = jQuery(this).parent().find("ul");
            if ($checkRadio == 5) {
                jQuery(".select-ongoing").fadeOut('slow');
                jQuery(".select-radio").find(".add-year").attr("checked", "checked");
                jQuery(".year-complete").fadeIn('slow');
            }
            if (jQuery(this).is(":checked")) {
                if ($check.hasClass("inputHide")) {
                    $check.fadeIn('slow');
                }
            }
            if (jQuery(this).hasClass("not-yet")) {
                jQuery(this).parents("li").find("ul").removeClass("show").fadeOut();
            }
        });
    },
    initValidate: function () {
        if (jQuery('form.validate').length > 0) {
            var forms = jQuery('form.validate');
            forms.each(function () {
                var form = jQuery(this);
                var inputEmails = form.find('input.validateEmail');
                var types = ['Number', 'Email', 'Url'];
                var inputElements = form.find('input.required');
                var content = form.find('textarea.required');
                var selectbox = form.find('select.required');
                var radiobox = form.find('input.radrequired');

                form.find('.btn-submit').click(function () {
                    var error = 0;
                    if (inputElements.length > 0) {
                        inputElements.each(function () {
                            var inputElement = jQuery(this);
                            if (!App.validateElement(inputElement, true)) {
                                error++;
                            }
                        });
                    }
                    if (content.length) {

                        content.each(function () {
                            if (!App.isNotOnlySpace(jQuery(this).val())) {
                                error++;
                                jQuery(this).addClass('error');
                                jQuery(this).parent().append('<label class="error">This field is required!</label>');
                            }
                        });
                    }
                    if (radiobox.length) {
                        var groupName = new Array();
                        radiobox.each(function () {
                            var name = jQuery(this).attr('name');
                            if (jQuery.inArray(name, groupName) < 0) {
                                groupName.push(name);
                            }
                        });
                        for (var i = 0; i < groupName.length; i++) {
                            var radBox = jQuery('input[name="' + groupName[i] + '"]');
                            if (!App.validateRadiobox(groupName[i])) {
                                error++;
                                radBox.parents('.wrapper-input').find('label.error').remove();
                                radBox.parents('.wrapper-input').append('<label class="error">This field is required!</label>');
                            }
                            else {
                                radBox.parents('.wrapper-input').remove('label.error');
                            }
                        }
                    }
                    if (selectbox.length) {

                        selectbox.each(function () {
                            if (!App.validateSelectbox(jQuery(this))) {
                                error++;
                            } else {
                                jQuery(this).removeClass('error');
                                jQuery(this).parent().remove('label.error');
                            }
                        });
                    }
                    for (var i = 0; i < types.length; i++) {
                        var inputTypes = form.find('input.validate' + types[i]);
                        if (inputTypes.length > 0) {
                            inputTypes.each(function () {
                                var inputType = jQuery(this);
                                if (jQuery.trim(inputType.val()) !== '') {
                                    if (!App.validateElement(inputType, false, types[i])) {
                                        error++;
                                    }
                                }
                            });
                        }
                    }
                    if (error > 0) {
                        return false;
                    }
                });
                for (var i = 0; i < types.length; i++) {
                    var inputTypes = form.find('input.validate' + types[i]);
                    if (inputTypes.length > 0) {
                        inputTypes.each(function () {
                            var inputType = jQuery(this);
                            var error = 0;
                            inputType.keyup(function () {
                                if (inputType.val().length > 0) {
                                    if (!App.validateElement(inputType, false, types[i])) {
                                        error++;
                                    }
                                }
                            });
                            if (error > 0) {
                                return false;
                            }

                        });
                    }
                }
                if (selectbox.length) {
                    var error = 0;
                    selectbox.change(function () {
                        if (!App.validateSelectbox(jQuery(this))) {
                            error++;
                        } else {
                            error = 0;
                            jQuery(this).removeClass('error');
                            jQuery(this).parent().find('label.error').remove();
                        }
                    });
                    if (error > 0) {
                        return false;
                    }
                }
                if (radiobox.length) {
                    var error = 0;
                    radiobox.change(function () {

                        if (!App.validateRadiobox(jQuery(this).attr('name'))) {
                            error++;
                            jQuery(this).parents('.wrapper-input').find('label.error').remove();
                            jQuery(this).parents('.wrapper-input').append('<label class="error">This field is required!</label>');
                        }
                        else {
                            jQuery(this).parents('.wrapper-input').find('label.error').remove();
                        }
                    });
                }
                inputElements.each(function () {
                    var inputElement = jQuery(this);
                    var error = 0;
                    inputElement.keyup(function () {
                        if (!App.validateElement(inputElement, true)) {
                            error++;
                        }
                    });
                    if (error > 0) {
                        return false;
                    }
                });
            });
        }
    },
    isNotOnlySpace: function (sString) {
        while (sString.substring(0, 1) == ' ') {
            sString = sString.substring(1, sString.length);
        }
        while (sString.substring(sString.length - 1, sString.length) == ' ') {
            sString = sString.substring(0, sString.length - 1);
        }
        if (sString === '') {
            return false;
        }
        else {
            return true;
        }
    },
    validateSelectbox: function (selectBoxElement) {
        selectBoxElement.parent().find('label.error').remove();
        if ((jQuery.trim(selectBoxElement.val()) == '') || (parseInt(jQuery.trim(selectBoxElement.val())) == 0)) {
            selectBoxElement.addClass('error');
            selectBoxElement.parent().append('<label class="error">This field is required!</label>');
            return false;
        }
        return true;
    },
    validateRadiobox: function (radioBoxName) {
        var radioBox = jQuery('input[name="' + radioBoxName + '"]');
        var check = 0;
        radioBox.each(function () {
            if (jQuery(this).is(':checked')) {
                check = 1;
            }
        });
        return check;
    },
    isEmail: function (val) {
        var regEmail = /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))$/i;
        return regEmail.test(val);
    },
    isUrl: function (val) {
        var regUrl = /(http|https|ftp|mailto):\/\/([\w-]+\.)+[\w-]+(\/[\w- .\/?%&=]*)?/;
        return regUrl.test(val);
    },
    initScrollToTopPage: function () {
        if (jQuery('.scrolltoTop').length) {
            jQuery('.scrolltoTop').click(function () {
                jQuery('html, body').animate({scrollTop: 0}, 800);
                return false;
            });
        }

    },
    validateElement: function (inputElement, required, type, minlength) {
        if (!type) {
            type = '';
        }
        inputElement.parent().find('label.error').remove();
        inputElement.removeClass('error');
        if (required) {
            if (jQuery.trim(inputElement.val()) === '') {
                inputElement.parent().append('<label class="error">This field is require</label>');
                inputElement.addClass('error');
                return false;
            }
            if (minlength && minlength > 0) {
                if ((parseInt(minlength) > 0) && (inputElement.val().length > parseInt(minlength))) {
                    inputElement.parent().append('<label class="error">This field at least ' + parseInt(minlength) + ' characters</label>');
                    inputElement.addClass('error');
                    return false;
                }
            }
        }

        if (inputElement.hasClass('validateEmail') || inputElement.attr('type').toLowerCase() === 'email' || type.toLowerCase() === 'email') {
            if (!App.isEmail(inputElement.val())) {
                inputElement.parent().append('<label class="error">Please enter a valid email address.</label>');
                inputElement.addClass('error');
                return false;
            }
        }
        if (inputElement.hasClass('validateUrl') || inputElement.attr('type').toLowerCase() === 'url' || type.toLowerCase() === 'url') {
            if (!App.isUrl(inputElement.val())) {
                inputElement.parent().append('<label class="error">This field is url</label>');
                inputElement.addClass('error');
                return false;
            }
        }

        if ((inputElement.hasClass('validateNumber')) || (inputElement.attr('type') === 'number') || (type.toLowerCase() === 'number')) {
            if (/\D/g.test(inputElement.val())) {
                inputElement.val(inputElement.val().replace(/\D/g, ''));
            }
        }
        return true;

    }, actionOngoing: function () {
        jQuery('#select_all_main_practice').change(function () {
            var checkboxes = jQuery(this).parents(".listPractice").find('tbody input[type="checkbox"]'),
                $flag = jQuery(this).parents('.listPractice').find('.set-flag');
            if (jQuery(this).is(':checked')) {
                checkboxes.attr('checked', 'checked');
                $flag.removeClass('flag-white').addClass("flag-red");
            } else {
                $flag.removeClass("flag-red").addClass('flag').fadeIn();
                jQuery('.listPractice').find(".checkFlag").removeAttr('checked');
            }
        });
        jQuery("#select_all_practice").click(function () {
            var $flag = jQuery(this).parents('.listPractice').find('.set-flag');
            if (jQuery(this).is(":checked")) {
                jQuery(".checkPractice").attr("checked", "checked");
                if ($flag.hasClass("flag-white")) {
                    $flag.removeClass("flag-white").addClass("flag").fadeIn('slow');
                }
                if ($flag.hasClass("flag-red")) {
                    if (!jQuery('.checkFlag').is(":checked")) {
                        $flag.removeClass("flag-red").addClass("flag").fadeIn('slow');
                    }
                }
            } else {
                if ($flag.hasClass("flag")) {
                    $flag.removeClass("flag").addClass("flag-white");
                }
                jQuery(".checkPractice").removeAttr("checked");
                jQuery('.listPractice').find(".checkFlag").removeAttr('checked');
            }
        });
        jQuery(".checkPractice").click(function () {
            var $flag = jQuery(this).parents("tr").find(".set-flag"),
                $_this = jQuery(this);
            if (jQuery(this).is(":checked")) {
                if ($flag.hasClass("flag-white")) {
                    $flag.removeClass("flag-white").addClass('flag').fadeIn('slow');
                }
            } else {
                if ($flag.hasClass("flag-red")) {
                    $_this.parents('tr').find(".checkFlag").removeAttr("checked");
                    $flag.removeClass("flag-red").addClass('flag-white');
                }
                if ($flag.hasClass("flag")) {
                    $flag.removeClass("flag").addClass('flag-white');
                }
            }
        });
        jQuery(".checkFlag").click(function () {
            var $_this = jQuery(this),
                $flag = $_this.parents('tr').find(".set-flag");
            if ($_this.is(":checked")) {
                if ($flag.hasClass("flag-white")) {
                    $_this.parents('tr').find(".checkPractice").attr("checked", "checked");
                    $flag.removeClass("flag-white").addClass('flag-red');
                }
                if ($flag.hasClass("flag")) {
                    $flag.removeClass("flag").addClass('flag-red');
                }
            } else {
                if ($flag.hasClass("flag-red")) {
                    $_this.parents('tr').find(".checkPractice").attr("checked", "checked");
                    $flag.removeClass("flag-red").addClass('flag').fadeIn('slow');
                }
            }
        });
    },
    customPrice: function () {
        jQuery(".change-price").change(function () {
            var $val = jQuery(this).find("option:selected").attr("data-toggle");
            if ($val == 1) {
                jQuery(this).parent().find(".price-custom").fadeIn("slow");
            } else {
                jQuery(this).parent().find(".price-custom").fadeOut("slow");
            }
        });
        jQuery(".price-custom").blur(function () {
            var $price = jQuery(this).val();
            var id = jQuery(this).data("id");
            var $mimimum = jQuery('#' + id).val();
            var $check = $price - $mimimum;
            $price = jQuery.trim($price);
            if (jQuery.isNumeric($price)) {
                console.log($price - $mimimum);
//                return false;                               
                if ($check < 0 && $mimimum > 0) {

                    alert("Please note that you cannot choose an offering smaller than the minimum set for this course.");
                    jQuery(this).val('').hide();
                    jQuery('.' + id).val($mimimum);
                    jQuery('input[name="update_cart"]').click();
                    return false;
                }
                jQuery(this).parent().find('.order-price').val($price);
            } else if ($price != "") {
                alert("Enter numeric, Please");
                jQuery(this).val('');
                return false;
            }
        });
        jQuery(".checkout-button").click(function () {
            var $price = jQuery(this).attr("data-total-cart");
            if (0 < $price && $price < 1) {
                jQuery(".alert-cart").slideDown('slow');
                jQuery("html,body").animate({scrollTop: 150}, 500);
                return false;
            }
        });
        jQuery('input[name="update_cart"]').click(function () {
            var $price = jQuery(".price-custom").val();
            var id = jQuery(this).data("id");
            var $mimimum = jQuery('#' + id).val();
            var $check = $price - $mimimum;
            if (jQuery.isNumeric($price)) {

                console.log($price - $mimimum);
//                return false;                               
                if ($check < 0 && $mimimum > 0) {

                    alert("Please note that you cannot choose an offering smaller than the minimum set for this course.");
                    jQuery(this).val('').hide();
                    jQuery('.' + id).val($mimimum);
                    jQuery('input[name="update_cart"]').click();
                    return false;
                }
                jQuery(this).parent().find('.order-price').val($price);
            } else if ($price != "") {
                alert("Enter numeric, Please");
                jQuery(".price-custom").val('');
                return false;
            }
        });
    },
    reloadCaptchaReally: function (element) {
        var $ajaxurl = jQuery(element).data('link');
        jQuery.ajax({
            type: 'POST',
            url: $ajaxurl,
            data: {action: 'reload_captcha_really'},
            beforeSend: function () {
                jQuery(element).parent().css('position', 'relative').append('<div class="ajaxloading small"></div>');
            },
            success: function (response) {
                if (response) {
                    var $result = jQuery.parseJSON(response);
                    jQuery('#imagecaptcha').attr('src', $result[0]);
                    jQuery('#hidden_captcha').val($result[1]);

                } else {
                    alert("Sorry :Can't get new captcha.\n Please click again.");
                }
                jQuery(element).parent().css('position', 'static');
                jQuery(element).parent().find('.ajaxloading').remove();
            }
        });
    },
    tableData: {
        table: null,
        initData: function () {
            if (jQuery('.dataTable').length) {

                this.table = jQuery('.dataTable')
                    .DataTable({
                        "aoColumnDefs": [{bSortable: false, aTargets: [-1]}],
                        "language": {
                            "paginate": {
                                "previous": "<",
                                "next": ">"
                            },
                        }
                    });
                jQuery(".paginate_button").click(function () {
                    App.tableData.editRow();
                });
                //                jQuery('.dataTable tbody').on('click', 'tr', function() {
//                    console.log(jQuery(this)[0]);
//                    return false;
//                   // console.log(table.row(this).data());
//                });
            }
        },
        // Add new
        addRow: function () {
            var $status = 1;
            jQuery('.save-received').click(function (event) {
                event.preventDefault();
                var $practice = jQuery('#practice-value').val(),
                    $_this = jQuery(this),
                    $teacher = jQuery("#teacher-list").val(),
                    $year = jQuery("#year-list").val(),
                    $link = jQuery(".link").attr("link");
                if ($practice == "") {
                    alert("Select Practice, please");
                }
                if ($teacher == "") {
                    alert("Select Teacher, please");
                }
                if ($year == "") {
                    alert("Select Year, please");
                }

                if ($status == 1 && $practice != "" && $teacher != "" && $year != "") {
                    jQuery(this).text("Load...");
                    jQuery.ajax({
                        type: "POST",
                        url: $link,
                        data: {action: 'process_received', practice: $practice, teacher: $teacher, year: $year},
                        beforeSend: function () {
                            $status = 0;
                        }, success: function (response) {
                            if (response != "") {
                                jQuery('.dataTables_empty').fadeOut("slow");
                                if (App.tableData.table) {
                                    App.tableData.table.destroy();
                                    jQuery("#show-received tbody").html(response);
                                    var $top = jQuery("#show-received tbody tr:last-child").offset().top;
                                    jQuery('html,body').animate({scrollTop: $top}, 800);
                                    jQuery("#teacher-list").val('');
                                    jQuery("#year-list").val('');
                                    jQuery('#practice-value').val('');
                                    jQuery("#show-received").addClass("dataTable");
                                    App.tableData.initData();
                                }
                                App.tableData.editRow();
                            }
                        }, complete: function () {
                            jQuery('.save-received').text("Apply");
                            $status = 1;
                        }
                    });
                }
            });
            jQuery(window).load(function () {
                jQuery(".dataTable thead tr td:last-child").removeAttr("class");
            });
        },
        // Delete and odify
        editRow: function () {
            jQuery(".action").each(function () {
                var $tr = jQuery(this).parents("tr");
                var $action = jQuery(this);
                jQuery(this).change(function (event) {
                    event.preventDefault();
                    var $select = jQuery(this).val(),
                        $id = jQuery(this).attr("data-id"),
                        $_this = jQuery(this),
                        $url = jQuery(".dataTable").attr("data-link");

                    if ($select == 'edit') {
                        $_this.fadeOut("slow");
                        $tr.addClass("selected");
                        //jQuery(this).parent().find(".selectEdit").fadeIn();
                        jQuery(this).parent().find(".uncheck").fadeIn();
                        $tr.find(".selectShow").fadeIn();
                        $tr.find(".editHide").fadeOut();
                    }

                    if ($select == 'del') {
                        $tr.addClass("selected");
                        var $result = confirm("You want to delete ?");
                        if ($result == true) {
                            $_this.fadeOut("slow");
                            jQuery.ajax({
                                url: $url,
                                type: "POST",
                                data: {action: 'delete_initiation', id: $id},
                                beforeSend: function () {
                                    $_this.parent().html("<span class='loading hideLoad'></span>");
                                },
                                success: function (data) {
                                    if (data == 1) {
                                        if (App.tableData.table) {
                                            App.tableData.table.row('.selected').remove().draw(true);
                                            return;
                                        }
                                    } else {
                                        alert("an error occurred please try again");
                                    }
                                }
                            });
                        } else {
                            $_this.val(0);
                            $tr.removeClass("selected");
                        }
                    }
                });

                jQuery(".uncheck", $tr).click(function () {
                    $tr.removeClass("selected");
                    $tr.find(".selectShow").fadeOut();
                    $tr.find(".editHide").fadeIn();
                    $action.val(0).fadeIn();
                    jQuery(this).fadeOut();
                    jQuery(this).parent().find(".selectEdit").fadeOut();
                    jQuery(this).parent().find(".edit").fadeIn();
                });
                jQuery(".check", $tr).click(function () {
                    var $dataold = App.tableData.table.row($tr[0]).data();
                    var $id = jQuery(this).attr("data-id"),
                        $user = jQuery(this).attr("data-user"),
                        $practice = $tr.find(".practiceEdit"),
                        $teacher = $tr.find(".teacherEdit").val(),
                        $year = $tr.find(".yearEdit").val(),
                        $url = jQuery(".dataTable").attr("data-link"),
                        $_this = jQuery(this),
                        $pval = new Array();
                    $practice.each(function () {
                        $pval.push(jQuery(this).val());
                    });
                    jQuery.ajax({
                        type: "POST",
                        url: $url,
                        data: {
                            action: 'update_initition_received',
                            id: $id,
                            practice: $pval.join(','),
                            year: $year,
                            teacher: $teacher,
                            user: $user
                        },
                        beforeSend: function () {
                            $_this.parent().append("<span class='loading'></span>");
                            $_this.parent().find(".selectEdit").fadeOut();
                        },
                        success: function (data) {

                            if (data != "" && data != 1) {
                                jQuery("#show-received tbody").html(data);
                                App.tableData.table.row().remove().draw(true);

                                var $data = jQuery(data);
                                $data.each(function () {
                                    App.tableData.table.row.add(jQuery(this)[0]);
                                });
                                App.tableData.table.draw(true);
                            } else if (data == 1) {
                                alert("Initiations received already exists. Please choose from the list");

                            }
                        }, complete: function () {
                            $_this.parent().find(".action").fadeIn();
                            $_this.parent().find(".loading").fadeOut();
                            $tr.find(".editHide").fadeIn();
                            $tr.find(".selectShow").fadeOut();
                            $_this.parent().find(".action").val(0);
                            $_this.parent().find(".selectEdit").fadeOut();
                            $tr.removeClass("selected");
                            App.tableData.editRow();
                        }
                    });
                });
                jQuery(".selectShow", $tr).change(function () {
                    $tr.find(".check").fadeIn("slow");
                });
            });

            jQuery(".sorting").click(function () {
                jQuery(".selectShow").fadeOut();
                jQuery(".editHide").fadeIn();
                jQuery('.action').fadeIn();
                jQuery(".loading").fadeOut();
            });
        }
    },
    checkAll: function () {
        if (jQuery('.checkAll').length) {
            jQuery('.checkAll').click(function () {
                var checkboxes = jQuery(this).closest('table').find(':checkbox');
                if (checkboxes.is(':checked')) {
                    checkboxes.removeAttr('checked');
                } else {
                    checkboxes.attr('checked', 'checked');
                }
            });
        }
    },
    resendActiveCode: function () {
        //        if (jQuery('#resendActiveForm').length) {
//            jQuery('#resendActiveForm').submit(function(e) {
//                var form = jQuery(this);
//                var email = jQuery("#username",form).val();
//                var ajaxurl = jQuery("#ajaxurl",form).val();
//                
//                jQuery.ajax({
//                    data: {action: 'check_email_reactive', email: email},
//                    type: 'post',
//                    url: ajaxurl,
//                    beforeSend:function(){
//                    form.append("<span class='ajaxloading'></span>");    
//                    },
//                    success: function(data) {
//                        var $data = jQuery.parseJSON(data);
//                        jQuery('.resendActivemsg').remove();
//                        form.find('.ajaxloading').remove();
//                        form.before('<div class="resendActivemsg '+$data[0]+'">'+$data[1]+'</div>');
//                        return false;
//
//                    }
//                });
//                return false;
//            });
//        }
        if (jQuery('.reactive').length) {
            jQuery('.reactive').click(function () {
                var $url = jQuery(".link").attr('data-link');
                jQuery.ajax({
                    data: {action: 'check_email_reactive'},
                    type: 'post', url: $url,
                    success: function (data) {
                        if (data == 1) {
                            alert('This account is already activated.');
                            location.reload();
                        } else {
                            alert(data);
                        }
                    }
                });
            });
        }
    },
    activeTab: function () {
        jQuery('.tab').click(function () {
            var $active = jQuery(this).attr('data-active');
            jQuery("#tabActive").val($active);
            var $_this = jQuery('.et_pb_all_tabs').find('.et_pb_tab');
            if ($_this.hasClass('et_pb_active_content')) {
                $_this.removeClass('et_pb_active_content');
            }
            if ($active == 1) {
                $_this = jQuery('.et_pb_all_tabs').find('.et_pb_tab:first').addClass("et_pb_active_content");
            }
        });
    },
    controlProfile: function () {
        jQuery('.next').click(function () {
            var $n = jQuery(this).attr('data-next');
            jQuery("#tabActive").val($n);
            var $_this = jQuery('.et_pb_tabs_controls li');
            if ($_this.hasClass('et_pb_tab_active')) {
                $_this.removeClass('et_pb_tab_active');
                jQuery('.et_pb_tabs_controls li:nth-child(' + $n + ')').addClass('et_pb_tab_active');
                jQuery('.et_pb_tab').removeClass('et_pb_active_content').hide();
                jQuery('.et_pb_tab:nth-child(' + $n + ')').addClass('et_pb_active_content').show();
            }
        });
        jQuery('.prev').click(function () {
            var $_this = jQuery('.et_pb_tabs_controls li'),
                $n = jQuery(this).attr('data-prev');
            jQuery("#tabActive").val($n);
            if ($_this.hasClass('et_pb_tab_active')) {
                $_this.removeClass('et_pb_tab_active');
                jQuery('.et_pb_tabs_controls li:nth-child(' + $n + ')').addClass('et_pb_tab_active');
                jQuery('.et_pb_tab').removeClass('et_pb_active_content').hide();
                jQuery('.et_pb_tab:nth-child(' + $n + ')').addClass('et_pb_active_content').show();

            }
        });
    },
    activeCode: function () {
        jQuery("#activeCode").click(function () {
            var $url = jQuery(this).attr('data-link'),
                $active = jQuery('.code').val(),
                $_this = jQuery(this);
            jQuery(this).val('Loading...');
            if ($active == '') {
                alert("Enter activation code, please");
                $_this.val('Active');
            } else {
                jQuery.ajax({
                    url: $url,
                    type: "POST",
                    data: {action: 'active_account', active: $active},
                    beforeSend: function () {
                        $_this.attr('disabled', 'disabled');
                    },
                    success: function (data) {
                        console.log(data);
                        if (data == 1) {
                            alert("Thank you for activating your account..\n You're all set now.");
                            location.reload();
                        } else {
                            alert('Sorry, that activation code does not match. Please try again. You can find the activation code in your welcome email.');
                            jQuery('.code').val('');
                            $_this.val('Active');
                            $_this.removeAttr('disabled');
                        }
                    },
                    error: function () {
                        alert("an error occurred, please try again");
                        location.reload();
                    }
                });
            }
        });
    },
    addActionContactForm: function () {
        if (jQuery('p.user-subscribed').length) {
            jQuery('div.footer-subcribe').remove();
        }
    },
    updateCart: function (element, url) {
        var $priceOption = jQuery('select.change-price option:selected');
        var $price = $priceOption.data('price');
        if (typeof ($price) != 'undefined' && $price == 'notselect') {
            jQuery('#dialog_box').dialog({
                title: 'Are you sure you want to proceed without offerings?',
                width: 500,
                height: 100,
                modal: true,
                resizable: false,
                draggable: false,
                buttons: [{
                    text: 'Yes, proceed',
                    click: function () {
                        jQuery(element).parents('form').attr('action', url);
                        jQuery(element).parents('form').submit();
                    }
                },
                    {
                        text: 'Oops, let me select offerings',
                        click: function () {
                            jQuery(this).dialog('close');
                            return false;
                        }
                    }]
            });
            return false;
        } else {
            return true;
        }
    },
    setCookie: function (cname, cvalue, min) {
        var d = new Date();
        d.setTime(d.getTime() + (min * 60 * 1000));
        var expires = "expires=" + d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";path=/; " + expires;
    },
    getCookie: function (cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ')
                c = c.substring(1);
            if (c.indexOf(name) == 0)
                return c.substring(name.length, c.length);
        }
        return "";
    },
    editOrDeleteNote: function () {
        if (jQuery('.listNotes').length) {
            var node_id_input = jQuery('.note-form-submit #note_id');
            var node_title_input = jQuery('.note-form-submit input#post_title_note');
            var node_reset_input = jQuery('.note-form-submit input#reset');
            App.checkNoteEditOrDelele();
            var $list = jQuery('.listNotes');
            var $edit = jQuery('a.edit_note', $list);
            var $delete = jQuery('a.delete_note', $list);
            $edit.on('click', function () {
                var li = jQuery(this).parents('li');
                var noteid = li.data('noteid');
                var notesub = jQuery('.subject', li).text();
                var notecontent = jQuery.trim(jQuery('div.content-note', li).text());
                node_title_input.val(notesub);
                jQuery('.note-form-note #post_content_note').val(notecontent);
                node_id_input.val(noteid);
                App.checkNoteEditOrDelele();
            });
            node_reset_input.on('click', function () {
                node_id_input.val(0);
                App.checkNoteEditOrDelele();
            });
            $delete.on('click', function () {
                jQuery('.msg,.error', '#note_course').remove();
                var $confirm = confirm(jQuery(this).data('confirm'));

                if ($confirm) {
                    var li = jQuery(this).parents('li');
                    var noteid = li.data('noteid');
                    var $url = jQuery(this).data('url');
                    jQuery.ajax({
                        url: $url,
                        type: "POST",
                        data: {action: 'delete_note', note_id: noteid},
                        success: function (data) {
                            var $notice = jQuery.parseJSON(data);
                            if ($notice && $notice.type == 'msg') {
                                li.remove();
                            } else if ($notice && $notice.type == 'error') {
                                alert($notice.content);
                            } else {
                                alert("an error occurred, please try again");
                                location.reload();
                            }
                        },
                        error: function () {
                            alert("an error occurred, please try again");
                            location.reload();
                        }
                    })
                }
            });
        }
    },
    checkNoteEditOrDelele: function () {
        var node_id_input = jQuery('.note-form-submit #note_id'),
            node_title_input = jQuery('.note-form-submit input#post_title_note'),
            node_reset_input = jQuery('.note-form-submit input#reset'),
            node_submit_input = jQuery('.note-form-submit input#submit'),
            title_form_note = jQuery('h3.title_form_note');
        if (parseInt(node_id_input.val())) {
            node_title_input.css('width', 'calc(73% - 10px)');
            node_reset_input.delay(1000).fadeIn(1000);
            node_submit_input.val('Update');
            title_form_note.text('Edit note');
        } else {
            node_title_input.css('width', 'calc(85% - 10px)');
            node_reset_input.fadeOut(0);
            node_submit_input.val('Save');
            title_form_note.text('Add new note');
        }
    },
    hideAudioVideoLink: function () {
        jQuery('video,audio').bind('contextmenu', function () {
            return false;
        });
    },
    autoCompleted: function () {
        //    return false;
        jQuery(".autocomplete-user").autocomplete({
            source: function (request, response) {

                jQuery.ajax({
                    url: App.ajaxurl,
                    dataType: "json",
                    type: "POST",
                    minLength: 3,
                    data: {
                        action: 'get_user_send_email', search: request.term
                    },
                    success: function (data) {
                        response(jQuery.map
                        (data, function (obj) {
                            return {
                                label: obj.display_name + ' <' + obj.user_email + '>',
                                value: obj.display_name + ' <' + obj.user_email + '>',
                                id: obj.ID
                            };
                        }));
                    }
                });
            },
            select: function (event, ui) {
                jQuery('#user_id_field').val(ui.item.id);
            },
        });
    },
    inboxDharma: function () {

        if (jQuery('.inbox-tabs').length) {
            var tabcontrol = jQuery('.et_pb_tabs_controls', '.inbox-tabs');
            var tabs = jQuery('.et_pb_all_tabs', '.inbox-tabs');
            var activeindex = jQuery('li.et_pb_tab_active', tabcontrol).index();
            var $msglist = jQuery('.msglist', tabs);

            $msglist.each(function (index) {
                jQuery('li:nth-child(' + (index + 2) + ')', tabcontrol).append(' (<span class="msgnumber">' + jQuery(this).data('number') + '</span>)');
            });
            jQuery('li', tabcontrol).click(function () {
                App.setCookie('inbox_tab', jQuery(this).index() + 1, 30);
            });
            if (App.getUrlParameter('type')) {
                jQuery('li:nth-child(' + (1) + ')', tabcontrol).trigger('click');
            } else {
                var $index = App.getCookie('inbox_tab');
                if (!$index) {
                    $index = 2;
                }
                jQuery('li:nth-child(' + $index + ')', tabcontrol).trigger('click');

            }
        }
    },
    check_email_unread: function ($uri) {
        var interval = setInterval(function () {
            App.check_email_unread_interval($uri)
        }, 30000);
    },
    check_email_unread_interval: function ($uri) {
        jQuery.ajax({
            url: App.ajaxurl,
            type: "POST",
            data: {
                action: 'count_email_unread', doajax: 1, page_current: $uri
            },
            success: function (data) {
                if (data !== 'ERROR') {
                    if ($uri == '/dmail/') {
                        if (data != 0) {
                            var $response = jQuery.parseJSON(data);
                            if (typeof ($response.receive) !== 'undefined') {
                                jQuery('ul.messagestab').find('li.msg_empty').remove();
                                var msgnumber_receive = jQuery('li:nth-child(2) .msgnumber', '.et_pb_tabs_controls');
                                msgnumber_receive.text(parseInt(msgnumber_receive.text()) + $response.receive[0]);
                                jQuery('ul.messagestab').prepend($response.receive[1]);
                            }
                            if (typeof ($response.sent) !== 'undefined') {
                                jQuery('ul.senttab').find('li.msg_empty').remove();
                                var msgnumber_sent = jQuery('li:nth-child(3) .msgnumber', '.et_pb_tabs_controls');
                                msgnumber_sent.text(parseInt(msgnumber_sent.text()) + $response.sent[0]);
                                jQuery('ul.senttab').prepend($response.sent[1]);
                            }
                        }

                    } else {
                        jQuery('.count_unread').text(parseInt(data));
                    }
                }
                else {
                    location.reload();
                }
            }
        });
    },
    getUrlParameter: function (sParam) {
        var sPageURL = window.location.search.substring(1);
        var sURLVariables = sPageURL.split('&');
        for (var i = 0; i < sURLVariables.length; i++) {
            var sParameterName = sURLVariables[i].split('=');
            if (sParameterName[0] == sParam) {
                return sParameterName[1];
            }
        }
    },
    setMsgtoArchive: function ($id, element) {
        var index = jQuery(element).parents('.et_pb_tab').index() + 1;
        jQuery.ajax({
            url: App.ajaxurl,
            type: "POST",
            data: {
                action: 'set_msg_to_status', id: $id, status: 3
            },
            beforeSend: function () {
                jQuery('.inbox-tabs').css('position', 'relative');
                jQuery('.inbox-tabs').append('<div class="ajaxloading" style="z-index:1"></div>');

            },
            success: function (data) {
                if ('SUCCESS' === data) {
                    var $contentmove = jQuery(element).parents('li');
                    var $contentto = jQuery('.et_pb_tab:nth-of-type(4) > ul.msglist', '.inbox-tabs');
                    jQuery('ul.archivetab').find('li.msg_empty').remove();
                    $contentmove.find('a.settoarchive').remove();
                    $contentto.prepend($contentmove);
                    var $tab = jQuery('li:nth-child(4)', '.et_pb_tabs_controls');
                    var msgnumberfrom = jQuery('li:nth-child(' + (index) + ') .msgnumber', '.et_pb_tabs_controls');
                    var msgnumberto = jQuery('.msgnumber', $tab);
                    msgnumberto.text(parseInt(msgnumberto.text()) + 1);
                    msgnumberfrom.text(parseInt(msgnumberfrom.text()) - 1);
                    $tab.trigger('click');
                    jQuery('.inbox-tabs').css('position', 'static');
                    jQuery('.ajaxloading').remove();
                }
                else {
                    location.reload();
                }
            }
        });
    },
    setMsgtoTrash: function ($id, element) {
        var index = jQuery(element).parents('.et_pb_tab').index() + 1;
        jQuery.ajax({
            url: App.ajaxurl,
            type: "POST",
            data: {
                action: 'set_msg_to_status', id: $id, status: 4
            },
            beforeSend: function () {
                jQuery('.inbox-tabs').css('position', 'relative');
                jQuery('.inbox-tabs').append('<div class="ajaxloading" style="z-index:1"></div>');

            },
            success: function (data) {
                if ('SUCCESS' === data) {
                    var $contentmove = jQuery(element).parents('li');
                    var $contentto = jQuery('.et_pb_tab:nth-of-type(5) > ul.msglist', '.inbox-tabs');
                    jQuery('ul.trashtab').find('li.msg_empty').remove();
                    $contentmove.find('a.settoarchive').remove();
                    $contentmove.find('a.control-rf').remove();
                    $contentmove.find('.control-panel').html('<a href="' + App.ajaxurl + '?action=restore_email&id=' + $id + '" title="Restore"><i class="fa fa-rotate-left"></i></a><a href="javascript:;" onclick="App.setMsgtoDelete(' + $id + ',this)" title="Delete permanently"><i class="fa fa-trash-o warning"></i></a>');

                    $contentto.prepend($contentmove);
                    var $tab = jQuery('li:nth-child(5)', '.et_pb_tabs_controls');
                    var msgnumberfrom = jQuery('li:nth-child(' + (index) + ') .msgnumber', '.et_pb_tabs_controls');
                    var msgnumberto = jQuery('.msgnumber', $tab);
                    msgnumberto.text(parseInt(msgnumberto.text()) + 1);
                    msgnumberfrom.text(parseInt(msgnumberfrom.text()) - 1);
                    $tab.trigger('click');
                    jQuery('.inbox-tabs').css('position', 'static');
                    jQuery('.ajaxloading').remove();
                }
                else {
                    location.reload();
                }
            }
        });
    },
    stopBroadcasting: function ($courseid) {
        var r = confirm("Are you sure you want to end this broadcast? Click OK to end the broadcast. This will automatically assign the recorded video to the course, replacing the live stream.");
        if (r == true) {
            jQuery('.broadcasting').html('Stopping');
            jQuery('#videowhisper_container .fluid-width-video-wrapper').css('opacity', 0.5);
////            alert(12);
//            return false;
            jQuery.ajax({
                type: 'POST',
                url: '/wp-admin/admin-ajax.php',
                data: {action: 'stop_broadcasting', courseid: $courseid},
                success: function (response) {
                    var data = jQuery.parseJSON(response);
                    //                    console.log(data);
//                    return false;
                    if (data.err == 0) {
                        window.location.href = "/broadcast-live/";

                    }

                }
            });
        }
    },
    setMsgtoDelete: function ($id, element) {
        var conf = window.confirm("Press a button!");
        if (conf) {
            var index = jQuery(element).parents('.et_pb_tab').index() + 1;
            jQuery.ajax({
                url: App.ajaxurl,
                type: "POST",
                data: {
                    action: 'set_msg_to_status', id: $id, status: 5
                }, beforeSend: function () {
                    jQuery('.inbox-tabs').css('position', 'relative');
                    jQuery('.inbox-tabs').append('<div class="ajaxloading" style="z-index:1"></div>');

                },
                success: function (data) {
                    if ('SUCCESS' === data) {
                        jQuery(element).parents('li').remove();
                        var $tab = jQuery('li:nth-child(5)', '.et_pb_tabs_controls');
                        var msgnumberfrom = jQuery('li:nth-child(' + (index) + ') .msgnumber', '.et_pb_tabs_controls');
                        msgnumberfrom.text(parseInt(msgnumberfrom.text()) - 1);
                        jQuery('.inbox-tabs').css('position', 'static');
                        jQuery('.ajaxloading').remove();
                    }
                    else {
                        location.reload();
                    }
                }
            });
        }
    },
    changesort: function () {
        jQuery(".box-search-right select.sort").change(function () {
            var val = jQuery(this).val();
            if (val > 0) {
                jQuery('.box-search-left .search-submit').click();
            }
        });
        jQuery(".box-search-left select.categories").change(function () {
            // var val = jQuery(this).val();
            // if (val != '') {
            jQuery('.box-search-left .search-submit').click();
            // }
        });
        jQuery(".box-search-left select.tags").change(function () {
            // var val = jQuery(this).val();
            // if (val != '') {
            jQuery('.box-search-left .search-submit').click();
            // }
        });
        jQuery(".box-search-left select.status").change(function () {
            // var val = jQuery(this).val();
            // if (val != '') {
            jQuery('.box-search-left .search-submit').click();
            // }
        });
        jQuery(".box-search-left select.types").change(function () {
            // var val = jQuery(this).val();
            // if (val != '') {
            jQuery('.box-search-left .search-submit').click();
            // }
        });
        jQuery(".box-search-left select.favorites").change(function () {
            // var val = jQuery(this).val();
            // if (val != '') {
            jQuery('.box-search-left .search-submit').click();
            // }
        });
        jQuery(".box-search-left select.featured").change(function () {
            // var val = jQuery(this).val();
            // if (val != '') {
            jQuery('.box-search-left .search-submit').click();
            // }
        });
    }
}

jQuery(document).ready(function () {

    App.changePositionLaunchRemove();
    App.tabcontent();
    App.actionAffiliation();
    App.actionCheckedCountry();
    App.actionCheckedSchool();
    App.actionCountry();
    App.actionTeacher();
    App.actionSchool();
    App.actionMultiSelect();
    App.DatePicker();
    App.generalProfile();
    App.ActionPractice();
    App.ActionRadio();
    App.initValidate();
    App.actionOngoing();
    App.customPrice();
    App.tableData.initData();
    App.tableData.editRow();
    App.tableData.addRow();
    App.checkAll();
    App.resendActiveCode();
    App.activeTab();
    App.controlProfile();
    App.activeCode();
    App.editOrDeleteNote();
    App.hideAudioVideoLink();
    App.autoCompleted();
    App.inboxDharma();
    App.playVideoOffline();
    App.changesort();
});
