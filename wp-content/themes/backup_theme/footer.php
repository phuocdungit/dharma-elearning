<?php if ('on' == et_get_option('divi_back_to_top', 'false')) : ?>

    <span class="et_pb_scroll_top et-pb-icon"></span>

    <?php
endif;

if (!is_page_template('page-template-blank.php')) :
    ?>

    <footer id="main-footer">
        <?php //get_sidebar( 'footer' );
        ?>


        <?php if (has_nav_menu('footer-menu')) : ?>

            <div id="et-footer-nav">
                <div class="container">
                    <?php
                    wp_nav_menu(array(
                        'theme_location' => 'footer-menu',
                        'depth' => '1',
                        'menu_class' => 'bottom-nav',
                        'container' => '',
                        'fallback_cb' => '',
                    ));
                    ?>
                </div>
                <div class="clearfix"></div>
            </div>  <!--#et-footer-nav -->
        <?php endif; ?>
        <div id="footer-bottom">
            <div class="container clearfix">
                <?php
                if (false !== et_get_option('show_footer_social_icons', true)) {
                    get_template_part('includes/social_icons', 'footer');
                }
                ?>
                <div class="copyright">
                    <?php
                    //                                         echo "&copy; ". get_the_time("Y")." ";
                    //                                         bloginfo('name');
                    _e('<p>&copy; 2014-18 by <a href="https://dharma.online" title="Dharma Online">Dharma.Online</a> |</p>
                    <p>Powered by <a href="/dana" title="Aspiration">Merit</a> and <a href="https://dupseng.org" title="Rinpoche">Wisdom & Compassion</a> |</p>
                    <p>humbly presented by the <a href="https://kaiminion.me" title="Kai Minion Me">minion</a></p>');
                    ?>
                </div>
                <div class="footer-subcribe">
                    <?php
                    if (!is_user_logged_in()) {
                        $user = wp_get_current_user();
                        if (isset($_COOKIE['set_cc_email']) && $_COOKIE['set_cc_email'] != base64_encode('clear')) {
                            send_cc_email();
                            setcookie('set_cc_name', base64_encode('clear'), -1, '/');
                            setcookie('set_cc_email', base64_encode('clear'), -1, '/');
                        }
                        if ($user->ID):
                            $meta = get_user_meta($user->ID);
                            if ($meta['user_meta_mailchimp'][0] == 0):
                                subscribe_sidebar();
                                dynamic_sidebar('subscribe');
                            endif;
                            ?>
                            <script>
                                jQuery(document).ready(function () {
                                    jQuery('input.button-info', '.footer-subcribe').on('click', function () {
                                        var $newemail = jQuery.trim(jQuery('input#mc4wp_email', '.footer-subcribe').val()),
                                            $oldemail = '<?php echo $user->data->user_email ?>';

                                        if ($oldemail != $newemail) {
                                            var textconfirm = 'Are you sure you want to sign up ' + $newemail + ' for the newsletter? Your registered email address here is ' + $oldemail + '. If you confirm this, ' + $newemail + ' will receive a confirmation email from mailchimp. To avoid the impression of spam, we will also send an email to ' + $newemail + ' notifying them that ' + $oldemail + ' has subscribed them for the newsletter. We kindly ask for your understanding and apologize for any inconvenience this may cause.'
                                            var $result = confirm(textconfirm);
                                            if ($result) {
                                                App.setCookie('set_cc_name', jQuery.trim(jQuery('input#mc4wp_fname', '.footer-subcribe').val()), 0.5);
                                                App.setCookie('set_cc_email', $newemail, 0.5);
                                                return true;
                                            } else {
                                                jQuery('input#mc4wp_email', '.footer-subcribe').focus().select();
                                                return false;
                                            }


                                        } else {
                                            console.log('true email');
                                        }

                                    });
                                });
                            </script>
                            <?php
                        else:
                            if (isset($_SESSION['emailActive'])) :
                                $user = get_user_by('email', $_SESSION['emailActive']);
                                if ($user->ID):
                                    $meta = get_user_meta($user->ID);
                                    if ($meta['user_meta_mailchimp'][0] == 0):
                                        subscribe_sidebar();
                                        dynamic_sidebar('subscribe');
                                    endif;
                                else:
                                    unset($_SESSION['emailActive']);
                                    subscribe_sidebar();
                                    dynamic_sidebar('subscribe');
                                endif;
                            else:
                                subscribe_sidebar();
                                dynamic_sidebar('subscribe');
                            endif;
                        endif;
                    }
                    ?>
                    <a class="condition"
                       href="<?php echo home_url() ?>/causes-conditions/"><?php _e('Causes & Conditions', 'divi') ?></a>
                </div>

            </div>    <!-- .container -->
        </div>
    </footer> <!-- #main-footer -->
    </div> <!-- #et-main-area -->

<?php endif; // ! is_page_template( 'page-template-blank.php' )   ?>

</div> <!-- #page-container -->

<?php wp_footer(); ?>
<script>
    App.ajaxurl = '<?php echo admin_url('admin-ajax.php') ?>';
</script>
</body>
</html>