<?php
/*
  Template Name: My Content
 */
get_header();
if (!is_user_logged_in()) {
    ?>
    <div id="main-content">
        <div id="content-area" class="clearfix post-course">
            <div class="entry-content" style="text-align:center">
                <div class="mycontent-loginform">
                    <?php
                    $args = array(
                        'echo' => true,
                        'redirect' => site_url($_SERVER['REQUEST_URI']),
                        'form_id' => 'loginform',
                        'label_username' => __('Email'),
                        'label_password' => __('Password'),
                        'label_remember' => __('Remember Me'),
                        'label_log_in' => __('Log In'),
                        'id_username' => 'user_login',
                        'id_password' => 'user_pass',
                        'id_remember' => 'rememberme',
                        'id_submit' => 'wp-submit',
                        'remember' => true,
                        'value_username' => NULL,
                        'value_remember' => false
                    );
                    wp_login_form($args);
                    ?>
                    <?php echo '<a class="register-link" id="register-link" data-href="' . wp_registration_url() . '" href="javascript:;" title="' . __('Register') . '">' . __('Register') . '</a>'; ?>
                    | <a href="<?php echo wp_lostpassword_url(); ?>"
                         title="Lost your password?"><?php _e('Lost your password ?', 'Divi'); ?></a>
                </div>
            </div>
        </div>
        <?php get_footer(); ?>
    </div>
    <?php
} else {
    $is_page_builder_used = et_pb_is_pagebuilder_used(get_the_ID());
    $user = wp_get_current_user();
    ?>

    <div id="main-content">

        <?php if (!$is_page_builder_used) : ?>
        <div id="content-area" class="clearfix post-course">
            <?php endif; ?>

            <div id="content">
                <?php
                global $woocommerce;

                $enrolled = get_enrolled_courses($user);
                //$order_id=fused_get_all_user_orders($user_id);
                if (empty($enrolled)) {
                    echo "<div class='container alert'>";
                    _e('Currently you have not enrolled in any modules (Or modules requiring approval are still pending). Please go to ', 'Divi');
                    echo '<a href="' . home_url() . '/content">';
                    _e('Content', 'Divi');
                    echo '</a>';
                    _e(' to select and enrol. After checkout and if necessary approval you will find your content here.If you feel this is not as it should be, please kindly ', 'Divi');
                    echo '<a href="' . home_url() . '/contact-us">';
                    _e('contact us', 'Divi');
                    echo '</a>';
                    _e(' and we will do our best to assist you.', 'Divi');
                    echo "</div>";
                } else {
                    ?>
                    <div class="productslist">
                        <?php
                        echo do_shortcode('[woocommerce_product_search]');
                        $i = 0;
                        foreach ($enrolled as $course):
                            ?>
                            <div class="entry-content row_<?php echo $i % 2; ?>">
                                <div class="et_pb_row" style="padding: 30px 0">
                                    <div class="et_pb_column_4_4">
                                        <?php
                                        $content = apply_filters('the_content', $course->post_content);
                                        $thecontent = str_replace(']]>', ']]&gt;', $content);
                                        $meta = get_post_meta($course->ID);
                                        echo $thecontent;
                                        echo ' <div class="launchRemoveWraper" style="padding-top:10px;display: none"><a class="et_pb_promo_button btn-green" href="' . get_permalink($course->ID) . '" title="' . __('More...', 'woocommerce') . '">' . __('More...', 'woocommerce') . '</a>';
                                        if ($meta['show_meta_on_detail'][0] == 1):
                                            echo '<a onclick="window.open(\'/launch?lauching=' . ($course->ID . ',' . $user->ID) . '\',\'null\',\'width=window.screen.availWidth,height=window.screen.availHeight ,location=no,directories=no,status=no, menubar=no,toolbar=no, resizable=no\')" href="javascript:;"><span class="et_pb_promo_button success">Launch</span></a>';
                                        endif;
                                        $status = get_post_meta($course->ID, 'wpcf-sharing-course', true);
                                        if ($status != 1) {
                                            echo '<a data-link="' . admin_url('admin-ajax.php') . '" data-id="' . $course->ID . '" data-userid="' . $user->ID . '" onclick="App.removeMyCourses(this,\'' . __('Are you sure you want to remove this course from your Content? You will no longer be able to launch the course or access the related Q&A forum.', 'woocommerce') . '\');" class="et_pb_promo_button info" href="javascript:;" title="' . __('Unenroll', 'woocommerce') . '">' . __('Unenroll', 'woocommerce') . '</a></div>';
                                        }
                                        ?>

                                    </div>
                                </div>
                            </div>
                            <?php
                            $i++;
                        endforeach;
                        ?>
                    </div>
                <?php } ?>
                <?php if (!$is_page_builder_used) : ?>
            </div>
        </div> <!-- #content-area -->
    <?php endif; ?>
        <div class="clearfix"></div>
    </div> <!-- #main-content -->

    <?php
    get_footer();
}