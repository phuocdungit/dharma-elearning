<?php
if (!is_user_logged_in()) {
    get_header();
    ?>
    <div id="main-content">
        <div id="content-area" class="clearfix post-course">
            <div class="entry-content" style="text-align:center">
                <div class="mycontent-loginform">
                    <?php
                    $args = array(
                        'echo' => true,
                        'redirect' => site_url($_SERVER['REQUEST_URI']),
                        'form_id' => 'loginform',
                        'label_username' => __('Email'),
                        'label_password' => __('Password'),
                        'label_remember' => __('Remember Me'),
                        'label_log_in' => __('Log In'),
                        'id_username' => 'user_login',
                        'id_password' => 'user_pass',
                        'id_remember' => 'rememberme',
                        'id_submit' => 'wp-submit',
                        'remember' => true,
                        'value_username' => NULL,
                        'value_remember' => false
                    );
                    wp_login_form($args);
                    ?>
                    <?php echo '<a class="register-link" id="register-link" data-href="' . wp_registration_url() . '" href="javascript:;" title="' . __('Register') . '">' . __('Register') . '</a>'; ?>
                    | <a href="<?php echo wp_lostpassword_url(); ?>"
                         title="Lost your password?"><?php _e('Lost your password ?', 'Divi'); ?></a>
                </div>
            </div>
        </div>
    </div>
    <?php
} else {
    $template_directory_uri = get_template_directory_uri();
    if (sfwd_lms_has_access(get_the_ID())) { //check user direct access
        $is_page_builder_used = et_pb_is_pagebuilder_used(get_the_ID());
        $user = wp_get_current_user();
        get_header();
        ?>
        <div id="main-content">
            <div class="container">
                <div id="content-area" class="clearfix">

                    <?php while (have_posts()) : the_post(); ?>

                        <?php if (et_get_option('divi_integration_single_top') <> '' && et_get_option('divi_integrate_singletop_enable') == 'on') echo(et_get_option('divi_integration_single_top')); ?>

                        <article id="post-<?php the_ID(); ?>" <?php post_class('et_pb_post'); ?>>

                            <div class="entry-content">
                                <div id="entry_content">
                                    <?php
                                    the_content();
                                    wp_link_pages(array('before' => '<div class="page-links">' . __('Pages:', 'Divi'), 'after' => '</div>'));
                                    ?>
                                </div>
                                <div class=" et_pb_column_4_4" style="padding-top:10px;">
                                    <!--<div class="launchRemoveWraper et_pb_column_4_4" style="padding-top:10px;display: none">-->
                                    <?php
                                    $meta = get_post_meta($post->ID);
                                    $post_slug = get_post($meta['channel_live'][0]);

                                    $addCode = VWliveStreaming::channelInvalid($post_slug->post_name, true);

                                    $image = get_the_post_thumbnail($meta['channel_live'][0], 'full', array('class' => 'alignleft'));

                                    switch ($meta['show_meta_on_detail'][0]) {
                                        default:
                                        case 1:
                                            echo '<a onclick="window.open(\'/launch?lauching=' . $post->ID . ',' . $user->ID . '\',\'null\',\'width=window.screen.availWidth,height=window.screen.availHeight ,location=no,directories=no,status=no, menubar=no,toolbar=no, resizable=no\')" href="javascript:;"><span class="et_pb_promo_button success">Launch</span></a>';
                                            $status = get_post_meta($post->ID, 'wpcf-sharing-course', true);
                                            if ($status != 1) {
                                                echo '<a data-link="' . admin_url('admin-ajax.php') . '" data-id="' . $post->ID . '" data-userid="' . $user->ID . '" onclick="App.removeMyCourses(this,\'' . __('Are you sure you want to remove this course from your Content? You will no longer be able to launch the course or access the related Q&A forum.', 'Divi') . '\');" class="et_pb_promo_button info" href="javascript:;" title="' . __('Unenroll', 'woocommerce') . '">' . __('Unenroll', 'woocommerce') . '</a>';
                                            }
                                            break;
                                        case 2:
                                            echo '<a onclick="jQuery(\'.videowhisper_watch\').slideToggle(600)" class="et_pb_promo_button success" >' . __('View Live') . '</a>';
                                            $status = get_post_meta($post->ID, 'wpcf-sharing-course', true);
                                            if ($status != 1) {
                                                echo '<a data-link="' . admin_url('admin-ajax.php') . '" data-id="' . $post->ID . '" data-userid="' . $user->ID . '" onclick="App.removeMyCourses(this,\'' . __('Are you sure you want to remove this course from your Content? You will no longer be able to launch the course or access the related Q&A forum.', 'Divi') . '\');" class="et_pb_promo_button info" href="javascript:;" title="' . __('Unenroll', 'woocommerce') . '">' . __('Unenroll', 'woocommerce') . '</a>';
                                            }
                                            if ($addCode != NULL) {
                                                echo '<div class="videowhisper_watch" style="display: none">' . do_shortcode("[videowhisper_watch channel=\"" . $meta['channel_live'][0] . "\"]") . '</div>';
                                            } else {
                                                echo '<div class="videowhisper_watch" style="display: none"> Channel was not found! Live channel is only accessible on broadcast.' . $image . '</div>';
                                            }
                                            break;
                                        case 3:
                                            echo '<a onclick="App.playVideoRecore();" class="et_pb_promo_button success view_recording" >' . __('View Recording') . '</a>';
                                            $status = get_post_meta($post->ID, 'wpcf-sharing-course', true);
                                            if ($status != 1) {
                                                echo '<a data-link="' . admin_url('admin-ajax.php') . '" data-id="' . $post->ID . '" data-userid="' . $user->ID . '" onclick="App.removeMyCourses(this,\'' . __('Are you sure you want to remove this course from your Content? You will no longer be able to launch the course or access the related Q&A forum.', 'Divi') . '\');" class="et_pb_promo_button info" href="javascript:;" title="' . __('Unenroll', 'woocommerce') . '">' . __('Unenroll', 'woocommerce') . '</a>';
                                            }
                                            $list_video = explode(',', $meta['video_saved'][0]);
                                            echo '<div class="videowhisper_watch" style="display: none">';
                                            ?>
                                            <script type="text/javascript"
                                                    src="<?php echo get_template_directory_uri(); ?>/js/jwplayer.js">
                                            </script>
                                            <script
                                                    type="text/javascript">jwplayer.key = "5qMQ1qMprX8KZ79H695ZPnH4X4zDHiI0rCXt1g==";</script>
                                            <input type="hidden" class="video-first"
                                                   value="<?php echo $list_video[0] ?>">
                                            <!--<div id="StrobeMediaPlayback"></div>-->
                                            <div id='content' class="jwplayer"
                                                 style='margin: 0px auto'><?php _e('Video not found', 'Divi'); ?></div>
                                            <ul id="playlist">
                                                <?php
                                                foreach ($list_video as $key => $value) {
                                                    echo '<li class="movieurl link-' . $key . '" movieurl="' . $value . '">' . $key . '.&nbsp' . $value . '</li>';
                                                }
                                                ?>
                                            </ul>

                                            <!--//                                            show_playback($video_saved);-->
                                            <?php echo '</div>';
                                            ?>

                                            <?php
//                                            echo '<div class="videowhisper_watch" style="display: none">' . do_shortcode("[videowhisper_watch channel=\"" . $meta['channel_live'][0] . "\"]") . '</div>';
                                            break;
                                        case 4:
                                            echo '<a href="javascript:;" class="et_pb_promo_button success" >' . __('Starting ') . $meta['prepare_live'][0] . '</a>';
                                            $status = get_post_meta($post->ID, 'wpcf-sharing-course', true);
                                            if ($status != 1) {
                                                echo '<a data-link="' . admin_url('admin-ajax.php') . '" data-id="' . $post->ID . '" data-userid="' . $user->ID . '" onclick="App.removeMyCourses(this,\'' . __('Are you sure you want to remove this course from your Content? You will no longer be able to launch the course or access the related Q&A forum.', 'Divi') . '\');" class="et_pb_promo_button info" href="javascript:;" title="' . __('Unenroll', 'woocommerce') . '">' . __('Unenroll', 'woocommerce') . '</a>';
                                            }
                                            break;
                                    }
                                    //echo do_shortcode($meta['wpcf-course-document'][0]);
                                    $current_user = wp_get_current_user();

                                    if (is_user_logged_in() && get_post_type($post) == 'sfwd-courses') {
                                        if (preg_match('/' . $post->ID . '/', get_user_meta($current_user->ID, 'favorite_course_user', true))) {
                                            echo '<a data-link="' . admin_url('admin-ajax.php') . '" data-id="' . $post->ID . '" data-userid="' . $current_user->ID . '" onclick="App.Addfavorite(this);" class="favorite remove" href="javascript:;" title="Remove to favorite">' . __('<i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i>', 'woocommerce') . '</a>';
                                        } else {
                                            echo '<a data-link="' . admin_url('admin-ajax.php') . '" data-id="' . $post->ID . '" data-userid="' . $current_user->ID . '" onclick="App.Addfavorite(this);" class="favorite add" href="javascript:;" title="Add to favorite">' . __('<i class="fa fa-star-o" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i>', 'woocommerce') . '</a>';
                                        }
                                    }

                                    $metaresource = get_post_meta($post->ID, "_sfwd-courses", true);
                                    ?>
                                </div>
                                <div class="et_pb_column_4_4" style="padding-top:10px;">
                                    <ul class="tabcontent" data-fixwidth="1" data-uid="<?php echo $user->ID ?>">
                                        <li><a href="javascript:;" id="tab_comment_course"
                                               data-forclass="tabcontentapply" data-idactive="comment_course"
                                               class="et_pb_promo_button success active"
                                               title="<?php _e('Questions & Answers', 'woocommerce'); ?>"><?php _e('Questions & Answers', 'woocommerce'); ?></a>
                                        </li>
                                        <?php if (isset($metaresource['sfwd-courses_course_resource_list'])): ?>
                                            <li><a href="javascript:;" id="tab_resource_course"
                                                   data-forclass="tabcontentapply" data-idactive="resource_course"
                                                   class="et_pb_promo_button violet"
                                                   title="<?php _e('Resources', 'woocommerce'); ?>"><?php _e('Resources', 'woocommerce'); ?></a>
                                            </li>
                                        <?php endif; ?>
                                        <li><a href="javascript:;" id="tab_note_course" data-forclass="tabcontentapply"
                                               data-idactive="note_course" class="et_pb_promo_button success"
                                               title="<?php _e('Notes', 'woocommerce'); ?>"><?php _e('Notes', 'woocommerce'); ?></a>
                                        </li>

                                    </ul>
                                    <div id="comment_course" class="comment_course tabcontentapply active"
                                         style="display:block">
                                        <!--                                    <h1 class="page-header"><?php _e('Question', 'Divi'); ?></h1>-->
                                        <p class="qa-course">
                                            <?php
                                            _e('Questions & Answers with Dupsing Rinpoche. Please post your questions here. Rinpoche will answer them here as well. If you have something too personal in nature, you may want to use the ', 'Divi');
                                            echo '<a href="' . home_url() . '/contact-us">';
                                            _e('contact form', 'Divi');
                                            echo '</a>';
                                            _e('. In order to avoid duplicating questions, please kindly read through the previous questions and answers to see whether maybe your question has already been asked. It goes without saying (and weâ€™re saying it anyway, of course) that we kindly ask you to be mindful of your speech and avoid harsh or divisive language as well as stay on topic.', 'Divi');
                                            ?>
                                        </p>
                                        <div class="comment-form">
                                            <?php
                                            $comments_args = array(
                                                'label_submit' => __('Submit'),
                                                'id_form' => 'commentform',
                                                'id_submit' => 'submit',
                                                'title_reply' => 'Write a Question to Rinpoche',
                                                'comment_notes_after' => '',
                                                'comment_field' => '<p class="comment-form-comment"><textarea class="question_answer" id="comment" name="comment" placeholder="Enter your question" aria-required="true"></textarea></p>',
                                                'logged_in_as' => '<p class="logged-in-as">' .
                                                    sprintf(
                                                        __('Logged in as <a href="%1$s">%2$s</a>. <a href="%3$s" title="Log out of this account">Log out?</a>'), home_url() . '/my-profile', $user->display_name, wp_logout_url(apply_filters('the_permalink', get_permalink()))
                                                    ) . '</p>',
                                            );
                                            comment_form($comments_args, $post->ID);
                                            ?>

                                        </div>
                                        <ul class="comment-list">
                                            <?php
                                            $comments = get_comments(array(
                                                'status' => 'approve'
                                            ));
                                            //Display the list of comments
                                            wp_list_comments(array(
                                                'per_page' => -1, //Allow comment pagination
                                                'reverse_top_level' => false, //Show the latest comments at the top of the list
                                                'avatar_size' => 225,
                                                'callback' => my_custom_comment_template,
                                            ), $comments);
                                            ?>
                                        </ul>

                                    </div>
                                    <div class="tabcontentapply hide" id="note_course">
                                        <?php if (isset($_SESSION['note_notice'])): ?>
                                            <p class="<?php echo $_SESSION['note_notice']['type']; ?>"><?php echo $_SESSION['note_notice']['content']; ?></p>
                                            <?php
                                            unset($_SESSION['note_notice']);
                                        endif;
                                        ?>
                                        <p style="padding-bottom: 1em"><?php _e('Here you can store notes regarding this practice. For the time being these notes are private, but we\'ll add functionality to share and save notes in a little while. This may be helpful if you come across questions or have feedback in this module and would like to gather them first before posting them as a question in the forum. Enter a note title and the text, then click on "Save" on the right to save the note. Saved notes you can edit or delete with the links displayed to the right of the timestamp for each note. A note you want to edit will be loaded into the same form (right below here) and when you are done editing click on "Update" and your changes will be saved, or "Cancel" to keep the previously saved note.', 'Divi') ?></p>
                                        <h3 class="title_form_note"><?php _e('Add new note', 'Divi') ?></h3>
                                        <form name="frmnote" action="<?php echo admin_url('admin-ajax.php') ?>"
                                              id="note_form" class="validate" method="post">
                                            <p class="note-form-submit">
                                                <input type="text" id="post_title_note" name="post_title"
                                                       placeholder="Title" aria-required="true" required value="">
                                                <input type="submit" name="submit" class="btn-submit" required
                                                       value="Save" id="submit">
                                                <input type="reset" name="reset" class="hide" required
                                                       value="<?php _e('Cancel', 'Divi') ?>" id="reset">
                                                <input type="hidden" name="action" value="submit_note">
                                                <input type="hidden" name="post_parent" value="<?php the_ID(); ?>">
                                                <input type="hidden" name="ID" id="note_id" value="0">
                                                <input type="hidden" name="UID" value="<?php echo $user->ID ?>">
                                            </p>
                                            <p class="note-form-note">
                                                <textarea aria-required="true" placeholder="Enter your note"
                                                          name="post_content" id="post_content_note"></textarea>
                                            </p>
                                        </form>
                                        <?php
                                        $args = array(
                                            'posts_per_page' => 5,
                                            'offset' => 0,
                                            'orderby' => 'post_date',
                                            'order' => 'DESC',
                                            'author' => get_current_user_id(),
                                            'post_type' => 'note',
                                            'post_parent' => $post->ID,
                                            'suppress_filters' => true);
                                        $notes = get_posts($args);
                                        if (!empty($notes)):
                                            ?>
                                            <ul class="listNotes">
                                                <?php foreach ($notes as $note):
                                                    ?>
                                                    <li data-noteid="<?php echo $note->ID ?>">
                                                        <div class="usernote">
                                                            <p><?php printf(__('%1$s at %2$s'), get_the_date('', $note->ID), get_the_time('', $note->ID)); ?>
                                                                <a href="javascript:;"
                                                                   title="<?php _e('Edit', 'Divi') ?>"
                                                                   class="control_note edit_note"><?php _e('Edit', 'Divi') ?></a><a
                                                                        href="javascript:;"
                                                                        title="<?php _e('Delete', 'Divi') ?>"
                                                                        data-url="<?php echo admin_url('admin-ajax.php'); ?>"
                                                                        data-confirm="<?php _e('Are you sure you want to delete the note? This cannot be undone.', 'Divi') ?>"
                                                                        class="control_note delete_note"><?php _e('Delete', 'Divi') ?></a>
                                                            </p>
                                                            <p class="subject"><?php echo $note->post_title ?></p>
                                                            <div class="content-note">
                                                                <?php echo $note->post_content ?>
                                                            </div>
                                                        </div>
                                                    </li>
                                                <?php endforeach; ?>
                                            </ul>
                                        <?php endif; ?>
                                        <!--                                        <p class="qa-course">
                                        <?php // _e('This feature is not yet available. Here you will be able to keep notes related to this course. Please check back later.', 'Divi');       ?>
            </p>-->
                                    </div>
                                    <?php if (isset($metaresource['sfwd-courses_course_resource_list'])): ?>
                                        <div class="tabcontentapply hide" id="resource_course">
                                            <div class="resource_course">
                                                <?php
                                                $resids = explode(',', $metaresource['sfwd-courses_course_resource_list']);
                                                foreach ($resids as $resid) :
                                                    $resource = get_post($resid);
                                                    if ($resource):
                                                        ?>
                                                        <div class="rescontent">
                                                            <?php
                                                            $rescontent = apply_filters('the_content', $resource->post_content);
                                                            $rescontent = str_replace(']]>', ']]&gt;', $rescontent);
                                                            echo $rescontent;
                                                            ?>

                                                        </div>
                                                        <?php
                                                    endif;
                                                endforeach;
                                                //                                            $description = apply_filters('the_content', $metapost[0]);
                                                //                                            $description = str_replace(']]>', ']]&gt;', $description);
                                                //                                            echo $description;
                                                ?>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div> <!-- .entry-content -->

                            <?php
                            if (et_get_option('divi_468_enable') == 'on') {
                                echo '<div class="et-single-post-ad">';
                                if (et_get_option('divi_468_adsense') <> '')
                                    echo(et_get_option('divi_468_adsense'));
                                else {
                                    ?>
                                    <a href="<?php echo esc_url(et_get_option('divi_468_url')); ?>"><img
                                                src="<?php echo esc_attr(et_get_option('divi_468_image')); ?>"
                                                alt="468 ad"
                                                class="foursixeight"/></a>
                                    <?php
                                }
                                echo '</div> <!-- .et-single-post-ad -->';
                            }
                            ?>

                            <?php
                            if ((comments_open() || get_comments_number()) && 'on' == et_get_option('divi_show_postcomments', 'on'))
                                comments_template('', true);
                            ?>
                        </article> <!-- .et_pb_post -->

                        <?php if (et_get_option('divi_integration_single_bottom') <> '' && et_get_option('divi_integrate_singlebottom_enable') == 'on') echo(et_get_option('divi_integration_single_bottom')); ?>
                    <?php endwhile; ?>
                </div> <!-- #content-area -->
            </div> <!-- .container -->
        </div> <!-- #main-content -->
    <?php } else {
        ?>
        <script>
            alert("The content you are trying to access is restricted. You may need to enrol or get approval in order to access it. If you feel this is an error, please kindly contact us");
            location.href = '<?php echo home_url('my-content'); ?>';
        </script>
        <?php
        exit;
    }
}
?>
<?php get_footer(); ?>
