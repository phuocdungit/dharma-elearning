<?php if (!defined('ABSPATH'))
    exit; // Exit if accessed directly 
$user = wp_get_current_user();
?>
<p>Dear <?php echo $user->first_name?> </p>
<p>Thank you very much! We have received your offering. 
<p>Any unrestricted modules you have enrolled in will be immediately available</p>
in <strong><a href="<?php echo home_url()?>/my-content/" title="my Dharma - my Content">my Dharma - my Content</a>.</strong></p>
<p>Any restricted items will need to be approved by us and will only be 
available after the approval has gone through. We will need to determine 
whether you meet the criteria necessary for accessing these modules. This 
can take a little while. Please kindly be patient with us.</p>
<p>We may also need to contact you by email to ensure that we can all guard 
our samaya well. If you feel this takes too long or if you are unsure about 
your qualifications for any restricted module, please kindly contact us via 
the website or by replying to this message and we will assist you as best 
we can. We will of course refund your offerings for any restricted courses 
that we cannot approve if you wish.</p>
<p>Since we are using Jinpa’s company The Vimalakirti Project Ltd for the 
payment process, the confirmation email from Paypal will show that name 
instead of Dharma-eLearning. However, you should also find our offeringrelated email address, <a href="mailto:dana@dharma-elearning.net">dana@dharma-elearning.net</a> (which is also the 
sender of this email) in PayPal’s confirmation. Your bank or credit card 
statement will show PAYPAL*DHARMA-ELG for this transaction.</p>
<p>Once again, if you have any questions please don’t hesitate to contact us 
through the website or simply reply to this email.</p>
<p>Once more thank you very much for your offerings and your practice!</p>