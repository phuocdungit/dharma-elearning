<?php
/**
 * Order details
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.2.0
 */
if (!defined('ABSPATH'))
    exit; // Exit if accessed directly

$order = wc_get_order($order_id);
if ($_SESSION['cartcustom']) {
    $_SESSION['cartcustom'] = array();
}
?>
<h2><?php _e('Offering Details', 'woocommerce'); ?></h2>
<table class="shop_table order_details">
    <thead>
        <tr>
            <th class="product-name"><?php _e('Item', 'woocommerce'); ?></th>
            <th class="product-total"><?php _e('Offering', 'woocommerce'); ?></th>
        </tr>
    </thead>
    <tbody>
        <?php
        if (sizeof($order->get_items()) > 0) {

            foreach ($order->get_items() as $item) {
                $_product = apply_filters('woocommerce_order_item_product', $order->get_product_from_item($item), $item);
                $item_meta = new WC_Order_Item_Meta($item['item_meta'], $_product);
                ?>
                <tr class="<?php echo esc_attr(apply_filters('woocommerce_order_item_class', 'order_item', $item, $order)); ?>">
                    <td class="product-name">
                        <?php
                        $courseId = get_post_meta($_product->id, '_related_course', true);
                        if ($_product && !$_product->is_visible())
                            echo apply_filters('woocommerce_order_item_name', $item['name'], $item);
                        else {
                            if ($courseId == 'dana') {
                                echo apply_filters('woocommerce_order_item_name', $item['name'], $item);
                            } else {
                                echo apply_filters('woocommerce_order_item_name', sprintf('<a href="%s">%s</a>', get_permalink($courseId), $item['name']), $item);
                            }
                        }

                        echo apply_filters('woocommerce_order_item_quantity_html', ' <strong class="product-quantity">' . sprintf('&times; %s', $item['qty']) . '</strong>', $item);

                        $item_meta->display();

                        if ($_product && $_product->exists() && $_product->is_downloadable() && $order->is_download_permitted()) {

                            $download_files = $order->get_item_downloads($item);
                            $i = 0;
                            $links = array();

                            foreach ($download_files as $download_id => $file) {
                                $i++;

                                $links[] = '<small><a href="' . esc_url($file['download_url']) . '">' . sprintf(__('Download file%s', 'woocommerce'), ( count($download_files) > 1 ? ' ' . $i . ': ' : ': ')) . esc_html($file['name']) . '</a></small>';
                            }

                            echo '<br/>' . implode('<br/>', $links);
                        }
                        ?>
                    </td>
                    <td class="product-total">
                        <?php echo $order->get_formatted_line_subtotal($item); ?>
                    </td>
                </tr>
                <?php
                if ($order->has_status(array('completed', 'processing')) && ( $purchase_note = get_post_meta($_product->id, '_purchase_note', true) )) {
                    ?>
                    <tr class="product-purchase-note">
                        <td colspan="3"><?php echo wpautop(do_shortcode($purchase_note)); ?></td>
                    </tr>
                    <?php
                }
            }
        }

        do_action('woocommerce_order_items_table', $order);
        ?>
    </tbody>
    <tfoot>
        <?php
        if ($totals = $order->get_order_item_totals())
            foreach ($totals as $total) :
                ?>
                <tr>
                    <th scope="row"><?php echo $total['label']; ?></th>
                    <td><?php echo $total['value']; ?></td>
                </tr>
                <?php
            endforeach;
        ?>

    </tfoot>
</table>
<div style="float:right"><a class="button" style="margin-right: 10px;white-space: nowrap;display: inline-block" href="<?php echo get_home_url(); ?>" title="Go to homepage"><?php _e('Home', 'divi') ?></a><a style="margin-right: 10px;white-space: nowrap;display: inline-block" class="button" href="<?php echo home_url() ?>/my-content"><?php _e('my Content', 'divi'); ?></a></div>
<div class="clear"></div>
