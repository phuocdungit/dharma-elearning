<?php

/**
 * WordPress Cron Implementation for hosts, which do not offer CRON or for which
 * the user has not set up a CRON job pointing to this file.
 *
 * The HTTP request to this file will not slow down the visitor who happens to
 * visit when the cron job is needed to run.
 *
 * @package WordPress
 */
ignore_user_abort(true);


if (!empty($_POST) || defined('DOING_AJAX') || defined('DOING_CRON'))
    die();


/**
 * Tell WordPress we are doing the CRON task.
 *
 * @var bool
 */
define('DOING_CRON', true);


if (!defined('ABSPATH')) {

    /** Set up WordPress environment */
    require_once(dirname(__FILE__) . '/wp-load.php');
}

//Begin practic
$argruments = array(
    'numberposts' => -1,
    'post_type' => 'practic',
    'post_status' => 'publish',
    'orderby' => 'name',
    'order' => 'ASC'
);
$AllPost = get_posts($argruments);

if ($AllPost) {
    foreach ($AllPost as $key => $value) {
        $post = array('ID' => $value->ID, 'post_status' => 'private');
        wp_update_post($post);
    }
    echo '<h2>Update practice done</h2>';
}
//End practic

//Begin teacher

$argruments = array(
    'numberposts' => -1,
    'post_type' => 'teacher',
    'post_status' => 'publish',
    'orderby' => 'name',
    'order' => 'ASC'
);
$AllPost = get_posts($argruments);
if ($AllPost) {
    foreach ($AllPost as $key => $value) {
        $post = array('ID' => $value->ID, 'post_status' => 'private');
        wp_update_post($post);
    }
    echo '<h2>Update teacher done</h2>';
}
//End teacher

//Begin resource

$argruments = array(
    'numberposts' => -1,
    'post_type' => 'resource',
    'post_status' => 'publish',
    'orderby' => 'name',
    'order' => 'ASC'
);
$AllPost = get_posts($argruments);
if ($AllPost) {
    foreach ($AllPost as $key => $value) {
        $post = array('ID' => $value->ID, 'post_status' => 'private');
        wp_update_post($post);
    }
    echo '<h2>Update resource done</h2>';
}
//End resource

//Begin note

$argruments = array(
    'numberposts' => -1,
    'post_type' => 'note',
    'post_status' => 'publish',
    'orderby' => 'name',
    'order' => 'ASC'
);
$AllPost = get_posts($argruments);
if ($AllPost) {
    foreach ($AllPost as $key => $value) {
        $post = array('ID' => $value->ID, 'post_status' => 'private');
        wp_update_post($post);
    }
    echo '<h2>Update note done</h2>';
}
//End note